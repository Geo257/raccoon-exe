﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CleanServiceWorkerNet
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string exePath = Path.Combine(Environment.CurrentDirectory, "9Pos.exe");
            bool killed = false;
            try
            {

                if (File.Exists(exePath))
                {
                    // Start the other application
                    Process[] processes = Process.GetProcessesByName("9Pos");
                    if (processes.Length > 0)
                    {
                        // Close each process found
                        foreach (Process process in processes)
                        {
                            // Close the process
                            process.Kill();
                            Console.WriteLine($"Process '{process.ProcessName}' with PID {process.Id} closed successfully.");
                            killed = true;
                        }
                    }
                    else
                    {
                        Console.WriteLine($"No process with name 9Pos.exe found.");
                    }

                }
                else
                {
                    Console.WriteLine("Executable file not found.");
                }

                Thread.Sleep(1000);
                string path_ServiceWorker = @"%USERPROFILE%\AppData\Local\CefSharp\Cache\Service Worker";
                string dir_ServiceWorker = Environment.ExpandEnvironmentVariables(path_ServiceWorker);
                DirectoryInfo dir = new DirectoryInfo(dir_ServiceWorker);
                if (dir.Exists) dir.Delete(true);


                Console.WriteLine("Successfully clear service worker.");
            }
            catch (System.Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {

                if (killed && File.Exists(exePath))
                {
                    // Start the other application
                    Process.Start(exePath);
                    Console.WriteLine("9Pos opened successfully.");
                }
            }
        }
    }
}
