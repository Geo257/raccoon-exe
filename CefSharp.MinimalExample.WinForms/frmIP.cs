﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CefSharp.MinimalExample.WinForms
{
	public partial class frmIP : Form
	{
		public string IP { get; set; }
		public string Encoding { get; set; }
		public frmIP() {
			InitializeComponent();
		}

		private void button1_Click(object sender, EventArgs e) {
			IP = textBox1.Text;
			Encoding = txtEncoding.Text;
			
		}
	}
}
