﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Runtime.Remoting;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CefSharp.MinimalExample.WinForms
{

    public class Escpos : IDisposable
    {
        byte[] INIT = { 0x1B, 0x40 };
        byte[] SET_RIGHT_CHAR_SPACE = { 0x1B, 0x20, 0x00 };
        byte[] SET_RIGHT_CHAR_SPACE_2X = { 0x1B, 0x20, 0x02 };
        //Print Modes
        byte[] RESET_TO_DEFAULT = { 0x1B, 0x21, 0x00 };

        byte[] FONT_3X = { 0x1D, 0x21, 0x21 };
        byte[] FONT_2X = { 0x1D, 0x21, 0x11 };
        byte[] FONT_1X = { 0x1D, 0x21, 0x00 };

        //AYTA EINAI 100%
        byte[] DOUBLE_HEIGHT = { 0x1D, 0x21, 0x01 };
        byte[] DOUBLE_WIDTH = { 0x1D, 0x21, 0x10 };

        byte[] BOLD_DISABLE = { 0x1B, 0x45, 0x00 };
        byte[] BOLD_ENABLE = { 0x1B, 0x45, 0x01 };
        byte[] BoldLarge = { 0x1D, 0x21, 0x10 };

        byte[] DOUBLE_STRIKE_DISABLE = { 0x1B, 0x47, 0x00 };
        byte[] DOUBLE_STRIKE_ENABLE = { 0x1B, 0x47, 0x01 };

        byte[] ITALIC_DISABLE = { 0x1B, 0x34, 0x00 };
        byte[] ITALIC_ENABLE = { 0x1B, 0x34, 0x01 };

        byte[] UNDERLINE_DISABLE = { 0x1B, 0x2D, 0x00 };
        byte[] UNDERLINE_ENABLE = { 0x1B, 0x2D, 0x01 };

        byte[] FONT_INTERNATIONAL = { 0x1b, 0x1d, 0x74, 0x02 };
        byte[] SET_GREEK_FORMAT = { 0x1b, 0x1d, 0x74, 0x0f };
        byte[] SET_GREEK_FORMAT1 = { 0x1b, 0x74, 0x0B };
        byte[] SET_GREEK_FORMAT2 = { 0x1b, 0x74, 0x0E };
        byte[] SET_GREEK_FORMAT3 = { 0x1b, 0x74, 0x0F };
        byte[] SET_GREEK_FORMAT4 = { 0x1b, 0x74, 0x26 };
        byte[] SET_GREEK_FORMAT5 = { 0x1b, 0x74, 0x2F };
        byte[] SET_GREEK_FORMAT6 = { 0x1b, 0x74, 0x15 };


        byte[] SELECT_CYRILLIC_CHARACTER_CODE_TABLE = { 0x1B, 0x74, 0x11 };

        byte[] PRINT_LINE_FEED = { 0x0A };
        byte[] PRINT_CARRIAGE_RETURN = { 0x0D };
        byte[] PRINT_PAPER_FEED = { 0x1B, 0x4A };



        byte[] LEFT_JUSTIFY = { 0x1B, 0x61, 0x00 };
        byte[] CENTER_JUSTIFY = { 0x1B, 0x61, 0x01 };
        byte[] RIGHT_JUSTIFY = { 0x1B, 0x61, 0x02 };

        byte[] FEED_PAPER_AND_CUT = { 0x1D, 0x56, 66, 0x00 };
        byte[] OPEN_DRAWER = { 0x1B, 0x70, 0x01 };
        byte[] BEEP = { 0x1B, 0x1E };
        byte[] EURO = { 0x1B, 0x74, 0x13, (byte)0xD5 };
        byte[] GREECE_2 = { 27, 116, 47 };
        byte[] GREECE = { 27, 116, 15 };

        byte[] PRINTER_STATUS = { 0x10, 0x04, 0x02 };

        public void Dispose()
        {

        }



        public bool PrintReceipt(string printer_ip, string port, string message)
        {
            bool ErrorPrinter = false;
            int Copies = 1;
            int NumberOfBlanksPerLine = 0;
            string EscposCommand = "";
            Socket clientSock = null;
            try
            {
                clientSock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                //clientSock.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.SendTimeout, 1000);
                //clientSock.SendTimeout = 2000;
                clientSock.NoDelay = true;
                clientSock.ReceiveTimeout = 20000;
                IPAddress ip = IPAddress.Parse(printer_ip);
                IPEndPoint remoteEP = new IPEndPoint(ip, Convert.ToInt32(port));

                clientSock.Connect(remoteEP);
                if (clientSock.Connected)
                {

                    try
                    {
                        byte[] byData = new byte[] { 16, 4, 1 }; // DLE EOT 1
                        clientSock.Send(byData);

                        byte[] bytes = new byte[1024];
                        int bytesReceived = clientSock.Receive(bytes);
                        if (bytesReceived != 1)
                        {
                            ErrorPrinter = true;
                        }
                    }
                    catch (System.Exception ex)
                    {
                        ErrorPrinter = true;
                    }

                    if (!ErrorPrinter)
                    {
                        clientSock.Send(INIT);

                        clientSock.Send(GREECE_2); //SET Greek format
                        //clientSock.Send(SET_GREEK_FORMAT);
                        string[] parts = message.Split('~');

                        String encodingCode = "windows-1253";
                        if (parts.Length >= 13 && parts[12] != null && parts[12] != "")
                        {
                            encodingCode = parts[12];
                        }
                        Encoding enc = Encoding.GetEncoding(encodingCode);

                        if (parts.Length >= 14 && parts[13] != null && parts[13] != "")
                        {
                            Copies = Convert.ToInt32(parts[13]);
                        }
                        if (Copies <= 1) Copies = 1;

                        if (parts.Length >= 15 && parts[14] != null && parts[14] != "")
                        {
                            String[] codepages = parts[14].Split('|');
                            GREECE_2 = new byte[] { Convert.ToByte(codepages[0]), Convert.ToByte(codepages[1]), Convert.ToByte(codepages[2]) };
                        }
                        if (parts.Length >= 16 && parts[15] != null && parts[15] != "")
                        {
                            NumberOfBlanksPerLine = Convert.ToInt32(parts[15]);
                        }
                        if (parts.Length >= 17 && parts[16] != null && parts[16] != "")
                        {
                            EscposCommand = parts[16];
                        }

                        for (int l = 1; l <= Copies; l++)
                        {
                            clientSock.Send(GREECE_2);
                            //Company Info
                            if (!string.IsNullOrEmpty(parts[0]))
                            {
                                string[] lines = parts[0].Split('|');
                                var CompanyName = lines[0];
                                var Tin = lines[1];
                                var Tel = lines[2];
                                var Add = lines[3];
                                var Email = lines[4];

                                clientSock.Send(CENTER_JUSTIFY);
                                clientSock.Send(FONT_2X);
                                clientSock.Send(enc.GetBytes(CompanyName + "\n"));

                                clientSock.Send(FONT_1X);
                                clientSock.Send(enc.GetBytes("Διεύθυνση: " + Add + "\n"));
                                clientSock.Send(enc.GetBytes("Email: " + Email + "\n"));
                                clientSock.Send(enc.GetBytes("Τηλέφωνο: " + Tel + "\n"));
                                clientSock.Send(enc.GetBytes("ΑΦΜ: " + Tin + "\n"));
                                clientSock.Send(PRINT_LINE_FEED);
                            }

                            //Customer delivery info
                            if (!string.IsNullOrEmpty(parts[1]))
                            {
                                clientSock.Send(GREECE_2);
                                string[] lines = parts[1].Split('|');
                                var CustomerName = lines[0];
                                var CustomerMobile = lines[1];
                                var CustomerTelephone = lines[2];
                                var CustomerCity = lines[3];
                                var CustomerAddress = lines[4];
                                var CustomerZipCode = lines[5];
                                var CustomerFloor = lines[6];
                                var CustomerBellDescription = lines[7];

                                clientSock.Send(LEFT_JUSTIFY);
                                
                                clientSock.Send(enc.GetBytes("Πελάτης:\t" + CustomerName + "\n"));
                                clientSock.Send(enc.GetBytes("Κινητό:\t\t" + CustomerMobile + "\n"));
                                clientSock.Send(enc.GetBytes("Τηλέφωνο:\t" + CustomerTelephone + "\n"));
                                clientSock.Send(enc.GetBytes("Πόλη:\t\t" + CustomerCity + "\n"));
                                clientSock.Send(enc.GetBytes("Διεύθυνση:\t" + CustomerAddress + "\n"));
                                clientSock.Send(enc.GetBytes("ΤΚ:\t\t" + CustomerZipCode + "\n"));
                                clientSock.Send(enc.GetBytes("Όροφος:\t\t" + CustomerFloor + "\n"));
                                clientSock.Send(enc.GetBytes("Κουδούνι:\t" + CustomerBellDescription + "\n"));
                                clientSock.Send(enc.GetBytes("\n"));
                            }

                            //ORDER INFORMATION
                            clientSock.Send(LEFT_JUSTIFY);
                            //clientSock.Send(SET_GREEK_FORMAT);
                    
                            if (!string.IsNullOrEmpty(parts[4]))
                            {
                                clientSock.Send(BOLD_ENABLE);
                                if (EscposCommand.ToLower().Contains("BoldLarge".ToLower()))
                                {
                                    clientSock.Send(DOUBLE_WIDTH);
                                }
                                clientSock.Send(enc.GetBytes("Τραπέζι:\t" + parts[4].ToString() + "\n"));
                                clientSock.Send(BOLD_DISABLE);
                                clientSock.Send(FONT_1X);
                            }

                            if (!string.IsNullOrEmpty(parts[2]))
                                clientSock.Send(enc.GetBytes("Παραγγελία:\t" + parts[2].ToString() + "\n"));

                            if (!string.IsNullOrEmpty(parts[3]))
                                clientSock.Send(enc.GetBytes("Ημερομηνία:\t" + parts[3].ToString() + "\n"));

                            //if (!string.IsNullOrEmpty(parts[4]))
                            //    clientSock.Send(enc.GetBytes("Τραπέζι:\t" + parts[4].ToString() + "\n"));

                            if (!string.IsNullOrEmpty(parts[5]))
                                clientSock.Send(enc.GetBytes("Σερβιτόρος:\t" + parts[5].ToString() + "\n"));

  
                            clientSock.Send(PRINT_LINE_FEED);
                            
                            clientSock.Send(UNDERLINE_ENABLE);

                            clientSock.Send(LEFT_JUSTIFY);

                            clientSock.Send(GREECE_2); //SET Greek format
                            clientSock.Send(enc.GetBytes("ΠΣΤ   Είδος                       Τιμή"));
                            clientSock.Send(enc.GetBytes("\n"));

                            clientSock.Send(UNDERLINE_DISABLE);

                            //Lines
                            clientSock.Send(BOLD_ENABLE);
                            clientSock.Send(FONT_1X);

                            if (!string.IsNullOrEmpty(parts[6]))
                            {
                                string[] lines = parts[6].Split('}');
                                foreach (string line in lines)
                                {
                                    string[] invLine = line.Split('|');
                                    if (invLine.Length > 1)
                                    {
                                        var qnt = invLine[0];
                                        var product = invLine[1];
                                        var price = invLine[2];
                                        var specs = "";
                                        if (invLine.Count() > 3)
                                            specs = invLine[3];

                                        clientSock.Send(GREECE_2); //SET Greek format

                                        if (EscposCommand.ToLower().Contains("BoldLarge".ToLower()))
                                        {
                                            clientSock.Send(DOUBLE_WIDTH);
                                        }

                                        if (EscposCommand.ToLower().Contains("DoubleHeight".ToLower()))
                                        {
                                            clientSock.Send(BOLD_ENABLE);
                                            clientSock.Send(DOUBLE_HEIGHT);
                                        }

                                        clientSock.Send(enc.GetBytes(" " + qnt + "    "));
                                        if (product.Length > 19)
                                        {
                                            clientSock.Send(enc.GetBytes(product.Substring(0, 20) + "        " + price));
                                        }
                                        else
                                        {
                                            while (product.Length < 20)
                                            {
                                                product += " ";
                                            }
                                            clientSock.Send(enc.GetBytes(product.PadRight(20 - product.Length) + "        " + price));
                                        }
                                        //clientSock.Send(EURO);
                                        clientSock.Send(enc.GetBytes("\n"));

                                        if (EscposCommand.ToLower().Contains("DoubleHeight".ToLower()))
                                        {
                                        }
                                        else
                                        {
                                            if (product.Length > 19)
                                            {
                                                while (product.Length < 50)
                                                {
                                                    product += " ";
                                                }
                                                clientSock.Send(GREECE_2); //SET Greek format
                                                clientSock.Send(enc.GetBytes("     "));
                                                clientSock.Send(enc.GetBytes(product.Substring(20, 30) + "        "));
                                                clientSock.Send(enc.GetBytes("\n"));
                                            }
                                        }

                                        clientSock.Send(GREECE_2); //SET Greek format
                                        if (!string.IsNullOrEmpty(specs))
                                        {
                                            string[] specLines = specs.Split(',');
                                            foreach (var specLine in specLines)
                                            {
                                                clientSock.Send(GREECE_2); //SET Greek format
                                                clientSock.Send(enc.GetBytes("     -" + specLine + "\n"));
                                            }
                                            clientSock.Send(enc.GetBytes("\n"));
                                        }

                                        for (int i = 0; i < NumberOfBlanksPerLine; i++)
                                        {
                                            clientSock.Send(enc.GetBytes("\n"));
                                        }
                                    }
                                }
                            }

                            clientSock.Send(BOLD_DISABLE);

                            clientSock.Send(GREECE_2); //SET Greek format
                                                       //Total Quantity
                            if (!string.IsNullOrEmpty(parts[7]))
                            {
                                clientSock.Send(PRINT_LINE_FEED);
                                clientSock.Send(RIGHT_JUSTIFY);
                                clientSock.Send(FONT_1X);
                                clientSock.Send(enc.GetBytes("Ποσότητα:" + parts[7] + "\n"));
                            }

                            clientSock.Send(GREECE_2); //SET Greek format
                                                       //Total Value

                            if (parts.Length >= 19 && parts[18] != null && parts[18].ToString() != "0")
                            {
                                clientSock.Send(GREECE_2);
                                clientSock.Send(enc.GetBytes("Συνολική Αξία:" + parts[18] + ""));
                                clientSock.Send(EURO);
                                clientSock.Send(enc.GetBytes("\n"));
                            }

                            if (parts.Length >= 20 && parts[19] != null && parts[19].ToString() != "0")
                            {
                                clientSock.Send(GREECE_2);
                                clientSock.Send(enc.GetBytes("Αξία Έκπτωσης:" + parts[19] + ""));
                                clientSock.Send(EURO);
                                clientSock.Send(enc.GetBytes("\n"));
                            }

                            if (!string.IsNullOrEmpty(parts[8]))
                            {
                                clientSock.Send(BOLD_ENABLE);
                                clientSock.Send(GREECE_2);
                                if (EscposCommand.ToLower().Contains("DoubleHeight".ToLower()))
                                {
                                    clientSock.Send(DOUBLE_HEIGHT);
                                }
                                clientSock.Send(enc.GetBytes("Σύνολο:" + parts[8] + ""));
                                clientSock.Send(EURO);
                                clientSock.Send(enc.GetBytes("\n"));
                                clientSock.Send(BOLD_DISABLE);
                                if (EscposCommand.ToLower().Contains("DoubleHeight".ToLower()))
                                {
                                    clientSock.Send(DOUBLE_HEIGHT);
                                }
                            }

                            if (parts.Length >= 21 && parts[20] != null && parts[20].ToString() != "0")
                            {
                                clientSock.Send(BOLD_ENABLE);
                                clientSock.Send(GREECE_2);
                                clientSock.Send(enc.GetBytes("Υπόλοιπο:" + parts[20] + ""));
                                clientSock.Send(EURO);
                                clientSock.Send(enc.GetBytes("\n"));
                                clientSock.Send(BOLD_DISABLE);
                            }

                            clientSock.Send(GREECE_2); //SET Greek format
                                                       //WIFI INFORMATION
                            if (!string.IsNullOrEmpty(parts[9]))
                            {
                                clientSock.Send(PRINT_LINE_FEED);
                                clientSock.Send(CENTER_JUSTIFY);
                                clientSock.Send(FONT_1X);

                                string[] lines = parts[9].Split('|');
                                var WifiSSID = lines[0];
                                var WifiKey = lines[1];

                                clientSock.Send(enc.GetBytes("WIFI SSID:" + WifiSSID + "\n"));
                                clientSock.Send(enc.GetBytes("WIFI KEY:" + WifiKey + "\n"));
                            }

                            clientSock.Send(GREECE_2); //SET Greek format
                                                       //INVOICE COMMENT
                            if (!string.IsNullOrEmpty(parts[10]))
                            {
                                clientSock.Send(PRINT_LINE_FEED);
                                clientSock.Send(LEFT_JUSTIFY);
                                clientSock.Send(FONT_1X);
                                clientSock.Send(enc.GetBytes(parts[10] + "\n"));
                            }

                            clientSock.Send(GREECE_2); //SET Greek format
                                                       //COMMENT
                            if (!string.IsNullOrEmpty(parts[11]))
                            {
                                clientSock.Send(PRINT_LINE_FEED);
                                clientSock.Send(LEFT_JUSTIFY);
                                clientSock.Send(FONT_1X);
                                clientSock.Send(enc.GetBytes(parts[11] + "\n"));
                            }

                            //#region QrCode
                            //clientSock.Send(CENTER_JUSTIFY);
                            //                  string buffer = "";
                            //                  var QrData = "https://www.google.com";
                            //                  int store_len = (QrData).Length + 3;
                            //                  byte store_pL = (byte)(store_len % 256);
                            //                  byte store_pH = (byte)(store_len / 256);

                            //                  buffer += enc.GetString(new byte[] { 29, 40, 107, 4, 0, 49, 65, 50, 0 });
                            //                  buffer += enc.GetString(new byte[] { 29, 40, 107, 3, 0, 49, 67, 8 });
                            //                  buffer += enc.GetString(new byte[] { 29, 40, 107, 3, 0, 49, 69, 48 });
                            //                  buffer += enc.GetString(new byte[] { 29, 40, 107, store_pL, store_pH, 49, 80, 48 });
                            //                  buffer += QrData;
                            //                  buffer += enc.GetString(new byte[] { 29, 40, 107, 3, 0, 49, 81, 48 });

                            //                  clientSock.Send(enc.GetBytes(buffer));
                            //#endregion

                            clientSock.Send(PRINT_LINE_FEED);

                            //PAPER FEED AND CUT AND BEEP AND OPEN DRAWER
                            clientSock.Send(FEED_PAPER_AND_CUT);
                            //clientSock.Send(OPEN_DRAWER);
                            clientSock.Send(BEEP);

                        }
                        ErrorPrinter = false;
                    }
                    else
                    {
                        ErrorPrinter = true;
                    }


                }

            }
            catch (System.Exception ex)
            {
                BrowserForm.Log("Print Error : " + ex.Message);
                ErrorPrinter = true;
            }

            if (clientSock != null && clientSock.Connected)
            {
                clientSock.Shutdown(SocketShutdown.Both);
                clientSock.Close();
            }

            return ErrorPrinter;
        }

        public void PrintCodePageTest(string printer_ip, string port, string encoding)
        {

            Socket clientSock = null;
            try
            {
                clientSock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                clientSock.NoDelay = true;
                clientSock.ReceiveTimeout = 2000;
                IPAddress ip = IPAddress.Parse(printer_ip);
                IPEndPoint remoteEP = new IPEndPoint(ip, Convert.ToInt32(port));

                Encoding enc = Encoding.GetEncoding(encoding);
                //Encoding enc = Encoding.GetEncoding("ibm737");

                clientSock.Connect(remoteEP);
                if (clientSock.Connected)
                {
                    for (int i = 1; i <= 150; i++)
                    {
                        clientSock.Send(new byte[] { 27, 116, Convert.ToByte(i) }); //SET Greek format
                        clientSock.Send(enc.GetBytes("position:" + i.ToString() + " ---> αβγδεζηθάίόώ€ " + "\n"));
                    }
                }
                clientSock.Send(FEED_PAPER_AND_CUT);
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            if (clientSock != null && clientSock.Connected)
            {
                clientSock.Shutdown(SocketShutdown.Both);
                clientSock.Close();
            }
        }

        public bool PrintSimpleReceipt(String printer_ip, String port, String message, String encoding, String codepage1, String codepage2, String codepage3)
        {
            bool ErrorPrinter = false;
            Socket clientSock = null;
            try
            {
                clientSock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                clientSock.NoDelay = true;
                clientSock.ReceiveTimeout = 20000;
                IPAddress ip = IPAddress.Parse(printer_ip);
                IPEndPoint remoteEP = new IPEndPoint(ip, Convert.ToInt32(port));

                clientSock.Connect(remoteEP);
                if (clientSock.Connected)
                {

                    try
                    {
                        byte[] byData = new byte[] { 16, 4, 1 }; // DLE EOT 1
                        clientSock.Send(byData);

                        byte[] bytes = new byte[1024];
                        int bytesReceived = clientSock.Receive(bytes);
                        if (bytesReceived != 1)
                        {
                            ErrorPrinter = true;
                        }
                    }
                    catch (System.Exception ex)
                    {
                        ErrorPrinter = true;
                    }

                    if (!ErrorPrinter)
                    {
                        clientSock.Send(INIT);

                        GREECE_2 = new byte[] { Convert.ToByte(codepage1), Convert.ToByte(codepage2), Convert.ToByte(codepage3) };
                        clientSock.Send(GREECE_2); //SET Greek format



                        if (encoding == null || encoding == "") encoding = "windows-1253";

                        Encoding enc = Encoding.GetEncoding(encoding);

                        clientSock.Send(GREECE_2);


                        if (message.Contains("##QRSTART##"))
                        {
                            var startMessage = message.Substring(0, message.IndexOf("##QRSTART##"));
                            var qrcode = message.Substring(message.IndexOf("##QRSTART##"), message.IndexOf("##QREND##") - message.IndexOf("##QRSTART##"));
                            var restMessage = message.Substring(message.IndexOf("##QREND##"));
                            restMessage = restMessage.Replace("##QREND##", "");
                            qrcode = qrcode.Replace("##QRSTART##", "");

                            clientSock.Send(enc.GetBytes(startMessage));

                            #region QrCode
                            clientSock.Send(CENTER_JUSTIFY);
                            string buffer = "";


                            int store_len = (qrcode).Length + 3;
                            byte store_pL = (byte)(store_len % 256);
                            byte store_pH = (byte)(store_len / 256);

                            buffer += enc.GetString(new byte[] { 29, 40, 107, 4, 0, 49, 65, 50, 0 });
                            buffer += enc.GetString(new byte[] { 29, 40, 107, 3, 0, 49, 67, 8 });
                            buffer += enc.GetString(new byte[] { 29, 40, 107, 3, 0, 49, 69, 48 });
                            buffer += enc.GetString(new byte[] { 29, 40, 107, store_pL, store_pH, 49, 80, 48 });
                            buffer += qrcode;
                            buffer += enc.GetString(new byte[] { 29, 40, 107, 3, 0, 49, 81, 48 });

                            BrowserForm.Log(buffer.Replace("\u001d", "\\u001d").Replace("\u0004", "\\u0004").Replace("\u0003", "\\u0003").Replace("\01A2", "\\01A2").Replace("\01E0", "\\01E0").Replace("\01Q0", "\\01Q0").Replace("\01C", "\\01C").Replace("\b", "\\b").Replace("\0", "\\0"));

                            clientSock.Send(enc.GetBytes(buffer));
                            #endregion

                            clientSock.Send(enc.GetBytes(restMessage));
                        }
                        else {
                            clientSock.Send(enc.GetBytes(message));
                        }

                        clientSock.Send(PRINT_LINE_FEED);

                        //PAPER FEED AND CUT AND BEEP AND OPEN DRAWER
                        clientSock.Send(FEED_PAPER_AND_CUT);
                        //clientSock.Send(OPEN_DRAWER);
                        clientSock.Send(BEEP);


                        ErrorPrinter = false;
                    }
                    else
                    {
                        ErrorPrinter = true;
                    }


                }

            }
            catch (System.Exception ex)
            {
                ErrorPrinter = true;
            }

            if (clientSock != null && clientSock.Connected)
            {
                clientSock.Shutdown(SocketShutdown.Both);
                clientSock.Close();
            }

            return ErrorPrinter;
        }

        public bool DiagnosticTestConnection(String Id,String printer_ip, String port, String encoding, String codepage1, String codepage2, String codepage3)
        {
            bool connectedToPrinter = false;
            Socket clientSock = null;
            try
            {
                clientSock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                clientSock.NoDelay = true;
                clientSock.ReceiveTimeout = 20000;
                IPAddress ip = IPAddress.Parse(printer_ip);
                IPEndPoint remoteEP = new IPEndPoint(ip, Convert.ToInt32(port));

                clientSock.Connect(remoteEP);
                connectedToPrinter = clientSock.Connected;
            }
            catch (System.Exception ex)
            {
                connectedToPrinter = false;
            }

            if (clientSock != null && clientSock.Connected)
            {
                clientSock.Shutdown(SocketShutdown.Both);
                clientSock.Close();
            }

            if (connectedToPrinter)
            {
                Common.browser.Load("javascript:DiagnosticMessageAnswer('" + Id + "')");
            }
            return connectedToPrinter;
        }

        public bool PrintDigiNetESCPos(String simansi, String qrcode, String printer_ip, String port, String message, String encoding, String codepage1, String codepage2, String codepage3)
        {
            bool ErrorPrinter = false;
            Socket clientSock = null;
            try
            {
                clientSock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                clientSock.NoDelay = true;
                clientSock.ReceiveTimeout = 20000;
                IPAddress ip = IPAddress.Parse(printer_ip);
                IPEndPoint remoteEP = new IPEndPoint(ip, Convert.ToInt32(port));

                clientSock.Connect(remoteEP);
                if (clientSock.Connected)
                {

                    try
                    {
                        Encoding enc = Encoding.GetEncoding(encoding);
                        byte[] byData = new byte[] { 16, 4, 1 }; // DLE EOT 1
                        clientSock.Send(byData);

                        byte[] greek_encode = new byte[] { Convert.ToByte(codepage1), Convert.ToByte(codepage2), Convert.ToByte(codepage3) };
                        clientSock.Send(greek_encode); //SET Greek format


                        byte[] bytes = new byte[1024];
                        int bytesReceived = clientSock.Receive(bytes);
                        if (bytesReceived != 1)
                        {
                            ErrorPrinter = true;
                        }
                    }
                    catch (System.Exception ex)
                    {
                        ErrorPrinter = true;
                    }

                    if (!ErrorPrinter)
                    {
                        clientSock.Send(INIT);

                        GREECE_2 = new byte[] { Convert.ToByte(codepage1), Convert.ToByte(codepage2), Convert.ToByte(codepage3) };
                        clientSock.Send(GREECE_2); //SET Greek format



                        if (encoding == null || encoding == "") encoding = "windows-1253";

                        Encoding enc = Encoding.GetEncoding(encoding);

                        clientSock.Send(GREECE_2);
                        clientSock.Send(enc.GetBytes(message));

                        #region QrCode
                        clientSock.Send(CENTER_JUSTIFY);
                        string buffer = "";

                        BrowserForm.Log(qrcode);

                        int store_len = (qrcode).Length + 3;
                        byte store_pL = (byte)(store_len % 256);
                        byte store_pH = (byte)(store_len / 256);

                        buffer += enc.GetString(new byte[] { 29, 40, 107, 4, 0, 49, 65, 50, 0 });
                        buffer += enc.GetString(new byte[] { 29, 40, 107, 3, 0, 49, 67, 8 });
                        buffer += enc.GetString(new byte[] { 29, 40, 107, 3, 0, 49, 69, 48 });
                        buffer += enc.GetString(new byte[] { 29, 40, 107, store_pL, store_pH, 49, 80, 48 });
                        buffer += qrcode;
                        buffer += enc.GetString(new byte[] { 29, 40, 107, 3, 0, 49, 81, 48 });

                        BrowserForm.Log(buffer.Replace("\u001d", "\\u001d").Replace("\u0004", "\\u0004").Replace("\u0003", "\\u0003").Replace("\01A2", "\\01A2").Replace("\01E0", "\\01E0").Replace("\01Q0", "\\01Q0").Replace("\01C", "\\01C").Replace("\b", "\\b").Replace("\0", "\\0"));

                        clientSock.Send(enc.GetBytes(buffer));

                        clientSock.Send(enc.GetBytes(simansi.Substring(0, 40)));
                        clientSock.Send(enc.GetBytes("\n"));
                        clientSock.Send(enc.GetBytes(simansi.Substring(41)));
                        #endregion


                        clientSock.Send(PRINT_LINE_FEED);

                        //PAPER FEED AND CUT AND BEEP AND OPEN DRAWER
                        clientSock.Send(FEED_PAPER_AND_CUT);
                        //clientSock.Send(OPEN_DRAWER);
                        clientSock.Send(BEEP);


                        ErrorPrinter = false;
                    }
                    else
                    {
                        ErrorPrinter = true;
                    }


                }

            }
            catch (System.Exception ex)
            {
                ErrorPrinter = true;
            }

            if (clientSock != null && clientSock.Connected)
            {
                clientSock.Shutdown(SocketShutdown.Both);
                clientSock.Close();
            }

            return ErrorPrinter;
        }

        public void SendMessage(String ipAddress, String message)
        {
            SendMessage(ipAddress, message, "9700");
            //bool ErrorPrinter = false;
            //Socket clientSock = null;
            //try
            //{
            //    clientSock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            //    clientSock.NoDelay = true;
            //    clientSock.ReceiveTimeout = 20000;
            //    IPAddress ip = IPAddress.Parse(ipAddress);
            //    IPEndPoint remoteEP = new IPEndPoint(ip, Convert.ToInt32(9700));

            //    clientSock.Connect(remoteEP);
            //    if (clientSock.Connected)
            //    {
            //        try
            //        {
            //            clientSock.Send(Encoding.UTF8.GetBytes(message));
            //        }
            //        catch (System.Exception ex)
            //        {
            //            ErrorPrinter = true;
            //        }
            //    }

            //}
            //catch (System.Exception ex)
            //{
            //    ErrorPrinter = true;
            //}

            //if (clientSock != null && clientSock.Connected)
            //{
            //    clientSock.Shutdown(SocketShutdown.Both);
            //    clientSock.Close();
            //}

        }

        public void SendMessage(String ipAddress, String message, String Port)
        {
            //bool ErrorPrinter = false;
            try
            {
                Byte[] buffer = Encoding.UTF8.GetBytes(message);
                var endPoint = new DnsEndPoint(ipAddress, Convert.ToInt32(Port));

                int SendCount = 0;

                while (SendCount < 10)
                {
                    SendCount++;

                    Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

                    IAsyncResult result = socket.BeginConnect(ipAddress, Convert.ToInt32(Port), null, null);
                    bool success = result.AsyncWaitHandle.WaitOne(2000, true);

                    if (socket.Connected)
                    {
                        socket.EndConnect(result);

                        //???? Send Length
                        int reqLen = buffer.Length;
                        int reqLenH2N = IPAddress.HostToNetworkOrder(reqLen);
                        byte[] reqLenArray = BitConverter.GetBytes(reqLenH2N);

                        byte[] sendData = new byte[reqLenArray.Length + buffer.Length];
                        Array.Copy(reqLenArray, 0, sendData, 0, reqLenArray.Length);
                        Array.Copy(buffer, 0, sendData, reqLenArray.Length, buffer.Length);
                        // --> sendData
                        //????
                        //socket.BeginSend(buffer, 0, buffer.Length, 0, new AsyncCallback(SendCallback), socket);
                        socket.BeginSend(sendData, 0, sendData.Length, 0, new AsyncCallback(SendCallback), socket);
                        SendCount = 10;
                    }
                    else
                    {
                        socket.Shutdown(SocketShutdown.Both);
                        socket.Close();
                    }

                }

            }
            catch (System.Exception ex)
            {
                //ErrorPrinter = true;
            }

        }
        //private void SendCallback(object sender, SocketAsyncEventArgs e)
        //{
        //    try
        //    {
        //        Socket socket = sender as Socket;
        //        if (socket != null) {
        //            socket.EndSend()
        //            socket.Shutdown(SocketShutdown.Both);
        //            socket.Close();
        //        }
        //    }
        //    catch (System.Exception ex) { 
        //    }

        //}

        private static void SendCallback(IAsyncResult ar)
        {
            try
            {
                Socket handler = (Socket)ar.AsyncState;

                int bytesSent = handler.EndSend(ar);

                handler.Shutdown(SocketShutdown.Both);
                handler.Close();

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }
    }
}
