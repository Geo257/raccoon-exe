﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CefSharp.MinimalExample.WinForms
{
    public class DiginetPrintData
    {
        public string WindowsDiginetServer_BillType { get; set; }
        public string WindowsDiginetServer_e_line { get; set; }
        public string WindowsDiginetServer_message { get; set; }
        public string WindowsDiginetServer_ip { get; set; }
        public string WindowsDiginetServer_port { get; set; }
        public string WindowsDiginetServer_encoding { get; set; }
        public string WindowsDiginetServer_codepage1 { get; set; }
        public string WindowsDiginetServer_codepage2 { get; set; }
        public string WindowsDiginetServer_codepage3 { get; set; }
        public string WindowsDiginetServer_InvoiceId { get; set; }
        public string WindowsDiginetServer_BillCode { get; set; }
        public string WindowsDiginetServer_lineIDs { get; set; }
        public string WindowsDiginetServer_Type { get; set; }
        public string WindowsDiginetServer_CancelBillCode { get; set; }
        public string WindowsDiginetServer_contextData { get; set; }
        public string WindowsDiginetServer_fromIP { get; set; }
        public string WindowsDiginetServer_PrintType { get; set; }
    }
}
