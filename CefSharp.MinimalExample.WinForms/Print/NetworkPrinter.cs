﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using System.Drawing.Printing;
using CefSharp.Enums;
using System.Runtime.Remoting.Contexts;
using System.Windows.Forms;
using CefSharp.DevTools.Browser;
using System.IO;
using System.Drawing;

namespace CefSharp.MinimalExample.WinForms.Print
{
    public class NetworkPrinter : IDisposable
    {

        public bool DynamicPrint(String ip, String port, String message, String Lines, int PrinterNum, String StationID, String InvoiceID, String encoding, int Copies, String CodePage1, String CodePage2, String CodePage3) 
        {
            Boolean result = false;
            int xCopies = Copies;
            if (xCopies == 0) xCopies = 1;
            for (int i = 0; i < xCopies; i++)
            {
                result = PrintTextToEscPosPrinter(ip, port, message, Lines, PrinterNum, StationID, InvoiceID, encoding, Copies, CodePage1, CodePage2, CodePage3);
            }
            return result;
        }


        public bool PrintTextToEscPosPrinter(String ipAddress, String port, String message, String LinesToPrint, int PrinterNum, String StationID, String InvoiceID, String encoding, int Copies, String CodePage1, String CodePage2, String CodePage3) {

            bool ErrorPrinter = false;
            Socket clientSock = null;
            try
            {
                clientSock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                clientSock.NoDelay = true;
                clientSock.ReceiveTimeout = 20000;
                IPAddress ip = IPAddress.Parse(ipAddress);
                IPEndPoint remoteEP = new IPEndPoint(ip, Convert.ToInt32(port));

                clientSock.Connect(remoteEP);
                if (clientSock.Connected)
                {

                    try
                    {
                        byte[] byData = new byte[] { 16, 4, 1 }; // DLE EOT 1
                        clientSock.Send(byData);

                        byte[] bytes = new byte[1024];
                        int bytesReceived = clientSock.Receive(bytes);
                        if (bytesReceived != 1)
                        {
                            ErrorPrinter = true;
                        }
                    }
                    catch (System.Exception ex)
                    {
                        ErrorPrinter = true;
                    }

                    if (!ErrorPrinter)
                    {
                        clientSock.Send(Commands.HW_INIT);
                        clientSock.Send(Commands.CHARCODE_GREEK);
                        setTextSize(clientSock,1, 1);  //setTextSizeNormal
                        clientSock.Send(new byte[] { Convert.ToByte(CodePage1), Convert.ToByte(CodePage2), Convert.ToByte(CodePage3) });

                        if (encoding == null || encoding == "") encoding = "windows-1253";
                        Encoding enc = Encoding.GetEncoding(encoding);

                        setTextFontA(clientSock);

                        String[] Lines = message.Split(new String[] { "\n"},StringSplitOptions.None);
                        if (Lines.Length > 0)
                        {
                            string textSize = "";
                            string textType = "";
                            string textAligment = "";

                            for (int jL = 0; jL < Lines.Length; jL++)
                            {

                                //if (textSize != "SM")
                                //{
                                //    setTextFontA(clientSock);
                                //    //setTextSize(clientSock, 1, 1);     //setTextSizeNormal
                                //    textSize = "SM";
                                //}

                                if (textType != "NORMAL")
                                {
                                    setTextType(clientSock, "NORMAL"); //setTextTypeNormal
                                    textType = "NORMAL";
                                }

                                clientSock.Send(enc.GetBytes("\n"));

                                String[] tagsOfLine = Lines[jL].Split(new String[] { "~~" },StringSplitOptions.None);

                                if (tagsOfLine.Length > 0)
                                {
                                    for (int j = 0; j < tagsOfLine.Length; j++)
                                    {
                                        if (tagsOfLine[j].Length >= 2)
                                        {
                                            switch (tagsOfLine[j].Substring(0, 2))
                                            {
                                                case "BO":
                                                    if (textType != "BO")
                                                    {
                                                        setTextTypeBold(clientSock);
                                                        textType = "BO";
                                                    }
                                                    break;
                                                case "LE":
                                                    if (textAligment != "LE")
                                                    {
                                                        setTextAlignLeft(clientSock);
                                                        textAligment = "LE";
                                                    }
                                                    break;
                                                case "CE":
                                                    if (textAligment != "CE")
                                                    {
                                                        setTextAlignCenter(clientSock);
                                                        textAligment = "CE";
                                                    }
                                                    break;
                                                case "RI":
                                                    if (textAligment != "RI")
                                                    {
                                                        setTextAlignRight(clientSock);
                                                        textAligment = "RI";
                                                    }
                                                    break;
                                                case "SM":
                                                    if (textSize != "SM")
                                                    {
                                                        setTextSizeSmall(clientSock);
                                                        //setTextFontA(clientSock);
                                                        textSize = "SM";
                                                    }
                                                    break;
                                                case "NO":
                                                    if (textSize != "NORMAL")
                                                    {
                                                        setTextSizeNormal(clientSock);
                                                        textSize = "NORMAL";
                                                    }
                                                    break;
                                                case "LW":
                                                    if (textSize != "LW")
                                                    {
                                                        setTextSize2W(clientSock);
                                                        textSize = "LW";
                                                    }
                                                    break;
                                                case "LH":
                                                    if (textSize != "LH")
                                                    {
                                                        setTextSize2H(clientSock);
                                                        textSize = "LH";
                                                    }
                                                    break;
                                                case "XL":
                                                    if (textSize != "XL")
                                                    {
                                                        setText4Square(clientSock);
                                                        textSize = "XL";
                                                    }
                                                    break;
                                                case "BE":
                                                    beep(clientSock);
                                                    break;
                                                case "CD":
                                                    openCashDrawerPin2(clientSock);
                                                    openCashDrawerPin5(clientSock);
                                                    break;
                                            }

                                            if (tagsOfLine[j].Substring(0, 2) == "BA")
                                                SendBarcode(clientSock, tagsOfLine[j].Substring(2, tagsOfLine[j].Length - 2),encoding);
                                            else
                                                if (tagsOfLine[j].Substring(2, tagsOfLine[j].Length - 2) != "")
                                                    clientSock.Send(enc.GetBytes(tagsOfLine[j].Substring(2, tagsOfLine[j].Length-2)));
                                        }
                                    }
                                }
                                else
                                {
                                    clientSock.Send(enc.GetBytes(Lines[jL]));
                                }
                            }
                        }

                        //lineBreak(clientSock, 2);
                        cut(clientSock ,"FULL");

                        ErrorPrinter = false;
                    }
                    else
                    {
                        ErrorPrinter = true;
                    }
                }
            }
            catch (System.Exception ex)
            {
                BrowserForm.Log("Print Error : " + ex.Message);
                ErrorPrinter = true;
            }

            if (clientSock != null && clientSock.Connected)
            {
                clientSock.Shutdown(SocketShutdown.Both);
                clientSock.Close();
                clientSock = null;
            }

            return ErrorPrinter;
        }

        private void setTextSize(Socket socket,int width, int height)
        {
            if (height == 2 && width == 2)
            {
                socket.Send(Commands.TXT_NORMAL);
                socket.Send(Commands.TXT_4SQUARE);
            }
            else if (height == 2)
            {
                socket.Send(Commands.TXT_NORMAL);
                socket.Send(Commands.TXT_2HEIGHT);
            }
            else if (width == 2)
            {
                socket.Send(Commands.TXT_NORMAL);
                socket.Send(Commands.TXT_2WIDTH);
            }
            else
            {
                socket.Send(Commands.TXT_NORMAL);
            }
        }
        private void setTextType(Socket socket, String type)
        {
            if (type == "B")
            {
                socket.Send(Commands.TXT_BOLD_ON);
                socket.Send(Commands.TXT_UNDERL_OFF);
            }
            else if (type == "U")
            {
                socket.Send(Commands.TXT_BOLD_OFF);
                socket.Send(Commands.TXT_UNDERL_ON);
            }
            else if (type == "U2")
            {
                socket.Send(Commands.TXT_BOLD_OFF);
                socket.Send(Commands.TXT_UNDERL2_ON);
            }
            else if (type == "BU")
            {
                socket.Send(Commands.TXT_BOLD_ON);
                socket.Send(Commands.TXT_UNDERL_ON);
            }
            else if (type == "BU2")
            {
                socket.Send(Commands.TXT_BOLD_ON);
                socket.Send(Commands.TXT_UNDERL2_ON);
            }
            else if (type == "NORMAL")
            {
                socket.Send(Commands.TXT_BOLD_OFF);
                socket.Send(Commands.TXT_UNDERL_OFF);
            }
        }
        private void setTextAlign(Socket socket, String align)
        {
            if (align == "CENTER")
            {
                socket.Send(Commands.TXT_ALIGN_CT);
            }
            else if (align == "RIGHT")
            {
                socket.Send(Commands.TXT_ALIGN_RT);
            }
            else
            {
                socket.Send(Commands.TXT_ALIGN_LT);
            }
        }
        private void setTextFont(Socket socket, String font)
        {
            if (font == "B")
            {
                socket.Send(Commands.TXT_FONT_B);
            }
            else
            {
                socket.Send(Commands.TXT_FONT_A);
            }
        }
        private void setTextTypeBold(Socket socket) {
            setTextType(socket, "B");
        }
        private void setTextAlignLeft(Socket socket)
        {
            setTextAlign(socket , "LEFT");
        }
        private void setTextAlignCenter(Socket socket) { setTextAlign(socket,"CENTER"); }
        private void setTextAlignRight(Socket socket) { setTextAlign(socket,"RIGHT"); }
        private void setTextFontA(Socket socket) { setTextFont(socket,"A"); }
        private void setTextSizeSmall(Socket socket) { socket.Send(Commands.TXT_NORMAL);  }
        private void setTextSizeNormal(Socket socket) { setTextSize(socket, 1, 1); }
        private void setTextSize2W(Socket socket) { setTextSize(socket,2, 1); }
        private void setTextSize2H(Socket socket) { setTextSize(socket,1, 2); }
        private void setText4Square(Socket socket) { setTextSize(socket,2, 2); }
        private void beep(Socket socket) {
            socket.Send(Commands.BEEPER);
        }
        private void openCashDrawerPin2(Socket socket) {
            socket.Send(Commands.CD_KICK_2);
        }
        private void openCashDrawerPin5(Socket socket) {
            socket.Send(Commands.CD_KICK_5);
        }
        private void cut(Socket socket,String mode)
        {
            for (int i = 0; i < 5; i++)
            {
                socket.Send(Commands.CTL_LF);
            }
            if (mode == "PART")
            {
                socket.Send(Commands.PAPER_PART_CUT);
            }
            else
            {
                socket.Send(Commands.PAPER_FULL_CUT);
            }
        }
        public void lineBreak(Socket socket, int nbLine)
        {
            for (int i = 0; i < nbLine; i++)
            {
                socket.Send(Commands.CTL_LF);
            }
        }
        public void SendBarcode(Socket socket ,string BarcodeNO,string encoding)
        {
            //printBarcode(socket, BarcodeNO, "", 100, 5, "", "", encoding);
            Encoding enc = Encoding.GetEncoding(encoding);

            socket.Send(Commands.TXT_ALIGN_CT);
            string buffer = "";

            int store_len = (BarcodeNO).Length + 3;
            byte store_pL = (byte)(store_len % 256);
            byte store_pH = (byte)(store_len / 256);

            buffer += enc.GetString(new byte[] { 29, 40, 107, 4, 0, 49, 65, 50, 0 });
            buffer += enc.GetString(new byte[] { 29, 40, 107, 3, 0, 49, 67, 8 });
            buffer += enc.GetString(new byte[] { 29, 40, 107, 3, 0, 49, 69, 48 });
            buffer += enc.GetString(new byte[] { 29, 40, 107, store_pL, store_pH, 49, 80, 48 });
            buffer += BarcodeNO;
            buffer += enc.GetString(new byte[] { 29, 40, 107, 3, 0, 49, 81, 48 });

            //BrowserForm.Log(buffer.Replace("\u001d", "\\u001d").Replace("\u0004", "\\u0004").Replace("\u0003", "\\u0003").Replace("\01A2", "\\01A2").Replace("\01E0", "\\01E0").Replace("\01Q0", "\\01Q0").Replace("\01C", "\\01C").Replace("\b", "\\b").Replace("\0", "\\0"));

            socket.Send(enc.GetBytes(buffer));
        }

        public void printBarcode(Socket socket ,String code, String bc, int width, int height, String pos, String font, string encoding) 
        {
            Encoding enc = Encoding.GetEncoding(encoding);

            // Align Bar Code()
            socket.Send(Commands.TXT_ALIGN_CT);

            // Height
            if (height >=2 || height <=6) {
                socket.Send(Commands.BARCODE_HEIGHT);
            } 

            //Width
            if (width >= 1 || width <= 255)
            {
                socket.Send(Commands.BARCODE_WIDTH);
            }

            //Font
            if (font == "B")
            {
                socket.Send(Commands.BARCODE_FONT_B);
            }
            else
            {
                socket.Send(Commands.BARCODE_FONT_A);
            }

            //Position
            if (pos == "OFF")
            {
                socket.Send(Commands.BARCODE_TXT_OFF);
            }
            else if (pos == "BOTH")
            {
                socket.Send(Commands.BARCODE_TXT_BTH);
            }
            else if (pos == "ABOVE")
            {
                socket.Send(Commands.BARCODE_TXT_ABV);
            }
            else
            {
                socket.Send(Commands.BARCODE_TXT_BLW);
            }
            //Type
            switch (bc.ToUpper())
            {
                case "UPC-A":
                    socket.Send(Commands.BARCODE_UPC_A);
                    break;
                case "UPC-E":
                    socket.Send(Commands.BARCODE_UPC_E);
                    break;
                default:
                case "EAN13":
                    socket.Send(Commands.BARCODE_EAN13);
                    break;
                case "EAN8":
                    socket.Send(Commands.BARCODE_EAN8);
                    break;
                case "CODE39":
                    socket.Send(Commands.BARCODE_CODE39);
                    break;
                case "ITF":
                    socket.Send(Commands.BARCODE_ITF);
                    break;
                case "NW7":
                    socket.Send(Commands.BARCODE_NW7);
                    break;
            }
            //Print Code
            if (code != "")
            {
                socket.Send(enc.GetBytes(code));
                socket.Send(Commands.CTL_LF);
            }
        }
        public void Dispose()
        {
          
        }
    }
}
