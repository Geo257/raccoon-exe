﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.UI.WebControls.WebParts;

namespace CefSharp.MinimalExample.WinForms
{
    public class RBSFHMRestaurant
    {
        private bool UseLogger = true;

        public string sendPort = "9700";
        public bool PrintEnded = false;

        private string Message;
        private string IP;
        private int Port;
        private string FromIP;
        public string InvoiceId;
        private string LineIDs;
        public RBSFHMMoveType MoveType;
        private bool KeepOpenForPayment;
        private bool FlagCheckCardTransaction = false;
        private bool FlagGetLastReceiptNum = false;
        public bool KeepPrinting = false;
        public string BillID;
        private bool FirstMessage;

        private List<RBSTAXFail> RBSTAXFails = new List<RBSTAXFail>();

        private Socket socket;
        private List<SocketAsyncEventArgs> ListSocketEventArgs = new List<SocketAsyncEventArgs>();
        private int CurrentPartNum = 0;


        public delegate void PrintEndedHandler();
        public event PrintEndedHandler PrintEndedEvent;

        public RBSFHMRestaurant(string message,string ip,int port,string fromIP,string invoiceId,string lineIDs, RBSFHMMoveType moveType , bool keepOpenForPayment,string billID,bool FirstMessage) {
            this.Message = message;
            this.IP = ip;
            this.Port = port;
            RBSTAXFails = new List<RBSTAXFail>();
            FromIP = fromIP;
            InvoiceId = invoiceId;
            LineIDs = lineIDs;
            MoveType = moveType;
            KeepOpenForPayment = keepOpenForPayment;
            BillID = billID;
            this.FirstMessage = FirstMessage;
        }
        public string ErrorMessage { get; set; }

        public void PrintReceipt()
        {
            try
            {
                IPAddress ipAddress = IPAddress.Parse(IP);
                IPEndPoint ipEndPoint = new IPEndPoint(ipAddress, Port);


                if (socket == null) socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                if (!socket.Connected)
                {
                    SocketAsyncEventArgs e = new SocketAsyncEventArgs();
                    e.RemoteEndPoint = ipEndPoint;
                    e.UserToken = socket;
                    e.Completed += new EventHandler<SocketAsyncEventArgs>(socket_Completed);
                    ListSocketEventArgs.Add(e);      // Add to a list so we dispose all the sockets when the timer ticks.
                    socket.ConnectAsync(e);
                }
            }
            catch (System.Exception ex)
            {
                SetErrorMessage(ex.Message);
            }
        }
        public void Payment(string message, bool keepOpenForPayment,bool FirstMessage) {
            this.Message = message;
            KeepOpenForPayment = keepOpenForPayment;
            MoveType = RBSFHMMoveType.CloseTable;
            CurrentPartNum = 0;
            FlagCheckCardTransaction = false;
            FlagGetLastReceiptNum = false;
            this.FirstMessage = FirstMessage;

            if (socket == null || !socket.Connected)
            {
                socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                IPAddress ipAddress = IPAddress.Parse(IP);
                IPEndPoint ipEndPoint = new IPEndPoint(ipAddress, Port);
                SocketAsyncEventArgs e = new SocketAsyncEventArgs();
                e.RemoteEndPoint = ipEndPoint;
                e.UserToken = socket;
                e.Completed += new EventHandler<SocketAsyncEventArgs>(socket_Completed);
                ListSocketEventArgs.Add(e);      // Add to a list so we dispose all the sockets when the timer ticks.
                socket.ConnectAsync(e);
            }
            else {
                SendItem(0);
            }
        }
        private void socket_Completed(object sender, SocketAsyncEventArgs e)
        {
            if (e.ConnectSocket != null)     //if there's a connection Add its info to a listview
            {
                SendItem(0);
            }
            else {
                PrintEnded = true;
                KeepPrinting = false;
                ReceiptEnded();
            }

        }
        private void SendCallback(object sender, SocketAsyncEventArgs e)
        {
            if (e.SocketError == SocketError.Success)
            {

                ReceiveBytes();

            }
            else
            {
                PrintEnded = true;
                KeepPrinting = false;
                ReceiptEnded();
            }
        }
        private void SendItem(int part)
        {
            try
            {
                string[] items = Message.Split(';');

                if(UseLogger) BrowserForm.Log(items[part]);

                byte[] buffer = System.Text.Encoding.Default.GetBytes(items[part]);
                SocketAsyncEventArgs sae = new SocketAsyncEventArgs();
                sae.SetBuffer(buffer, 0, buffer.Length);
                sae.Completed += new EventHandler<SocketAsyncEventArgs>(SendCallback);

                var res = socket.SendAsync(sae);
                Common.browser.Load("javascript:RBSFHMRestaurantLoggerPost('" + items[part] + "')");
            }
            catch(System.Exception ex)
            {
                SetErrorMessage(ex.Message);
            }

        }

        private void ReceiptEnded()
        {
            try
            {
                if (socket != null && socket.Connected)
                    socket.Shutdown(SocketShutdown.Send);
                if (socket != null)
                    socket.Close();
                socket = null;


                //?????? Ended Data


            }
            catch (System.Exception ex)
            {
            }
            finally {
                PrintEndedEvent?.Invoke();
            }
        }
        public void CloseConnection() {
            try
            {
                if (socket != null && socket.Connected)
                    socket.Shutdown(SocketShutdown.Send);
                if (socket != null)
                    socket.Close();
                socket = null;

            }
            catch (System.Exception ex)
            {
            }
        }

        public class StateObject
        {
            public Socket socket = null;
            public const int BufferSize = 1024;
            public byte[] buffer = new byte[BufferSize];
            public List<byte> bytes = new List<byte>();
        }
        private void ReceiveBytes()
        {
            try
            {
                // create the state object
                StateObject state = new StateObject();
                state.socket = socket;

                // begin receiving data from the remote device
                socket.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0, new AsyncCallback(ReceiveCallback), state);
            }
            catch (Exception e)
            {
                SetErrorMessage(e.Message);
            }
        }
        private void ReceiveCallback(IAsyncResult ar)
        {
            try
            {
                string[] items = Message.Split(';');

                StateObject state = (StateObject)ar.AsyncState;
                Socket client = state.socket;
                int bytesRead = client.EndReceive(ar);
                for (int bufferIndex = 0; bufferIndex < bytesRead; bufferIndex++)
                {
                    state.bytes.Add(state.buffer[bufferIndex]);
                }


                //ReadData ...
                var res = Encoding.UTF8.GetString(state.bytes.ToArray(), 0, state.bytes.Count());

                Common.browser.Load("javascript:RBSFHMRestaurantLoggerPost('" + res + "')");

                if (UseLogger) BrowserForm.Log(res);

                string result = res.Split('/').FirstOrDefault();

                if (CurrentPartNum == 0 && result != "00") {
                    if (result != "5C" && result != "C9") { 
                        CheckRBSTaxErrorCode(result);
                        if (MoveType == RBSFHMMoveType.CloseTable || MoveType == RBSFHMMoveType.TakeAway)
                        {
                            if (!FirstMessage)
                            {
                                KeepPrinting = true;
                            }
                        }
                        ReceiptEnded();
                        return;
                    }
                }

                if (CurrentPartNum == items.Count() - 1)
                {
                    PrintEnded = true;

                    //00 --> OK
                    //2C --> 44  --> Paper end.
                    //5C --> 92  --> Same Header lines.
                    //C9 --> 201 --> Closure of a Table is in progress
                    KeepPrinting = false;
                    //
                    if (result == "00" || result == "2C" || result == "5C" || result == "C9")
                    {
                        if (FlagCheckCardTransaction) {
                            string[] resCard = res.Split('/');
                            if (resCard.Length > 4) {
                                if (resCard[3] == "2") {
                                    KeepPrinting = true;
                                    CheckRBSTaxErrorCode(resCard[4]);
                                    MoveType = RBSFHMMoveType.None;
                                }   
                            }
                        }

                        if (MoveType == RBSFHMMoveType.Order)
                        {
                            string BillCode = "";
                            if (FlagGetLastReceiptNum == false)
                            {
                                GetLastOrderNum();
                                return;
                            }
                            else
                            {
                                var resitems = res.Split('/');
                                if (resitems.Length > 8)
                                {
                                    BillCode = resitems[8];
                                }
                            }

                            if (RBSTAXFails.Count == 0)
                            {
                                Common.browser.Load("javascript:OrderDigiNetCompleted('" + Guid.NewGuid() + "','"+ BillCode + "','" + InvoiceId + "','" + LineIDs + "','" + Message + "','','" + FromIP + "')");
                            }
                            else
                            {
                                var errors = JsonConvert.SerializeObject(RBSTAXFails);
                                var ScriptText = "javascript:OrderDigiNetCompletedWithErrors('" + Guid.NewGuid() + "','"+ BillCode + "','" + InvoiceId + "','" + LineIDs + "','" + Message + "','','" + FromIP + "','" + errors + "')";
                                Common.browser.Load(ScriptText);

                            }
                        }
                        if (MoveType == RBSFHMMoveType.Return)
                        {
                            string BillCode = "";
                            if (FlagGetLastReceiptNum == false)
                            {
                                GetLastOrderNum();
                                return;
                            }
                            else
                            {
                                var resitems = res.Split('/');
                                if (resitems.Length > 6)
                                {
                                    BillCode = resitems[6];
                                }
                            }

                            if (RBSTAXFails.Count == 0)
                            {
                                Common.browser.Load("javascript:OrderCancelDigiNetCompleted('" + Guid.NewGuid() + "','"+ BillCode + "','" + InvoiceId + "','" + LineIDs + "','" + Message + "','','" + FromIP + "')");
                            }
                            else
                            {
                                var errors = JsonConvert.SerializeObject(RBSTAXFails);
                                var ScriptText = "javascript:OrderCancelDigiNetCompletedWithErrors('" + Guid.NewGuid() + "','"+ BillCode + "','" + InvoiceId + "','" + LineIDs + "','" + Message + "','','" + FromIP + "','" + errors + "')";
                                Common.browser.Load(ScriptText);

                            }
                        }
                        if (MoveType == RBSFHMMoveType.CloseTable) {
                            if (result == "C9") {
                                CheckCardTransaction();
                                return;
                            }
                            string BillCode = "";
                            if (FlagGetLastReceiptNum == false)
                            {
                                GetLastReceiptNum();
                                return;
                            }
                            else {
                                var resitems = res.Split('/');
                                if (resitems.Length > 10) {
                                    BillCode = resitems[9];
                                }
                            }

                            Common.browser.Load("javascript:RBSFHMProceedPayment('" + FromIP + "' , '" + InvoiceId + "')");
                            if (!KeepOpenForPayment)
                            {
                                Common.browser.Load("javascript:RBSFHMCloseTableCompleted('" + InvoiceId + "','" + FromIP + "','" + LineIDs + "','" + BillCode + "')");
                            }
                            else
                            {
                                Common.browser.Load("javascript:RBSFHMContinuePayment('" + FromIP + "')");
                            }
                        }
                        if (MoveType == RBSFHMMoveType.TakeAway) {
                            if (result == "C9")
                            {
                                CheckCardTransaction();
                                return;
                            }
                            string BillCode = "";
                            if (FlagGetLastReceiptNum == false)
                            {
                                GetLastReceiptNum();
                                return;
                            }
                            else
                            {
                                var resitems = res.Split('/');
                                if (resitems.Length > 10)
                                {
                                    BillCode = resitems[9];
                                }
                            }

                            Common.browser.Load("javascript:RBSFHMProceedPayment('" + FromIP + "' , '" + InvoiceId + "')");
                            if (!KeepOpenForPayment)
                            {
                                if (RBSTAXFails.Count == 0)
                                {
                                    Common.browser.Load("javascript:BillDigiNetCompleted('" + Guid.NewGuid() + "','"+ BillCode +"','" + InvoiceId + "','" + LineIDs + "','" + Message + "','','" + FromIP + "')");
                                }
                                else
                                {
                                    var errors = JsonConvert.SerializeObject(RBSTAXFails);
                                    Common.browser.Load("javascript:RBSTaxBillCompletedWithErrors('" + Guid.NewGuid() + "','"+ BillCode +"','" + InvoiceId + "','" + LineIDs + "','" + Message + "','','" + FromIP + "','" + errors + "')");
                                }
                            }
                            else
                            {
                                Common.browser.Load("javascript:RBSFHMContinuePayment('" + FromIP + "',)");
                            }
                        }
                        if (MoveType == RBSFHMMoveType.Cancel)
                        {
                            string BillCode = "";
                            if (FlagGetLastReceiptNum == false)
                            {
                                GetLastReceiptNum();
                                return;
                            }
                            else
                            {
                                var resitems = res.Split('/');
                                if (resitems.Length > 10)
                                {
                                    BillCode = resitems[9];
                                }
                            }
                            Common.browser.Load("javascript:RBSFHMCancelCompleted('" + InvoiceId + "','" + FromIP + "','" + LineIDs + "','"+ BillCode + "','" + BillID + "')");
                        }
                        if (MoveType == RBSFHMMoveType.Transfer)
                        {
                            string BillCode = "";
                            if (FlagGetLastReceiptNum == false)
                            {
                                GetLastOrderNum();
                                return;
                            }
                            else
                            {
                                var resitems = res.Split('/');
                                if (resitems.Length > 8)
                                {
                                    BillCode = resitems[8];
                                }
                            }

                            Common.browser.Load("javascript:RBSFHMTransferTableComplete('" + LineIDs + "','" + BillID + "','" + FromIP + "','" + BillCode + "','" + InvoiceId + "')");
                        }
                        if (MoveType == RBSFHMMoveType.InvoicePayment)
                        {
                            if (result == "C9")
                            {
                                CheckCardTransaction();
                                return;
                            }

                            Common.browser.Load("javascript:RBSFHMProceedPayment('" + FromIP + "' , '" + InvoiceId + "')");
                            Common.browser.Load("javascript:RBSFHMInvoicePaymentCompleted('" + FromIP + "', '" + InvoiceId + "')");
                        }
                        if (MoveType == RBSFHMMoveType.ClearTable){
                            string BillCode = "";
                            if (FlagGetLastReceiptNum == false)
                            {
                                GetLastReceiptNum();
                                return;
                            }
                            else
                            {
                                var resitems = res.Split('/');
                                if (resitems.Length > 10)
                                {
                                    BillCode = resitems[9];
                                    Common.browser.Load("javascript:RBSFHMCloseTableCompleted('" + InvoiceId + "','" + FromIP + "','" + LineIDs + "','" + BillCode + "','Cancel')");
                                }
                            }
                        }
                    }
                    else
                    {
                        if (MoveType == RBSFHMMoveType.CloseTable || MoveType == RBSFHMMoveType.TakeAway) {
                            if (!FirstMessage) {
                                KeepPrinting = true;
                            }
                        }
                        //if (MoveType != RBSFHMMoveType.Return && MoveType != RBSFHMMoveType.Order)
                        //{
                        //    KeepPrinting = true;
                        //}
                        CheckRBSTaxErrorCode(result);
                    }
                }
                else
                {
                    if (result != "00")
                    {
                        String[] separated = Message.Split(';');
                        RBSTAXFails.Add(new RBSTAXFail() { PartNum = CurrentPartNum - 1, Error = RBSTaxGetErrorMessage(result), Description = separated[CurrentPartNum + 1] });
                    }
                }

                CurrentPartNum++;
                if (CurrentPartNum < items.Count())
                    SendItem(CurrentPartNum);
                else
                {
                    ReceiptEnded();
                }
            }
            catch (Exception e)
            {
                SetErrorMessage(e.Message);
            }
        }

        private void SetErrorMessage(string error) { 
            ErrorMessage = error;

        }

        private string RBSTaxGetErrorMessage(string code)
        {
            string message = "RBS Error : " + code;
            int intCode = 0;
            bool successfullyParsed = false;
            try
            {
                intCode = Convert.ToInt32(code, 16);
                successfullyParsed = true;
            }
            catch (System.Exception ex) {
                successfullyParsed = false;
            }

            //bool successfullyParsed = int.TryParse(code, out intCode);
            if (successfullyParsed)
            {
                if (intCode == 1) message = "The protocol command expects more fields";
                if (intCode == 2) message = "A protocol command field is longer than expected";
                if (intCode == 3) message = "A protocol command filed is smaller than expected";
                if (intCode == 4) message = "Check the protocol command fields";
                if (intCode == 5) message = "Check the protocol command fields";
                if (intCode == 6) message = "The protocol command is not supported";
                if (intCode == 7) message = "The PLU code doesn’t exist";
                if (intCode == 8) message = "The DPT Code doesn’t exist";
                if (intCode == 9) message = "Wrong VAT code";
                if (intCode == 10) message = "The Clerk’s index number doesn’t exist";
                if (intCode == 11) message = "Wrong Clerk’s password";
                if (intCode == 12) message = "The payment code doesn’t exist";
                if (intCode == 13) message = "The requested Fiscal record doesn’t exist";
                if (intCode == 14) message = "The requested Fiscal record type doesn’t exist";
                if (intCode == 15) message = "Printing type Error";
                if (intCode == 16) message = "The day is open, issue a Z- Report first";
                if (intCode == 17) message = "Disconnect Jumpers first";
                if (intCode == 18) message = "Wrong TIME, not allowed operation";
                if (intCode == 19) message = "CAN NOT PERFORM SALES";
                if (intCode == 20) message = "A transaction is open, close the transaction first";
                if (intCode == 21) message = "Receipt in Payment";
                if (intCode == 22) message = "CASH IN/OUT transaction in progress";
                if (intCode == 23) message = "Wrong VAT rate";
                if (intCode == 24) message = "Price Error";
                if (intCode == 25) message = "The online communication of the ECR is ON";
                if (intCode == 26) message = "The ECR is busy, try again later";
                if (intCode == 27) message = "Invalid sales operation";
                if (intCode == 28) message = "Invalid Discount/Markup type";
                if (intCode == 29) message = "No more headers can be programmed";
                if (intCode == 30) message = "A user’s report is open";
                if (intCode == 31) message = "A user’s report is open";
                if (intCode == 32) message = "The Fiscal Memory has no transactions";
                if (intCode == 33) message = "Discount/Markup index number Error";
                if (intCode == 34) message = "You can’t program any more PLUs";
                if (intCode == 35) message = "Error in BMP Data";
                if (intCode == 36) message = "The BMP index number doesn’t exist";
                if (intCode == 37) message = "The category index number doesn’t exist";
                if (intCode == 38) message = "Printer ID index Error";
                if (intCode == 39) message = "Error printing type";
                if (intCode == 40) message = "Unit ID index Error";
                if (intCode == 41) message = "No more sales can be performed";
                if (intCode == 42) message = "Keyboard Error-or keyboard disconnected";
                if (intCode == 43) message = "Battery Error.";
                if (intCode == 44) message = "Paper end.";
                if (intCode == 45) message = "Error with the cutter.";
                if (intCode == 46) message = "The printer is disconnected.";
                if (intCode == 47) message = "Printer overheated.";
                if (intCode == 48) message = "The fiscal memory is offline.";
                if (intCode == 49) message = "Fatal Error.";
                if (intCode == 50) message = "Jumpers are still ON.";
                if (intCode == 51) message = "The printer’s cover is open.";
                if (intCode == 52) message = "No more VATs can be programmed";
                if (intCode == 53) message = "No more lines.";
                if (intCode == 54) message = "Into Menu.";
                if (intCode == 55) message = "No Ticket Discount.";
                if (intCode == 56) message = "Not supported.";
                if (intCode == 57) message = "Access not allowed for current clerk.";
                if (intCode == 58) message = "Wrong Baud Rate.";
                if (intCode == 60) message = "Inactive PLU.";
                if (intCode == 61) message = "Fiscal Memory is full.";
                if (intCode == 62) message = "Wrong Coupon Index";
                if (intCode == 63) message = "Larger Quantity Value than the one allowed.";
                if (intCode == 64) message = "Inactive payment type.";
                if (intCode == 65) message = "Inactive DPT.";
                if (intCode == 66) message = "No more Stock";
                if (intCode == 67) message = "Serial port error";
                if (intCode == 68) message = "Cannot open Drawer.";
                if (intCode == 69) message = "Open ticket";
                if (intCode == 70) message = "There is not an open receipt.";
                if (intCode == 71) message = "Error in SD reports.";
                if (intCode == 72) message = "Inactive ticket";
                if (intCode == 73) message = "Wrong coupon";
                if (intCode == 74) message = "Discount/Markup limit is exceeded.";
                if (intCode == 75) message = "Zero Discount/Markup.";
                if (intCode == 76) message = "Description is blank.";
                if (intCode == 77) message = "Wrong barcode";
                if (intCode == 78) message = "Wrong Key code.";
                if (intCode == 79) message = "EJ Transfer";
                if (intCode == 81) message = "The SD is full.";
                if (intCode == 82) message = "The SD is old.";
                if (intCode == 83) message = "Negative Receipt total.";
                if (intCode == 84) message = "Clientindex Error";
                if (intCode == 85) message = "Wrong Client code.";
                if (intCode == 86) message = "Out of limits bonus.";
                if (intCode == 87) message = "Wrong Promotion link index.";
                if (intCode == 88) message = "WORD NOT ALLOWED";
                if (intCode == 89) message = "Error in barcode data.";
                if (intCode == 90) message = "Change is not allowed for this Payment type.";
                if (intCode == 91) message = "Must insert the payment amount.";
                if (intCode == 92) message = "Same Header lines.";
                if (intCode == 93) message = "In Error Message.";
                if (intCode == 94) message = "Not found SD data.";
                if (intCode == 95) message = "There is no more data to read from SD.";
                if (intCode == 96) message = "Set the limit to read SD data.";
                if (intCode == 97) message = "Receipt sales amount exceeded.";
                if (intCode == 98) message = "Daily sales amount exceeded.";
                if (intCode == 99) message = "The SD is full.";
                if (intCode == 100) message = "Wrong ticket index";
                if (intCode == 101) message = "Fiscal Communication problem.";
                if (intCode == 102) message = "Cannot transfer payment amount";
                if (intCode == 104) message = "LCD disconnection";
                if (intCode == 105) message = "SD disconnection";
                if (intCode == 106) message = "Battery Error";
                if (intCode == 107) message = "NAND Memory full";
                if (intCode == 108) message = "Wrong TIN";
                if (intCode == 109) message = "Empty EJ";
                if (intCode == 110) message = "Invalid IP";
                if (intCode == 111) message = "Invalid refund";
                if (intCode == 112) message = "Invalid void";
                if (intCode == 113) message = "Exceed amount limit";
                if (intCode == 114) message = "Null header";
                if (intCode == 115) message = "Inactive clerk";
                if (intCode == 116) message = "Barcode error";
                if (intCode == 117) message = "Need program TIN";
                if (intCode == 118) message = "SD memory need format";
                if (intCode == 119) message = "Wrong Date";
                if (intCode == 120) message = "Wrong Time";
                if (intCode == 121) message = "Call technician";
                if (intCode == 122) message = "Cannot open EJ file";
                if (intCode == 123) message = "Cannot write EJ file";
                if (intCode == 124) message = "Cannot read EJ file";
                if (intCode == 125) message = "Wrong GSIS AES Key";
                if (intCode == 126) message = "Cannot change";
                if (intCode == 127) message = "Ethernet communication error";
                if (intCode == 128) message = "Send GSIS error";
                if (intCode == 129) message = "Fiscal Blank";
                if (intCode == 130) message = "Web Services file error";
                if (intCode == 132) message = "Active GPRS";
                if (intCode == 133) message = "Wrong Activate Services Key";
                if (intCode == 134) message = "Cannot open file in SD";
                if (intCode == 135) message = "Cannot write file in SD";
                if (intCode == 136) message = "Need quantity";
                if (intCode == 137) message = "Receipt closed";
                if (intCode == 138) message = "Unauthorized function";
                if (intCode == 139) message = "Already used coupon";
                if (intCode == 140) message = "Invalid key for services’ activation or already activated";
                if (intCode == 141) message = "Invalid client coupon";
                if (intCode == 142) message = "Inactive client";
                if (intCode == 143) message = "Barcode error (not valid EAN)";
                if (intCode == 144) message = "Same client code";
                if (intCode == 145) message = "Reserved";
                if (intCode == 146) message = "Error in header FM write";
                if (intCode == 147) message = "No more clients";
                if (intCode == 148) message = "Reserved";
                if (intCode == 149) message = "Reserved";
                if (intCode == 150) message = "Reserved";
                if (intCode == 151) message = "Inactive Ethernet";
                if (intCode == 152) message = "Client not found";
                if (intCode == 153) message = "Reserved";
                if (intCode == 154) message = "Reserved";
                if (intCode == 155) message = "Reserved";
                if (intCode == 156) message = "Reserved";
                if (intCode == 157) message = "Reserved";
                if (intCode == 158) message = "Reserved";
                if (intCode == 159) message = "Reserved";
                if (intCode == 160) message = "Reserved";
                if (intCode == 161) message = "Reserved";
                if (intCode == 162) message = "Read SD File";
                if (intCode == 163) message = "Need to confirm new invoice row";
                if (intCode == 164) message = "Open EJ";
                if (intCode == 165) message = "Write EJ";
                if (intCode == 166) message = "Wait to synchronize with date / time received from GSIS";
                if (intCode == 167) message = "Unregister User(Link4All)";
                if (intCode == 168) message = "Zero receipt total";
                if (intCode == 169) message = "Wrong or does not exist activation code";
                if (intCode == 170) message = "BUSY in send to GSIS";
                if (intCode == 171) message = "Wrong Code";
                if (intCode == 172) message = "Reserved";
                if (intCode == 173) message = "Programming Memory Index Error";
                if (intCode == 174) message = "Reserved";
                if (intCode == 175) message = "Inactive Table";
                if (intCode == 176) message = "Need subtotal first";
                if (intCode == 177) message = "Same GSIS AES Key";
                if (intCode == 178) message = "Reserved";
                if (intCode == 179) message = "Reserved";
                if (intCode == 180) message = "Expired Licence(Link4All)";
                if (intCode == 181) message = "Inactive Invoice Type";
                if (intCode == 182) message = "Reserved";
                if (intCode == 183) message = "Wrong / Missing Invoice Issuer Information";
                if (intCode == 184) message = "SD Disconnection";
                if (intCode == 185) message = "Not valid SD";
                if (intCode == 186) message = "EFTPOS Payment Failed";
                if (intCode == 187) message = "EFTPOS Fail to read transactions";
                if (intCode == 188) message = "Inactive EFTPOS";
                if (intCode == 189) message = "Reserved IP Address";
                if (intCode == 190) message = "FM in pause";
                if (intCode == 191) message = "Inactive GSIS send";
                if (intCode == 192) message = "Wrong company category";
                if (intCode == 193) message = "EFTPOS payment in progress";
                if (intCode == 194) message = "Wrong EFTPOS transaction type";
                if (intCode == 195) message = "Open Table";
                if (intCode == 196) message = "Close Table";
                if (intCode == 197) message = "Abort by user";
                if (intCode == 198) message = "Close Table failure";
                if (intCode == 199) message = "Info of transaction lines available for transfer between tables";
                if (intCode == 200) message = "Not Used";
                if (intCode == 201) message = "Closure of a Table is in progress";
                if (intCode == 202) message = "No more lines available for transfer between tables";
                if (intCode == 203) message = "Not open day";
                if (intCode == 204) message = "Fee category is inactive";

                message = code + " " + message;
            }
            return message;
        }

        private void CheckRBSTaxErrorCode(string code)
        {
            string message = RBSTaxGetErrorMessage(code);

            Common.browser.Load("javascript:PostDBInvoiceBillError('','','" + InvoiceId + "','" + Message + "','" + message + "');");
            if (FromIP == null || FromIP == "")
            {
                Common.browser.Load("javascript:showNotification('Error on printing with RBS. " + message + "');");
            }
            else
            {
                Escpos escpos = new Escpos();
                escpos.SendMessage(FromIP, "ErrorMessage^^^" + message, sendPort);
            }

            Common.browser.Load("javascript:RBSFHMContinuePayment('" + FromIP + "')");
        }
        private void CheckCardTransaction() {
            Message = "5/99/0////";
            CurrentPartNum = 0;
            FlagCheckCardTransaction = true;
            SendItem(0);
        }
        private void GetLastReceiptNum() {
            Message = "0/5/5/";
            CurrentPartNum = 0;
            FlagGetLastReceiptNum = true;
            SendItem(0);
        }
        private void GetLastOrderNum() {
            Message = "X";
            CurrentPartNum = 0;
            FlagGetLastReceiptNum = true;
            SendItem(0);
        }
    }
}
