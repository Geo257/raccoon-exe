﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace CefSharp.MinimalExample.WinForms
{
    public class SocketListener
    {
        // Incoming data from the client.  
        public string data = null;
        private System.Timers.Timer timer;

        public string sendPort = "9700";
        //9702

        public void StartListening()
        {
            // Data buffer for incoming data.  
            byte[] bytes = new Byte[4096];

            // Establish the local endpoint for the socket.  
            // Dns.GetHostName returns the name of the
            // host running the application.  
            IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());
            IPAddress ipAddress = ipHostInfo.AddressList[0];

            string xxAddress = "";
            foreach (var ipl in ipHostInfo.AddressList) {
                if (ipl.ToString().Substring(0,3) == "192" || ipl.ToString().Substring(0, 2) == "10" || ipl.ToString().Substring(0, 3) == "172") {
                    ipAddress = ipl;
                }
            }

            try
            {
                string localIP = ConfigurationManager.AppSettings["LocalIP"].ToString();
                if (localIP != "0.0.0.0") {
                    ipAddress = System.Net.IPAddress.Parse(localIP);
                }
            }
            catch (System.Exception ex) { }

        

            IPEndPoint localEndPoint = new IPEndPoint(ipAddress, 9700);

            // Create a TCP/IP socket.  
            Socket listener = new Socket(ipAddress.AddressFamily,
                SocketType.Stream, ProtocolType.Tcp);

            // Bind the socket to the local endpoint and
            // listen for incoming connections.  
            try
            {
                listener.Bind(localEndPoint);
                listener.Listen(10);

                // Start listening for connections.  
                while (true)
                {
                    Console.WriteLine("Waiting for a connection...");
                    // Program is suspended while waiting for an incoming connection.  
                    Socket handler = listener.Accept();
                    data = "";

                    try
                    {
                        //int bytesRec = handler.Receive(bytes);

                        //while (bytesRec != 0) {
                        //    data += Encoding.UTF8.GetString(bytes, 0, bytesRec);
                        //    bytesRec = handler.Receive(bytes);
                        //}

                        var lengthData = new byte[4];
                        var lengthBytesRead = 0;

                        Stopwatch s = new Stopwatch();
                        s.Start();
                        while (lengthBytesRead < lengthData.Length && s.Elapsed < TimeSpan.FromSeconds(30000))
                        {
                            var read = handler.Receive(lengthData, lengthBytesRead, lengthData.Length - lengthBytesRead, SocketFlags.None);
                            lengthBytesRead += read;
                        }
                        s.Stop();

                        Array.Reverse(lengthData);
                        var length = BitConverter.ToInt32(lengthData, 0);


                        if (length > 0)
                        {
                            var imageData = new byte[length];
                            var imageBytesRead = 0;

                            while (imageBytesRead < imageData.Length)
                            {
                                var read = handler.Receive(imageData, imageBytesRead, imageData.Length - imageBytesRead, SocketFlags.None);
                                imageBytesRead += read;
                            }
                            data += Encoding.UTF8.GetString(imageData, 0, length);
                        }

                        //    int bytesRec = handler.Receive(bytes);
                        //while (bytesRec > 0)
                        //{
                        //    data += Encoding.UTF8.GetString(bytes, 0, bytesRec);
                        //    if (handler.Available > 0)
                        //    {
                        //        bytesRec = handler.Receive(bytes);
                        //    }
                        //    else
                        //    {
                        //        bytesRec = 0;
                        //    }
                        //}

                        byte[] answerBytes = Encoding.ASCII.GetBytes("OK");

                        int reqLen = answerBytes.Length;
                        int reqLenH2N = IPAddress.HostToNetworkOrder(reqLen);
                        byte[] reqLenArray = BitConverter.GetBytes(reqLenH2N);
                        handler.Send(reqLenArray);
                        handler.Send(answerBytes, answerBytes.Length, System.Net.Sockets.SocketFlags.None);

                        String[] arrayString = data.Split(new string[] { "^^^" }, StringSplitOptions.None);
                        String messageType = arrayString[0];

                        if (messageType.Contains("ReturnDiginetSign"))
                        {
                            String sign = arrayString[1];
                            String message = arrayString[2];
                            String InvoiceId = arrayString[3];
                            String lineIDs = arrayString[4];

                            String amessage = message.Replace("\n", "\\n").Replace("\r", "\\r").Replace("\'", "\\'").Replace("\u001B", "\\u001B").Replace("\u0000", "\\u0000").Replace("\u0001", "\\u0001").Replace("\0", "\\0");

                            Common.browser.Load("javascript:PrintBillReturnFromDiginetServer('" + sign + "','" + amessage + "','" + InvoiceId + "','" + lineIDs + "')");
                        }
                        if (messageType.Contains("RbsClientPrint"))
                        {
                            if (Common.ws != null)
                            {
                                string returnErrorIP = arrayString[2];
                                String SendMessage = "ErrorMessage^^^Η Ταμειακή χρησιμοποιείται , ξαναπροσπάθησε!";
                                Escpos escpos = new Escpos();
                                escpos.SendMessage(returnErrorIP, SendMessage, sendPort);
                                //Return Message error
                            }
                            else
                            {
                                Message = arrayString[1];
                                ReturnIP = arrayString[2];
                                String cashierIP = arrayString[3];
                                LineIDs = "";
                                BillCode = "";
                                if (arrayString.Length > 3)
                                    LineIDs = arrayString[4];
                                if (arrayString.Length > 4)
                                    BillCode = arrayString[5];
                                if (arrayString.Length > 5)
                                    InvoiceID = arrayString[6];



                                Common.ws = new WebSocketSharp.WebSocket("ws://" + cashierIP);
                                Common.ws.OnMessage += new EventHandler<WebSocketSharp.MessageEventArgs>(OnRbsMessage);
                                Common.ws.OnError += new EventHandler<WebSocketSharp.ErrorEventArgs>(OnRbsError);
                                Common.ws.Connect();

                                string[] parts = Message.Split(';');
                                PartsCount = parts.Length;
                                PartsNum = 1;
                                Common.ws.Send(parts[0]);
                            }
                        }
                        if (messageType.Contains("GetDiginetSign"))
                        {
                            String eLine = arrayString[1];
                            int index = eLine.IndexOf(';');
                            StringBuilder sb = new StringBuilder(eLine);
                            sb.Remove(index, 1);
                            eLine = sb.ToString();

                            index = eLine.IndexOf(';');
                            sb = new StringBuilder(eLine);
                            sb.Remove(index, 1);
                            eLine = sb.ToString();

                            index = eLine.IndexOf(';');
                            sb = new StringBuilder(eLine);
                            sb.Remove(index, 1);
                            eLine = sb.ToString();

                            index = eLine.IndexOf(';');
                            sb = new StringBuilder(eLine);
                            sb.Remove(index, 1);
                            eLine = sb.ToString();

                            index = eLine.IndexOf(';');
                            sb = new StringBuilder(eLine);
                            sb.Remove(index, 1);
                            eLine = sb.ToString();

                            String message = arrayString[2];
                            String invoiceID = arrayString[3];
                            String lineIDs = arrayString[4];
                            String returnIP = arrayString[5];

                            String amessage = message.Replace("\n", "\\n").Replace("\r", "\\r").Replace("\'", "\\'").Replace("\u001B", "\\u001B").Replace("\u0000", "\\u0000").Replace("\u0001", "\\u0001").Replace("\0", "\\0");

                            Common.browser.Load("javascript:GetBillCodeForDiginetPrint('" + eLine + "','" + amessage + "','" + invoiceID + "','" + lineIDs + "','" + returnIP + "')");
                        }
                        if (messageType.Contains("KDSInvoiceAnswer"))
                        {
                            String invoiceID = arrayString[1];
                            String StationID = arrayString[2];
                            Common.browser.Load("javascript:KDSOfflinePrintAnswer('" + invoiceID + "','" + StationID + "')");
                        }
                        if (messageType.Contains("PostDataToService")) {
                            String message = arrayString[1];
                            String fromIP = arrayString[2];
                            message = message.Replace( "\\\"" ,  "\\\\\"");
                            Common.browser.Load(@"javascript:PostDataFromClient('" + message + "','"+ fromIP + "')");
                        }


                        handler.Shutdown(SocketShutdown.Both);
                        handler.Close();
                    }
                    catch (System.Exception ex) {
                        //BrowserForm.Log("Socket Listener Error : " + ex.Message);
                        if (handler != null) {
                            handler.Shutdown(SocketShutdown.Both);
                            handler.Close();
                        }
                    }

                }

            }
            catch (Exception e)
            { 
                Console.WriteLine(e.ToString());
            }

            Console.WriteLine("\nPress ENTER to continue...");
            Console.Read();

        }

        #region Rbs Printing
        private string Message;
        private string ReturnIP;
        private int PartsCount;
        private int PartsNum;
        private string LineIDs;
        private string BillCode;
        private string InvoiceID;
        private void OnRbsMessage<TEventArgs>(object sender, TEventArgs e)
        {
            if (PartsNum < PartsCount)
            {
                PartsNum++;
                String[] separated = Message.Split(';');
                Common.ws.Send(separated[PartsNum - 1]);
                if (PartsNum == PartsCount)
                {
                    if (e is WebSocketSharp.MessageEventArgs)
                    {
                        string res = (e as WebSocketSharp.MessageEventArgs).Data;
                        if (res.Substring(0, 2) == "00")
                        {

                            //InvoiceBill Send Data
                            //Common.browser.Load("javascript:RBSPrintCompleted();");
                            Common.browser.Load("javascript:CardlinkDiginetPrintCompletedWithParams2('" + Guid.NewGuid().ToString() + "','" + BillCode + "','" + InvoiceID + "','" + LineIDs + "', 'RBS Print');");
                            String SendMessage = "RbsClientCompleted";
                            Escpos escpos = new Escpos();
                            escpos.SendMessage(ReturnIP, SendMessage, sendPort);
                        }
                        else {
                            Escpos escpos = new Escpos();
                            escpos.SendMessage(ReturnIP, "ErrorMessage^^^Error on printing with RBS. Error Code : " + res , sendPort);
                        }
                    }
                    else
                    {
                        Escpos escpos = new Escpos();
                        escpos.SendMessage(ReturnIP, "ErrorMessage^^^Error on printing with RBS. Null EventArgs", sendPort);
                    }

                    Common.ws.Close();
                    Common.ws = null;
                }
            }
        }
        private void OnRbsError<TEventArgs>(object sender, TEventArgs e)
        {
            //if (e is WebSocketSharp.ErrorEventArgs)
            //    System.Windows.Forms.MessageBox.Show((e as WebSocketSharp.ErrorEventArgs).Message);
            //else
            //    System.Windows.Forms.MessageBox.Show("Error on printing with RBS.");

            if (e is WebSocketSharp.ErrorEventArgs)
                Common.browser.Load("javascript:showNotification('" + (e as WebSocketSharp.ErrorEventArgs).Message + "');");
            else
                Common.browser.Load("javascript:showNotification('Error on printing with RBS.');");

            try
            {
                Common.ws.Close();
            }
            catch (System.Exception e1) { }
            Common.ws = null;
        }
        #endregion
    }

}
