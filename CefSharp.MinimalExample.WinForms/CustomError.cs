﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using CefSharp.Handler;

namespace CefSharp.MinimalExample.WinForms {
	public abstract class BaseRequestEventArgs : System.EventArgs {
		protected BaseRequestEventArgs(IWebBrowser chromiumWebBrowser, IBrowser browser) {
			ChromiumWebBrowser = chromiumWebBrowser;
			Browser = browser;
		}

		public IWebBrowser ChromiumWebBrowser { get; private set; }
		public IBrowser Browser { get; private set; }
	}

	public class OnCertificateErrorEventArgs : BaseRequestEventArgs {
		public OnCertificateErrorEventArgs(IWebBrowser chromiumWebBrowser, IBrowser browser, CefErrorCode errorCode, string requestUrl, ISslInfo sslInfo, IRequestCallback callback)
			: base(chromiumWebBrowser, browser) {
			ErrorCode = errorCode;
			RequestUrl = requestUrl;
			SSLInfo = sslInfo;
			Callback = callback;

			ContinueAsync = false; // default
		}

		public CefErrorCode ErrorCode { get; private set; }
		public string RequestUrl { get; private set; }
		public ISslInfo SSLInfo { get; private set; }

		/// <summary>
		///     Callback interface used for asynchronous continuation of url requests.
		///     If empty the error cannot be recovered from and the request will be canceled automatically.
		/// </summary>
		public IRequestCallback Callback { get; private set; }

		/// <summary>
		///     Set to false to cancel the request immediately. Set to true and use <see cref="T:CefSharp.IRequestCallback" /> to
		///     execute in an async fashion.
		/// </summary>
		public bool ContinueAsync { get; set; }
	}

	public class CustomRequesthandle : RequestHandler {
		//public event EventHandler<OnCertificateErrorEventArgs> OnCertificateErrorEvent;
		protected override bool OnBeforeBrowse(IWebBrowser chromiumWebBrowser, IBrowser browser, IFrame frame, IRequest request, bool userGesture, bool isRedirect) {
			return base.OnBeforeBrowse(chromiumWebBrowser, browser, frame, request, userGesture, isRedirect);

		}
		protected override bool OnSelectClientCertificate(IWebBrowser chromiumWebBrowser, IBrowser browser, bool isProxy, string host, int port, X509Certificate2Collection certificates, ISelectClientCertificateCallback callback) {
			return base.OnSelectClientCertificate(chromiumWebBrowser, browser, isProxy, host, port, certificates, callback);

		}
		protected override bool OnCertificateError(IWebBrowser chromiumWebBrowser, IBrowser browser, CefErrorCode errorCode, string requestUrl, ISslInfo sslInfo, IRequestCallback callback) {
			//var args = new OnCertificateErrorEventArgs(chromiumWebBrowser, browser, errorCode, requestUrl, sslInfo, callback);

			//OnCertificateErrorEvent?.Invoke(this, args);

			//callback.Continue(true);
			//return args.ContinueAsync;
			Task.Run(() =>
			{
				//NOTE: When executing the callback in an async fashion need to check to see if it's disposed
				if (!callback.IsDisposed) {
					using (callback) {
						//We'll allow the expired certificate from badssl.com
						if (requestUrl.ToString().Substring(0, 11).Equals("https://192")) {
							callback.Continue(true);
						} else {
							callback.Continue(false);
						}

						//if (requestUrl.ToLower().Contains("https://expired.badssl.com/")) {
						//	callback.Continue(true);
						//} else {
						//	callback.Continue(false);
						//}
					}
				}
			});

			return true;

		}
	}
}
