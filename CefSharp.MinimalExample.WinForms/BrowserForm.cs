﻿// Copyright © 2010-2015 The CefSharp Authors. All rights reserved.
//
// Use of this source code is governed by a BSD-style license that can be found in the LICENSE file.

using CefSharp.MinimalExample.WinForms.Controls;
using CefSharp.WinForms;
using SocketLibrary;
using System;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Security.AccessControl;
using System.Text;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using WebSocketSharp;
using System.Threading;
using System.Web;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Drawing.Drawing2D;
using System.Web.UI.WebControls;
using System.Threading.Tasks;
using myPOS;
using System.Runtime.ConstrainedExecution;
using System.Globalization;
using System.Collections;
using Newtonsoft.Json;
using System.Management;
using static System.Net.Mime.MediaTypeNames;
using System.Timers;
using System.Runtime.InteropServices.ComTypes;

using AutoUpdaterDotNET;
using System.IO;
using CefSharp.DevTools.DOMStorage;

namespace CefSharp.MinimalExample.WinForms
{
    public partial class BrowserForm : Form
    {
        //private readonly ChromiumWebBrowser browser;
		private string status = "";
		myPOSTerminal t = new myPOSTerminal();

		[DllImport("user32.dll")]
        static extern IntPtr GetSystemMenu(IntPtr hWnd, bool bRevert);
        [DllImport("user32.dll")]
        static extern bool DrawMenuBar(IntPtr hWnd);
        [DllImport("user32.dll")]
        static extern bool InsertMenuItem(IntPtr hMenu, uint uItem,
        bool fByPosition, [In] ref MENUITEMINFO lpmii);

        [StructLayout(LayoutKind.Sequential)]
        public struct MENUITEMINFO
        {
            public uint cbSize;
            public uint fMask;
            public uint fType;
            public uint fState;
            public uint wID;
            public IntPtr hSubMenu;
            public IntPtr hbmpChecked;
            public IntPtr hbmpUnchecked;
            public IntPtr dwItemData;
            public string dwTypeData;
            public uint cch;
            public IntPtr hbmpItem;
        }
        private const int WM_SYSCOMMAND = 0x112;

        public BrowserForm()
        {
            InitializeComponent();

            Text = "POS";
            WindowState = FormWindowState.Maximized;

            //https://cardlinkmaitre.ninepos.com/MPos/Account/SignIn
            //https://www.ninepos.com/MPos/Account/SignIn
            //https://dninepos.azurewebsites.net/Mpos/Account/SignIn
            //https://localhost:44315/MPos/
            //https://sandbox.ninepos.com/MPos/
            Common.browser = new ChromiumWebBrowser("https://dninepos.azurewebsites.net/Mpos/Account/SignIn")
            {
                Dock = DockStyle.Fill,
				
            };
            toolStripContainer.ContentPanel.Controls.Add(Common.browser);
            
            Common.browser.IsBrowserInitializedChanged += OnIsBrowserInitializedChanged;
            Common.browser.LoadingStateChanged += OnLoadingStateChanged;
            Common.browser.ConsoleMessage += OnBrowserConsoleMessage;
            Common.browser.StatusMessage += OnBrowserStatusMessage;
            Common.browser.TitleChanged += OnBrowserTitleChanged;
            Common.browser.AddressChanged += OnBrowserAddressChanged;
            Common.browser.RequestHandler = new CustomRequesthandle();
           
			//Για να καλέσεις απο javascript C# μέθοδο
			//CefSharpSettings.LegacyJavascriptBindingEnabled = true;
            CefSharpSettings.WcfEnabled = true;
            Common.browser.JavascriptObjectRepository.Settings.LegacyBindingEnabled = true;
            Common.browser.JavascriptObjectRepository.Register("Android", new BoundObject(t), isAsync: true, options: BindingOptions.DefaultBinder);


            Common.browser.JavascriptObjectRepository.ObjectBoundInJavascript += (sender, e) =>
			{
				var name = e.ObjectName;

				Debug.WriteLine($"Object {e.ObjectName} was bound successfully.");
			};

			
			var bitness = Environment.Is64BitProcess ? "x64" : "x86";
            var version = String.Format("Chromium: {0}, CEF: {1}, CefSharp: {2}, Environment: {3}", Cef.ChromiumVersion, Cef.CefVersion, Cef.CefSharpVersion, bitness);

            IntPtr hMenu = GetSystemMenu(Handle, false);
            if (hMenu != IntPtr.Zero) {
                var menuInfo = new MENUITEMINFO {
                    cbSize = (uint)Marshal.SizeOf(typeof(MENUITEMINFO)),
                    cch = 255,
                    dwTypeData = "Debug browser",
                    fMask = 0x1 | 0x2 | 0x10,
                    fState = 0,
                    // Add an ID for your Menu Item
                    wID = 0x1,
                    fType = 0x0
                };

                InsertMenuItem(hMenu, 0, true, ref menuInfo);

                menuInfo = new MENUITEMINFO {
                    cbSize = (uint)Marshal.SizeOf(typeof(MENUITEMINFO)),
                    cch = 255,
                    dwTypeData = "Print test page",
                    fMask = 0x1 | 0x2 | 0x10,
                    fState = 0,
                    // Add an ID for your Menu Item
                    wID = 0x2,
                    fType = 0x0
                };

                InsertMenuItem(hMenu, 0, true, ref menuInfo);


				menuInfo = new MENUITEMINFO
				{
					cbSize = (uint)Marshal.SizeOf(typeof(MENUITEMINFO)),
					cch = 255,
					dwTypeData = "Purchase Test with Go",
					fMask = 0x1 | 0x2 | 0x10,
					fState = 0,
					// Add an ID for your Menu Item
					wID = 0x3,
					fType = 0x0
				};

				InsertMenuItem(hMenu, 0, true, ref menuInfo);

				DrawMenuBar(Handle);
            }

            SocketListener socketListener = new SocketListener();
            Thread thread = new Thread(socketListener.StartListening);
            thread.Start();

            TelephoneListener telephoneListener = new TelephoneListener();
            Thread threadTel = new Thread(telephoneListener.StartListening);
            threadTel.Start();

            try
            {
                t.Initialize("COM7");
                t.ProcessingFinished += ProcessResult;
                t.onPresentCard += _PresentCard;
                t.onCardDetected += _CardDetected;
                t.onPresentPin += _PresentPin;
                t.onPinEntered += _PinEntered;
                t.onPresentDCC += _PresentDCC;
                t.onDCCSelected += _DCCSelected;
                t.onApprovalGetReceiptData += _ReceiptReceiver;
                t.SetLanguage(Language.Greek);
                t.SetCOMTimeout(3000);
                t.isFixedPinpad = true;
            }
            catch (System.Exception ex)
            {
                //MessageBox.Show("Πρόβημα αρχικοποιήσης myPOS Go" + ex.Message);
            }
		}
        public class BoundObject {
            private string Message;
            private string LineIDs;
            private int PartsCount;
            private int PartsNum;
            private myPOSTerminal t;
            public BoundObject(myPOSTerminal _t)
            {
                t = _t;
            }
            public string sendPort = "9700";
            //9702

            public string getDeviceName() {
                return System.Environment.MachineName;
            }

            public string getSerialNumber() {

                ManagementObjectCollection mbcList = null;
                ManagementObjectSearcher mbs = new ManagementObjectSearcher("Select * From Win32_processor");
                mbcList = mbs.Get();
                string processorid = "";
                foreach (ManagementObject mo in mbcList)
                {
                    processorid = mo["ProcessorID"].ToString();
                    break;
                }
                return processorid;
            }
            public string isOfflineModeSupported() {
                return "true";
            }
            public void clearAndroidCache()
            {
                try
                {


                    //string path_ServiceWorker = @"%USERPROFILE%\AppData\Local\CefSharp\Cache\Service Worker";
                    //string dir_ServiceWorker = Environment.ExpandEnvironmentVariables(path_ServiceWorker);
                    //DirectoryInfo dir = new DirectoryInfo(dir_ServiceWorker);
                    //if (dir.Exists) dir.Delete(true);
                    //Common.browser.Reload(true);

                    //string path_blob_storage = @"%USERPROFILE%\AppData\Local\CefSharp\Cache\blob_storage";
                    //string dir_blob_storage = Environment.ExpandEnvironmentVariables(path_blob_storage);
                    //string path_Cache = @"%USERPROFILE%\AppData\Local\CefSharp\Cache\Cache";
                    //string dir_Cache = Environment.ExpandEnvironmentVariables(path_Cache);
                    //string path_CodeCache = @"%USERPROFILE%\AppData\Local\CefSharp\Cache\Code Cache";
                    //string dir_CodeCache = Environment.ExpandEnvironmentVariables(path_CodeCache);
                    //string path_LocalStorage = @"%USERPROFILE%\AppData\Local\CefSharp\Cache\Local Storage";
                    //string dir_LocalStorage = Environment.ExpandEnvironmentVariables(path_LocalStorage);
                    //DirectoryInfo dir = new DirectoryInfo(dir_blob_storage);
                    //if (dir.Exists) dir.Delete(true);
                    //dir = new DirectoryInfo(dir_Cache);
                    //if (dir.Exists) dir.Delete(true);
                    //dir = new DirectoryInfo(dir_CodeCache);
                    //if (dir.Exists) dir.Delete(true);
                    //dir = new DirectoryInfo(dir_LocalStorage);
                    //if (dir.Exists) dir.Delete(true);

                    string exePath = Path.Combine(Environment.CurrentDirectory, "CleanServiceWorkerNet.exe");

                    // Check if the executable file exists
                    if (File.Exists(exePath))
                    {
                        // Start the other application
                        Process.Start(exePath);
                        Console.WriteLine("CleanServiceWorker started successfully.");
                    }
                    else
                    {
                        Console.WriteLine("Executable file not found.");
                    }
                }
                catch (System.Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                
            }
            public void executeRbsOpen(string ip)
            {
                Common.ws = new WebSocket("ws://" + ip);
                Common.ws.OnMessage += new EventHandler<MessageEventArgs>(OnRbsMessage);
                Common.ws.OnError += new EventHandler<WebSocketSharp.ErrorEventArgs>(OnRbsError);
                Common.ws.WaitTime = TimeSpan.FromSeconds(2);
                Common.ws.Connect();
            }
            public void executeRbsMessage(string message)
            {
                Message = message;
                Common.ws.Send(message);
                System.Threading.Thread.Sleep(400);
            }
            public void executeRbsPrint(string ip, string message)
            {
                if (Common.ws != null)
                {
                    Common.browser.Load("javascript:showNotification('Η Ταμειακή χρησιμοποιείται , ξαναπροσπάθησε!');");
                }
                else
                {
                    LineIDs = "";
                    Message = message;

                    Common.ws = new WebSocket("ws://" + ip);
                    Common.ws.OnMessage += new EventHandler<MessageEventArgs>(OnRbsMessage);
                    Common.ws.OnError += new EventHandler<WebSocketSharp.ErrorEventArgs>(OnRbsError);
                    Common.ws.Connect();

                    string[] parts = message.Split(';');
                    PartsCount = parts.Length;
                    PartsNum = 1;
                    Common.ws.Send(parts[0]);
                }
            }
            public void executeRbsPrintWithData(string ip, string message, string lineIDs)
            {

                if (Common.ws != null)
                {
                    Common.browser.Load("javascript:showNotification('Η Ταμειακή χρησιμοποιείται , ξαναπροσπάθησε!');");
                }
                else
                {
                    LineIDs = lineIDs;
                    Message = message;

                    Common.ws = new WebSocket("ws://" + ip);
                    Common.ws.OnMessage += new EventHandler<MessageEventArgs>(OnRbsMessage);
                    Common.ws.OnError += new EventHandler<WebSocketSharp.ErrorEventArgs>(OnRbsError);
                    Common.ws.Connect();

                    string[] parts = message.Split(';');
                    PartsCount = parts.Length;
                    PartsNum = 1;
                    Common.ws.Send(parts[0]);
                }
            }
            private void OnRbsMessage<TEventArgs>(object sender, TEventArgs e)
            {
                if (PartsNum < PartsCount)
                {
                    PartsNum++;
                    String[] separated = Message.Split(';');
                    Common.ws.Send(separated[PartsNum - 1]);
                    if (PartsNum == PartsCount)
                    {
                        if (e is WebSocketSharp.MessageEventArgs)
                        {
                            string res = (e as WebSocketSharp.MessageEventArgs).Data;
                            if (res.Substring(0, 2) == "00")
                            {

                                if (LineIDs != "")
                                {
                                    Common.browser.Load("javascript:RBSPrintCompletedWithData('" + LineIDs + "');");
                                }
                                else
                                {
                                    Common.browser.Load("javascript:RBSPrintCompleted();");
                                }
                            }
                            else
                            {
                                Common.browser.Load("javascript:showNotification('Error on printing with RBS. Error Code : " + res + "');");
                            }
                        }
                        else
                        {
                            Common.browser.Load("javascript:showNotification('Error on printing with RBS. Null EventArgs');");
                        }
                        Common.ws.Close();
                        Common.ws = null;
                    }
                }
            }
            private void OnRbsError<TEventArgs>(object sender, TEventArgs e)
            {
                if (e is WebSocketSharp.ErrorEventArgs)
                    Common.browser.Load("javascript:showNotification('" + (e as WebSocketSharp.ErrorEventArgs).Message + "');");
                else
                    Common.browser.Load("javascript:showNotification('Error on printing with RBS.');");

                try
                {
                    if (Common.ws != null && Common.ws.ReadyState != WebSocketState.Closed)
                        Common.ws.Close();
                }
                catch (System.Exception e1) { }
                Common.ws = null;
            }

            public void executeESCPosPrint(string ip, string port, string message, string lines) {
                Escpos esc = new Escpos();
                if (string.IsNullOrEmpty(port)) {
                    port = "9100";
                }

                if (!esc.PrintReceipt(ip, port, message)) {
                    Common.browser.Load("javascript:ESCPOSPrintCompleted('" + lines + "')");
                } else {
                    Common.browser.Load("javascript:EspPosFailed();");
                }
                esc.Dispose();
                esc = null;
            }

            public void executeESCPosPrintNum(string ip, string port, string message, string lines, int PrinterNum)
            {
                Escpos esc = new Escpos();
                if (string.IsNullOrEmpty(port))
                {
                    port = "9100";
                }

                if (!esc.PrintReceipt(ip, port, message))
                {
                    Common.browser.Load("javascript:ESCPOSPrintNumCompleted('" + lines + "','" + PrinterNum.ToString() + "')");
                }
                else
                {
                    Common.browser.Load("javascript:EspPosNumFailed('" + PrinterNum.ToString() + "');");
                }
                esc.Dispose();
                esc = null;
            }
            public void executeESCPosStationPrint(string ip, string port, string message, string lines, int PrinterNum, String StationID, String InvoiceID)
            {
                Escpos esc = new Escpos();
                if (string.IsNullOrEmpty(port))
                {
                    port = "9100";
                }

                if (!esc.PrintReceipt(ip, port, message))
                {
                    //Common.browser.Load("javascript:ESCPOSPrintNumCompleted('" + lines + "','" + PrinterNum.ToString() + "')");
                    Common.browser.Load("javascript:ESCPOSPrintStationCompleted('" + lines + "' , '" + PrinterNum.ToString() + "','" + InvoiceID + "' , '" + StationID + "');");
                }
                else
                {
                    //Common.browser.Load("javascript:EspPosNumFailed('" + PrinterNum.ToString() + "');");
                    Common.browser.Load("javascript:EspPosStationFailed('" + PrinterNum.ToString() + "','" + StationID.ToString() + "','" + InvoiceID.ToString() + "','" + lines.ToString() + "');");
                }
                esc.Dispose();
                esc = null;
            }
            public void executeESCPosSimplePrint(String ip, String port, String message, String encoding, String codepage1, String codepage2, String codepage3) {
                Escpos esc = new Escpos();
                if (string.IsNullOrEmpty(port)) {
                    port = "9100";
                }

                esc.PrintSimpleReceipt(ip, port, message, encoding, codepage1, codepage2, codepage3);
                esc.Dispose();
                esc = null;
            }
            public void executeDynamicPrint(String ip, String port, String message, String Lines, int PrinterNum, String StationID, String InvoiceID, String encoding, int Copies, String CodePage1, String CodePage2, String CodePage3)
            {
                Print.NetworkPrinter print = new Print.NetworkPrinter();
                if (string.IsNullOrEmpty(port))
                {
                    port = "9100";
                }

                if (!print.DynamicPrint(ip, port, message, Lines, PrinterNum, StationID, InvoiceID, encoding, Copies, CodePage1, CodePage2, CodePage3))
                {
                    Common.browser.Load("javascript:ESCPOSPrintStationCompleted('" + Lines + "' , '" + PrinterNum.ToString() + "','" + InvoiceID + "' , '" + StationID + "');");
                }
                else
                {
                    Common.browser.Load("javascript:EspPosStationFailed('" + PrinterNum.ToString() + "','" + StationID.ToString() + "','" + InvoiceID.ToString() + "','" + Lines.ToString() + "');");
                }
                print.Dispose();
                print = null;
            }
            public void executeAndroidToDiginetServer(String e_line, String message, String InvoiceId, String lineIDs, String ServerIPAddress) {
                String returnIP = GetIP();
                if (returnIP == "")
                {
                    ShowWebViewMessage("H συσκευή δεν έχει IP , ελέγξτε το καλώδιο δικτύου της συσκευής ή το Wifi αν είστε με ασύρματο δίκτυο. Απο το κεντρικό μενού --> ρυθμίσεις --> Whats my IP , μπορείτε να δείτε αν έχει παρει IP η συσκευή");
                    return;
                }

                String amessage = message.Replace("\n", "\\n").Replace("\r", "\\r").Replace("\'", "\\'").Replace("\u001B", "\\u001B").Replace("\u0000", "\\u0000").Replace("\u0001", "\\u0001").Replace("\0", "\\0");
                String SendMessage = "GetDiginetSign^^^" + e_line + "^^^" + amessage + "^^^" + InvoiceId + "^^^" + lineIDs + "^^^" + returnIP;
                Escpos esc = new Escpos();
                esc.SendMessage(ServerIPAddress, SendMessage);
            }
            public void executeDigiNetESCPosRePrint(String sign, String ip, String port, String message, String encoding, String codepage1, String codepage2, String codepage3) {
                try
                {
                    String[] arrayString = sign.ToUpper().Split(new string[] { "HTTPS" }, StringSplitOptions.None);
                    String simansi = arrayString[0];
                    String qrcode = "HTTPS" + arrayString[1];

                    Escpos esc = new Escpos();
                    if (string.IsNullOrEmpty(port))
                    {
                        port = "9100";
                    }

                    esc.PrintDigiNetESCPos(simansi, qrcode, ip, port, message, encoding, codepage1, codepage2, codepage3);
                    esc.Dispose();
                    esc = null;
                }
                catch (System.Exception ex) {

                }
            }
            public string GetIP()
            {
                try
                {
                    string lIP = ConfigurationManager.AppSettings["LocalIP"].ToString();
                    if (lIP != "0.0.0.0")
                    {
                        return lIP;
                    }
                }
                catch (Exception ex)
                {

                }


                IPHostEntry host;
                string localIP = "";
                host = Dns.GetHostEntry(Dns.GetHostName());

                foreach (IPAddress ip in host.AddressList)
                {
                    localIP = ip.ToString();

                    string[] temp = localIP.Split('.');

                    if (ip.AddressFamily == AddressFamily.InterNetwork && temp[0] == "192" || ip.AddressFamily == AddressFamily.InterNetwork && temp[0] == "10" || ip.AddressFamily == AddressFamily.InterNetwork && temp[0] == "172")
                    {
                        break;
                    }
                    else
                    {
                        localIP = null;
                    }
                }

                return localIP;
            }

            public void executeWhatsMyIP() {
                String returnIP = GetIP();
                Common.browser.Load("javascript:ReturnWhatsMyIP('" + returnIP + "');");
            }


            public void ShowWebViewMessage(string message) {
                Common.browser.Load("javascript:showNotification('" + message + "');");
            }

            #region Diginet Print Bill
            private string WindowsDiginetServer_BillType;
            private string WindowsDiginetServer_e_line;
            private string WindowsDiginetServer_message;
            private string WindowsDiginetServer_ip;
            private string WindowsDiginetServer_port;
            private string WindowsDiginetServer_encoding;
            private string WindowsDiginetServer_codepage1;
            private string WindowsDiginetServer_codepage2;
            private string WindowsDiginetServer_codepage3;
            private string WindowsDiginetServer_InvoiceId;
            private string WindowsDiginetServer_BillCode;
            private string WindowsDiginetServer_lineIDs;
            private string WindowsDiginetServer_Type;
            private string WindowsDiginetServer_CancelBillCode;
            private string WindowsDiginetServer_ContextData;
            private string WindowsDiginetServer_fromIP;
            private string WindowsDiginetServer_PrintType;

            private bool DiginetIsPrinting = false;
            private List<DiginetPrintData> DiginetQueue = new List<DiginetPrintData>();


            public void executeCancelDigiNetESCPosPrint(String e_line, String ip, String port, String message, String encoding, String codepage1, String codepage2, String codepage3, String InvoiceId, String BillCode, String lineIDs, String CancelBillCode)
            {
                try
                {
                    if (CheckForQueue("Bill", e_line, message, ip, port, encoding, codepage1, codepage2, codepage3, InvoiceId, BillCode, lineIDs, "SERVER", CancelBillCode, "", "", ""))
                    {
                        Log("CheckForQueue ok");
                        return;
                    }

                    ServerDiginetPrint("Cancel", e_line, ip, port, message, encoding, codepage1, codepage2, codepage3, InvoiceId, BillCode, lineIDs, CancelBillCode, "", "", "");

                }
                catch (System.Exception ex)
                {
                }
            }
            public void executeDigiNetESCPosPrintWithParams2(String e_line, String ip, String port, String message, String encoding, String codepage1, String codepage2, String codepage3, String InvoiceId, String BillCode, String lineIDs)
            {

                try
                {
                    if (CheckForQueue("Bill", e_line, message, ip, port, encoding, codepage1, codepage2, codepage3, InvoiceId, BillCode, lineIDs, "SERVER", "", "", "", "")) {
                        Log("CheckForQueue ok");
                        return;
                    }


                    ServerDiginetPrint("Bill", e_line, ip, port, message, encoding, codepage1, codepage2, codepage3, InvoiceId, BillCode, lineIDs, "", "", "", "");

                }
                catch (System.Exception ex) {
                }
            }
            private void ServerDiginetPrint(String BillType, String e_line, String ip, String port, String message, String encoding, String codepage1, String codepage2, String codepage3, String InvoiceId, String BillCode, String lineIDs, string CancelBillCode, String contextData, string fromIP, string PrintType)
            {

                WindowsDiginetServer_BillType = BillType;
                WindowsDiginetServer_Type = "SERVER";
                WindowsDiginetServer_e_line = e_line;
                WindowsDiginetServer_message = message;
                WindowsDiginetServer_ip = ip;
                WindowsDiginetServer_port = port;
                WindowsDiginetServer_encoding = encoding;
                WindowsDiginetServer_codepage1 = codepage1;
                WindowsDiginetServer_codepage2 = codepage2;
                WindowsDiginetServer_codepage3 = codepage3;
                WindowsDiginetServer_InvoiceId = InvoiceId;
                WindowsDiginetServer_BillCode = BillCode;
                WindowsDiginetServer_lineIDs = lineIDs;
                WindowsDiginetServer_CancelBillCode = CancelBillCode;
                WindowsDiginetServer_ContextData = contextData;
                WindowsDiginetServer_fromIP = fromIP;
                WindowsDiginetServer_PrintType = PrintType;

                if (WindowsDiginetServer_BillType == "Bill")
                {
                    if (WindowsDiginetServer_ContextData == "" || WindowsDiginetServer_ContextData == null)
                    {
                        String lastBillCode = GetLastBillCode();
                        String amessage = message.Replace("\n", "\\n").Replace("\r", "\\r").Replace("\'", "\\'").Replace("\u001B", "\\u001B").Replace("\u0000", "\\u0000").Replace("\u0001", "\\u0001").Replace("\0", "\\0");

                        bool flagCanPrint = true;
                        if (lastBillCode != null && lastBillCode != "")
                        {
                            int SendCode = Convert.ToInt32(BillCode.Substring(BillCode.Length - 8));
                            int LastCode = Convert.ToInt32(lastBillCode.Substring(BillCode.Length - 8));
                            if (SendCode <= LastCode)
                            {
                                flagCanPrint = false;

                                Common.browser.Load("javascript:FixDiginetBillCodeByLastCode(" + LastCode + ")");

                                DiginetIsPrinting = false;
                                DiginetQueue = new List<DiginetPrintData>();
                                Common.browser.Load("javascript:showNotification('Δεν εκδόθηκε απόδειξη με κωδικό λάθους: η αριθμιση της απόδειξης έχει ξαναεκτυπωθεί: " + BillCode + ". Ξαναπροσπαθήστε!');");
                                return;
                            }
                        }
                        if (flagCanPrint)
                        {

                            if (Common.ws == null)
                            {
                                StartNanotaxDriver();
                            }
                            else
                            {
                                Log(Common.ws.ReadyState.ToString());
                            }
                        }
                        GetDiginetSign(amessage, e_line);
                    }
                    else
                    {
                        String lastBillCode = GetLastBillCode();
                        String amessage = message.Replace("\n", "\\n").Replace("\r", "\\r").Replace("\'", "\\'").Replace("\u001B", "\\u001B").Replace("\u0000", "\\u0000").Replace("\u0001", "\\u0001").Replace("\0", "\\0");

                        bool flagCanPrint = true;
                        if (lastBillCode != null && lastBillCode != "")
                        {
                            int SendCode = Convert.ToInt32(BillCode.Substring(BillCode.Length - 8));
                            int LastCode = Convert.ToInt32(lastBillCode.Substring(BillCode.Length - 8));
                            if (SendCode <= LastCode)
                            {
                                flagCanPrint = false;

                                Common.browser.Load("javascript:FixDiginetBillCodeByLastCode(" + LastCode + ")");

                                String retMessage = WindowsDiginetServer_ContextData.Replace("\n", "\\n").Replace("\r", "\\r").Replace("\'", "\\'").Replace("\u001B", "\\u001B").Replace("\u0000", "\\u0000").Replace("\u0001", "\\u0001").Replace("\0", "\\0");
                                retMessage = retMessage.Replace("\\\"", "\\\\\"");

                                Common.browser.Load("javascript:PostDataFromClient('" + retMessage + "','" + WindowsDiginetServer_fromIP + "',1)");

                                DiginetIsPrinting = false;
                                return;
                            }
                        }
                        if (flagCanPrint)
                        {

                            if (Common.ws == null)
                            {
                                StartNanotaxDriver();
                            }
                            else
                            {
                                Log(Common.ws.ReadyState.ToString());
                            }
                        }
                        GetDiginetSign(amessage, e_line);
                    }
                }
                if (WindowsDiginetServer_BillType == "Cancel")
                {
                    if (WindowsDiginetServer_ContextData == "" || WindowsDiginetServer_ContextData == null)
                    {
                        String lastBillCode = GetLastCancelBillCode();
                        String amessage = message.Replace("\n", "\\n").Replace("\r", "\\r").Replace("\'", "\\'").Replace("\u001B", "\\u001B").Replace("\u0000", "\\u0000").Replace("\u0001", "\\u0001").Replace("\0", "\\0");

                        bool flagCanPrint = true;
                        if (lastBillCode != null && lastBillCode != "")
                        {
                            int SendCode = Convert.ToInt32(BillCode.Substring(BillCode.Length - 8));
                            int LastCode = Convert.ToInt32(lastBillCode.Substring(BillCode.Length - 8));
                            if (SendCode <= LastCode)
                            {
                                flagCanPrint = false;

                                Common.browser.Load("javascript:FixDiginetCancelCodeByLastCode(" + LastCode + ")");

                                Common.browser.Load("javascript:showNotification('Δεν εκδόθηκε απόδειξη με κωδικό λάθους: η αριθμιση της απόδειξης έχει ξαναεκτυπωθεί: " + BillCode + ". Ξαναπροσπαθήστε!');");

                                DiginetIsPrinting = false;
                                DiginetQueue = new List<DiginetPrintData>();
                                return;
                            }
                        }
                        if (flagCanPrint)
                        {

                            if (Common.ws == null)
                            {
                                StartNanotaxDriver();
                            }
                            else
                            {
                                Log(Common.ws.ReadyState.ToString());
                            }
                        }
                        GetDiginetSign(amessage, e_line);
                    }
                    else
                    {
                        String lastBillCode = GetLastCancelBillCode();
                        String amessage = message.Replace("\n", "\\n").Replace("\r", "\\r").Replace("\'", "\\'").Replace("\u001B", "\\u001B").Replace("\u0000", "\\u0000").Replace("\u0001", "\\u0001").Replace("\0", "\\0");

                        bool flagCanPrint = true;
                        if (lastBillCode != null && lastBillCode != "")
                        {
                            int SendCode = Convert.ToInt32(BillCode.Substring(BillCode.Length - 8));
                            int LastCode = Convert.ToInt32(lastBillCode.Substring(BillCode.Length - 8));
                            if (SendCode <= LastCode)
                            {
                                flagCanPrint = false;

                                Common.browser.Load("javascript:FixDiginetCancelCodeByLastCode(" + LastCode + ")");
                                String retMessage = WindowsDiginetServer_ContextData.Replace("\n", "\\n").Replace("\r", "\\r").Replace("\'", "\\'").Replace("\u001B", "\\u001B").Replace("\u0000", "\\u0000").Replace("\u0001", "\\u0001").Replace("\0", "\\0");
                                retMessage = retMessage.Replace("\\\"", "\\\\\"");
                                Common.browser.Load("javascript:PostDataFromClient('" + retMessage + "','" + WindowsDiginetServer_fromIP + "',1)");

                                DiginetIsPrinting = false;
                                return;
                            }
                        }
                        if (flagCanPrint)
                        {

                            if (Common.ws == null)
                            {
                                StartNanotaxDriver();
                            }
                            else
                            {
                                Log(Common.ws.ReadyState.ToString());
                            }
                        }
                        GetDiginetSign(amessage, e_line);
                    }
                }
                if (WindowsDiginetServer_BillType == "Order")
                {
                    String lastBillCode = GetLastOrderCode();
                    String amessage = message.Replace("\n", "\\n").Replace("\r", "\\r").Replace("\'", "\\'").Replace("\u001B", "\\u001B").Replace("\u0000", "\\u0000").Replace("\u0001", "\\u0001").Replace("\0", "\\0");

                    bool flagCanPrint = true;
                    if (lastBillCode != null && lastBillCode != "")
                    {
                        int SendCode = Convert.ToInt32(BillCode.Substring(BillCode.Length - 8));
                        int LastCode = Convert.ToInt32(lastBillCode.Substring(BillCode.Length - 8));
                        if (SendCode <= LastCode)
                        {
                            flagCanPrint = false;

                            Common.browser.Load("javascript:FixDiginetOrderCodeByLastCode(" + LastCode + ")");

                            String retMessage = WindowsDiginetServer_ContextData.Replace("\n", "\\n").Replace("\r", "\\r").Replace("\'", "\\'").Replace("\u001B", "\\u001B").Replace("\u0000", "\\u0000").Replace("\u0001", "\\u0001").Replace("\0", "\\0");
                            retMessage = retMessage.Replace("\\\"", "\\\\\"");

                            Common.browser.Load("javascript:PostDataFromClient('" + retMessage + "','" + WindowsDiginetServer_fromIP + "',1)");

                            DiginetIsPrinting = false;
                            return;
                        }
                    }
                    if (flagCanPrint)
                    {

                        if (Common.ws == null)
                        {
                            StartNanotaxDriver();
                        }
                        else
                        {
                            Log(Common.ws.ReadyState.ToString());
                        }
                    }
                    GetDiginetSign(amessage, e_line);
                }
                if (WindowsDiginetServer_BillType == "OrderCancel")
                {
                    String lastBillCode = GetLastOrderCancelCode();
                    String amessage = message.Replace("\n", "\\n").Replace("\r", "\\r").Replace("\'", "\\'").Replace("\u001B", "\\u001B").Replace("\u0000", "\\u0000").Replace("\u0001", "\\u0001").Replace("\0", "\\0");

                    bool flagCanPrint = true;
                    if (lastBillCode != null && lastBillCode != "")
                    {
                        int SendCode = Convert.ToInt32(BillCode.Substring(BillCode.Length - 8));
                        int LastCode = Convert.ToInt32(lastBillCode.Substring(BillCode.Length - 8));
                        if (SendCode <= LastCode)
                        {
                            flagCanPrint = false;

                            Common.browser.Load("javascript:FixDiginetOrderCancelCodeByLastCode(" + LastCode + ")");
                            String retMessage = WindowsDiginetServer_ContextData.Replace("\n", "\\n").Replace("\r", "\\r").Replace("\'", "\\'").Replace("\u001B", "\\u001B").Replace("\u0000", "\\u0000").Replace("\u0001", "\\u0001").Replace("\0", "\\0");
                            retMessage = retMessage.Replace("\\\"", "\\\\\"");
                            Common.browser.Load("javascript:PostDataFromClient('" + retMessage + "','" + WindowsDiginetServer_fromIP + "',1)");

                            DiginetIsPrinting = false;
                            return;
                        }
                    }
                    if (flagCanPrint)
                    {

                        if (Common.ws == null)
                        {
                            StartNanotaxDriver();
                        }
                        else
                        {
                            Log(Common.ws.ReadyState.ToString());
                        }
                    }
                    GetDiginetSign(amessage, e_line);
                }
            }
            private void ClientDiginetPrint(String BillType, String e_line, String ip, String port, String message, String encoding, String codepage1, String codepage2, String codepage3, String InvoiceId, String BillCode, String lineIDs, string CancelBillCode, string fromIP)
            {
                WindowsDiginetServer_BillType = BillType;
                WindowsDiginetServer_e_line = e_line;
                WindowsDiginetServer_message = message;
                WindowsDiginetServer_InvoiceId = InvoiceId;
                WindowsDiginetServer_BillCode = BillCode;
                WindowsDiginetServer_lineIDs = lineIDs;
                WindowsDiginetServer_ip = ip;
                WindowsDiginetServer_Type = "CLIENT";
                WindowsDiginetServer_CancelBillCode = CancelBillCode;
                WindowsDiginetServer_fromIP = fromIP;

                String lastBillCode = GetLastBillCode();

                bool flagCanPrint = true;
                if (lastBillCode != null && lastBillCode != "")
                {
                    int SendCode = Convert.ToInt32(WindowsDiginetServer_BillCode.Substring(WindowsDiginetServer_BillCode.Length - 8));
                    int LastCode = Convert.ToInt32(lastBillCode.Substring(WindowsDiginetServer_BillCode.Length - 8));
                    if (SendCode <= LastCode)
                    {
                        flagCanPrint = false;

                        Common.browser.Load("javascript:FixDiginetBillCodeByLastCode(" + LastCode + ")");
                        Escpos escpos = new Escpos();
                        escpos.SendMessage(WindowsDiginetServer_ip, "ErrorMessage^^^Δεν εκδόθηκε απόδειξη με κωδικό λάθους: η αριθμιση της απόδειξης έχει ξαναεκτυπωθεί: " + WindowsDiginetServer_BillCode + ". Ξαναπροσπαθήστε!", sendPort);

                        DiginetIsPrinting = false;
                        DiginetQueue = new List<DiginetPrintData>();
                        return;
                    }
                }
                if (flagCanPrint)
                {
                    if (Common.ws == null)
                    {
                        StartNanotaxDriver();
                    }
                }
                String amessage = message.Replace("\n", "\\n").Replace("\r", "\\r").Replace("\'", "\\'").Replace("\u001B", "\\u001B").Replace("\u0000", "\\u0000").Replace("\u0001", "\\u0001").Replace("\0", "\\0");
                GetDiginetSign(amessage, WindowsDiginetServer_e_line);
            }
            private string GetLastBillCode()
            {
                try
                {
                    string fileName = System.Windows.Forms.Application.StartupPath + "\\" + "DiginetLastValue.txt";
                    if (System.IO.File.Exists(fileName))
                    {
                        string text = System.IO.File.ReadAllText(fileName);
                        if (text != null && text != "") {
                            return text;
                        }
                        else
                            return "0";
                    }
                    else {
                        return "0";
                    }

                }
                catch (System.Exception ex) {
                    return "0";
                }
            }
            private string GetLastCancelBillCode()
            {
                try
                {
                    string fileName = System.Windows.Forms.Application.StartupPath + "\\" + "DiginetCancelLastValue.txt";
                    if (System.IO.File.Exists(fileName))
                    {
                        string text = System.IO.File.ReadAllText(fileName);
                        if (text != null && text != "")
                        {
                            return text;
                        }
                        else
                            return "0";
                    }
                    else
                    {
                        return "0";
                    }

                }
                catch (System.Exception ex)
                {
                    return "0";
                }
            }
            private void SaveLastBillCode(string BillCode)
            {
                try
                {
                    string fileName = System.Windows.Forms.Application.StartupPath + "\\" + "DiginetLastValue.txt";
                    using (System.IO.FileStream fs = System.IO.File.Create(fileName))
                    {
                        byte[] info = new UTF8Encoding(true).GetBytes(BillCode);
                        fs.Write(info, 0, info.Length);
                    }

                }
                catch (System.Exception ex)
                {
                }
            }
            private void SaveLastCancelBillCode(string BillCode)
            {
                try
                {
                    string fileName = System.Windows.Forms.Application.StartupPath + "\\" + "DiginetCancelLastValue.txt";
                    using (System.IO.FileStream fs = System.IO.File.Create(fileName))
                    {
                        byte[] info = new UTF8Encoding(true).GetBytes(BillCode);
                        fs.Write(info, 0, info.Length);
                    }

                }
                catch (System.Exception ex)
                {
                }
            }
            private void StartNanotaxDriver() {
                try
                {
                    String wsUri = "ws://localhost:1453/nanotax";
                    Common.ws = new WebSocket(wsUri);
                    Common.ws.OnOpen += new EventHandler(OnDiginetOpened);
                    Common.ws.OnMessage += new EventHandler<MessageEventArgs>(OnDiginetMessage);
                    Common.ws.OnError += new EventHandler<WebSocketSharp.ErrorEventArgs>(OnDiginetError);
                    Common.ws.OnClose += new EventHandler<CloseEventArgs>(OnDiginetClose);
                    Common.ws.Connect();
                }
                catch (System.Exception ex) {
                    Common.browser.Load("javascript:showNotification('" + ex.Message + "');");
                }
            }
            private void GetDiginetSign(string message, string e_line) {
                try
                {
                    message = message.Replace("\\n", "\n").Replace("€", "");
                    string data = message + "/@" + e_line + "@/";
                    string data1 = HttpUtility.HtmlDecode(HttpUtility.HtmlEncode(data));
                    byte[] bytes = Encoding.UTF8.GetBytes(data);
                    string toReturn = System.Convert.ToBase64String(bytes);
                    toReturn = "SIGN/" + toReturn;
                    Common.ws.Send(toReturn);
                }
                catch (System.Exception ex) {
                    Log("Get Diginet Sign Error : " + ex.Message);
                    Common.browser.Load("javascript:showNotification('" + ex.Message + "');");
                    try
                    {
                        Common.ws.Close();
                    }
                    catch (System.Exception ex1) { }
                    Common.ws = null;
                    StartNanotaxDriver();
                }
            }
            private void OnDiginetMessage<TEventArgs>(object sender, TEventArgs e)
            {
                try
                {
                    if (WindowsDiginetServer_BillType == "Bill")
                    {
                        if (WindowsDiginetServer_Type == "SERVER")
                        {
                            if (e is WebSocketSharp.MessageEventArgs)
                            {

                                string sign = (e as WebSocketSharp.MessageEventArgs).Data;
                                if (sign.Substring(0, 3) == "ERR")
                                {
                                    Log(sign);
                                    Common.browser.Load("javascript:showNotification('" + sign.Substring(3) + "');");
                                }
                                else
                                {
                                    if (WindowsDiginetServer_ContextData == "" || WindowsDiginetServer_ContextData == null)
                                    {
                                        String[] arrayString = sign.Split(new string[] { "https" }, StringSplitOptions.None);
                                        String simansi = arrayString[0].Substring(0, arrayString[0].Length - 1);
                                        String qrcode = "https" + arrayString[1];
                                        String aSign = sign.Replace("\n", "\\n");
                                        String aMessage = WindowsDiginetServer_message.Replace("\n", "\\n");
                                        Common.browser.Load("javascript:RBSPrintCompletedWithParams2('" + aSign + "','" + WindowsDiginetServer_BillCode + "','" + WindowsDiginetServer_InvoiceId + "','" + WindowsDiginetServer_lineIDs + "','" + aMessage + "')");
                                        SaveLastBillCode(WindowsDiginetServer_BillCode);
                                        Escpos esc = new Escpos();
                                        esc.PrintDigiNetESCPos(simansi, qrcode, WindowsDiginetServer_ip, WindowsDiginetServer_port, WindowsDiginetServer_message, WindowsDiginetServer_encoding, WindowsDiginetServer_codepage1, WindowsDiginetServer_codepage2, WindowsDiginetServer_codepage3);
                                    }
                                    else
                                    {
                                        String[] arrayString = sign.Split(new string[] { "https" }, StringSplitOptions.None);
                                        String simansi = arrayString[0].Substring(0, arrayString[0].Length - 1);
                                        String qrcode = "https" + arrayString[1];
                                        String aSign = sign.Replace("\n", "\\n");
                                        String aMessage = WindowsDiginetServer_message.Replace("\n", "\\n");
                                        String contextData = WindowsDiginetServer_ContextData.Replace("\n", "\\n").Replace("\r", "\\r").Replace("\'", "\\'").Replace("\u001B", "\\u001B").Replace("\u0000", "\\u0000").Replace("\u0001", "\\u0001").Replace("\0", "\\0");
                                        //Save InvoiceBill to Database
                                        contextData = contextData.Replace("\\\"", "\\\\\"");
                                        Common.browser.Load("javascript:BillDigiNetCompleted('" + aSign + "','" + WindowsDiginetServer_BillCode + "','" + WindowsDiginetServer_InvoiceId + "','" + WindowsDiginetServer_lineIDs + "','" + aMessage + "','" + contextData + "','" + WindowsDiginetServer_fromIP + "')");
                                        //Inform Last number
                                        SaveLastBillCode(WindowsDiginetServer_BillCode);
                                        //Print Invoice Bill
                                        if (WindowsDiginetServer_PrintType == "EscPos")
                                        {
                                            Escpos esc = new Escpos();
                                            esc.PrintDigiNetESCPos(simansi, qrcode, WindowsDiginetServer_ip, WindowsDiginetServer_port, WindowsDiginetServer_message, WindowsDiginetServer_encoding, WindowsDiginetServer_codepage1, WindowsDiginetServer_codepage2, WindowsDiginetServer_codepage3);
                                        }
                                    }
                                }
                            }
                        }
                        if (WindowsDiginetServer_Type == "CLIENT")
                        {
                            string sign = (e as WebSocketSharp.MessageEventArgs).Data;
                            if (sign.Substring(0, 3) == "ERR")
                            {
                                Escpos escpos = new Escpos();
                                escpos.SendMessage(WindowsDiginetServer_ip, "ErrorMessage^^^Δεν εκδόθηκε απόδειξη με κωδικό λάθους : " + sign.Substring(3) + " . Ελέγξτε τον φορολογικό μηχανισμό και απο την εφαρμογή Android Nanotax Driver κάντε Start και Stop για εξακριβώσετε την ομαλή λειτουργία του. ", sendPort);
                            }
                            else
                            {

                                String[] arrayString = sign.Split(new string[] { "https" }, StringSplitOptions.None);
                                String aSign = sign.Replace("\n", "\\n").Replace(";https", "https");
                                String aMessage = WindowsDiginetServer_message.Replace("\n", "\\n");
                                Common.browser.Load("javascript:CardlinkDiginetPrintCompletedWithParams2('" + aSign + "','" + WindowsDiginetServer_BillCode + "','" + WindowsDiginetServer_InvoiceId + "','" + WindowsDiginetServer_lineIDs + "','" + aMessage + "')");
                                SaveLastBillCode(WindowsDiginetServer_BillCode);
                                String SendMessage = "ReturnDiginetSign^^^" + aSign + "^^^" + aMessage + "^^^" + WindowsDiginetServer_InvoiceId + "^^^" + WindowsDiginetServer_lineIDs;
                                Escpos escpos = new Escpos();
                                escpos.SendMessage(WindowsDiginetServer_ip, SendMessage, sendPort);

                                Log("Answer : " + SendMessage);
                            }
                        }

                        DiginetIsPrinting = false;
                        ChextNextQueue();
                        return;
                    }
                    if (WindowsDiginetServer_BillType == "Cancel")
                    {
                        Log("Get Diginet Message Cancel");
                        if (WindowsDiginetServer_Type == "SERVER")
                        {
                            if (e is WebSocketSharp.MessageEventArgs)
                            {
                                string sign = (e as WebSocketSharp.MessageEventArgs).Data;
                                if (sign.Substring(0, 3) == "ERR")
                                {
                                    Log(sign);
                                    Common.browser.Load("javascript:showNotification('" + sign.Substring(3) + "');");
                                }
                                else
                                {
                                    if (WindowsDiginetServer_ContextData == "" || WindowsDiginetServer_ContextData == null)
                                    {
                                        String[] arrayString = sign.Split(new string[] { "https" }, StringSplitOptions.None);
                                        String simansi = arrayString[0].Substring(0, arrayString[0].Length - 1);
                                        String qrcode = "https" + arrayString[1];
                                        String aSign = sign.Replace("\n", "\\n");
                                        String aMessage = WindowsDiginetServer_message.Replace("\n", "\\n");
                                        Common.browser.Load("javascript:CanclelDigiNetCompleted('" + aSign + "','" + WindowsDiginetServer_BillCode + "','" + WindowsDiginetServer_InvoiceId + "','" + WindowsDiginetServer_CancelBillCode + "','" + aMessage + "','" + WindowsDiginetServer_lineIDs + "')");
                                        SaveLastCancelBillCode(WindowsDiginetServer_BillCode);
                                        Escpos esc = new Escpos();
                                        esc.PrintDigiNetESCPos(simansi, qrcode, WindowsDiginetServer_ip, WindowsDiginetServer_port, WindowsDiginetServer_message, WindowsDiginetServer_encoding, WindowsDiginetServer_codepage1, WindowsDiginetServer_codepage2, WindowsDiginetServer_codepage3);
                                    }
                                    else
                                    {
                                        String[] arrayString = sign.Split(new string[] { "https" }, StringSplitOptions.None);
                                        String simansi = arrayString[0].Substring(0, arrayString[0].Length - 1);
                                        String qrcode = "https" + arrayString[1];
                                        String aSign = sign.Replace("\n", "\\n");
                                        String aMessage = WindowsDiginetServer_message.Replace("\n", "\\n");
                                        String contextData = WindowsDiginetServer_ContextData.Replace("\n", "\\n").Replace("\r", "\\r").Replace("\'", "\\'").Replace("\u001B", "\\u001B").Replace("\u0000", "\\u0000").Replace("\u0001", "\\u0001").Replace("\0", "\\0");
                                        contextData = contextData.Replace("\\\"", "\\\\\"");
                                        //Save InvoiceBill to Database
                                        Common.browser.Load("javascript:BillCancelDigiNetCompleted('" + aSign + "','" + WindowsDiginetServer_BillCode + "','" + WindowsDiginetServer_InvoiceId + "','" + WindowsDiginetServer_lineIDs + "','" + aMessage + "','" + contextData + "','" + WindowsDiginetServer_fromIP + "')");
                                        //Inform Last number
                                        SaveLastCancelBillCode(WindowsDiginetServer_BillCode);
                                        //Print Invoice Bill
                                        if (WindowsDiginetServer_PrintType == "EscPos")
                                        {
                                            Escpos esc = new Escpos();
                                            esc.PrintDigiNetESCPos(simansi, qrcode, WindowsDiginetServer_ip, WindowsDiginetServer_port, WindowsDiginetServer_message, WindowsDiginetServer_encoding, WindowsDiginetServer_codepage1, WindowsDiginetServer_codepage2, WindowsDiginetServer_codepage3);
                                        }
                                    }
                                }
                            }
                        }
                        if (WindowsDiginetServer_Type == "CLIENT")
                        {
                            string sign = (e as WebSocketSharp.MessageEventArgs).Data;
                            if (sign.Substring(0, 3) == "ERR")
                            {
                                Escpos escpos = new Escpos();
                                escpos.SendMessage(WindowsDiginetServer_ip, "ErrorMessage^^^Δεν εκδόθηκε απόδειξη με κωδικό λάθους : " + sign.Substring(3) + " . Ελέγξτε τον φορολογικό μηχανισμό και απο την εφαρμογή Android Nanotax Driver κάντε Start και Stop για εξακριβώσετε την ομαλή λειτουργία του. ", sendPort);
                            }
                            else
                            {
                                String[] arrayString = sign.Split(new string[] { "https" }, StringSplitOptions.None);
                                String aSign = sign.Replace("\n", "\\n").Replace(";https", "https");
                                String aMessage = WindowsDiginetServer_message.Replace("\n", "\\n");
                                Common.browser.Load("javascript:CanclelDigiNetCompleted('" + aSign + "','" + WindowsDiginetServer_BillCode + "','" + WindowsDiginetServer_InvoiceId + "','" + WindowsDiginetServer_CancelBillCode + "','" + aMessage + "','" + WindowsDiginetServer_lineIDs + "')");
                                SaveLastCancelBillCode(WindowsDiginetServer_BillCode);
                                Escpos esc = new Escpos();

                                String SendMessage = "ReturnDiginetSign^^^" + aSign + "^^^" + aMessage + "^^^" + WindowsDiginetServer_InvoiceId + "^^^" + WindowsDiginetServer_lineIDs;
                                Escpos escpos = new Escpos();
                                escpos.SendMessage(WindowsDiginetServer_ip, SendMessage, sendPort);
                            }
                        }

                        DiginetIsPrinting = false;
                        ChextNextQueue();
                        return;
                    }
                    if (WindowsDiginetServer_BillType == "Order")
                    {
                        if (WindowsDiginetServer_Type == "SERVER")
                        {
                            if (e is WebSocketSharp.MessageEventArgs)
                            {
                                string sign = (e as WebSocketSharp.MessageEventArgs).Data;
                                if (sign.Substring(0, 3) == "ERR")
                                {
                                    Log(sign);
                                    Common.browser.Load("javascript:showNotification('" + sign.Substring(3) + "');");
                                }
                                else
                                {
                                    String[] arrayString = sign.Split(new string[] { "https" }, StringSplitOptions.None);
                                    String simansi = arrayString[0].Substring(0, arrayString[0].Length - 1);
                                    String qrcode = "https" + arrayString[1];
                                    String aSign = sign.Replace("\n", "\\n");
                                    String aMessage = WindowsDiginetServer_message.Replace("\n", "\\n");
                                    String contextData = WindowsDiginetServer_ContextData.Replace("\n", "\\n").Replace("\r", "\\r").Replace("\'", "\\'").Replace("\u001B", "\\u001B").Replace("\u0000", "\\u0000").Replace("\u0001", "\\u0001").Replace("\0", "\\0");
                                    //Save InvoiceBill to Database
                                    contextData = contextData.Replace("\\\"", "\\\\\"");
                                    Common.browser.Load("javascript:OrderDigiNetCompleted('" + aSign + "','" + WindowsDiginetServer_BillCode + "','" + WindowsDiginetServer_InvoiceId + "','" + WindowsDiginetServer_lineIDs + "','" + aMessage + "','" + contextData + "','" + WindowsDiginetServer_fromIP + "')");
                                    //Inform Last number
                                    SaveLastOrderCode(WindowsDiginetServer_BillCode);
                                    //Print Invoice Bill
                                    if (WindowsDiginetServer_PrintType == "EscPos")
                                    {
                                        Escpos esc = new Escpos();
                                        esc.PrintDigiNetESCPos(simansi, qrcode, WindowsDiginetServer_ip, WindowsDiginetServer_port, WindowsDiginetServer_message, WindowsDiginetServer_encoding, WindowsDiginetServer_codepage1, WindowsDiginetServer_codepage2, WindowsDiginetServer_codepage3);
                                    }
                                }
                            }
                        }

                        DiginetIsPrinting = false;
                        ChextNextQueue();
                        return;
                    }
                    if (WindowsDiginetServer_BillType == "OrderCancel")
                    {
                        if (WindowsDiginetServer_Type == "SERVER")
                        {
                            if (e is WebSocketSharp.MessageEventArgs)
                            {
                                string sign = (e as WebSocketSharp.MessageEventArgs).Data;
                                if (sign.Substring(0, 3) == "ERR")
                                {
                                    Log(sign);
                                    Common.browser.Load("javascript:showNotification('" + sign.Substring(3) + "');");
                                }
                                else
                                {
                                    String[] arrayString = sign.Split(new string[] { "https" }, StringSplitOptions.None);
                                    String simansi = arrayString[0].Substring(0, arrayString[0].Length - 1);
                                    String qrcode = "https" + arrayString[1];
                                    String aSign = sign.Replace("\n", "\\n");
                                    String aMessage = WindowsDiginetServer_message.Replace("\n", "\\n");
                                    String contextData = WindowsDiginetServer_ContextData.Replace("\n", "\\n").Replace("\r", "\\r").Replace("\'", "\\'").Replace("\u001B", "\\u001B").Replace("\u0000", "\\u0000").Replace("\u0001", "\\u0001").Replace("\0", "\\0");
                                    contextData = contextData.Replace("\\\"", "\\\\\"");
                                    //Save InvoiceBill to Database
                                    Common.browser.Load("javascript:OrderCancelDigiNetCompleted('" + aSign + "','" + WindowsDiginetServer_BillCode + "','" + WindowsDiginetServer_InvoiceId + "','" + WindowsDiginetServer_lineIDs + "','" + aMessage + "','" + contextData + "','" + WindowsDiginetServer_fromIP + "')");
                                    //Inform Last number
                                    SaveLastCancelOrderCode(WindowsDiginetServer_BillCode);
                                    //Print Invoice Bill
                                    if (WindowsDiginetServer_PrintType == "EscPos")
                                    {
                                        Escpos esc = new Escpos();
                                        esc.PrintDigiNetESCPos(simansi, qrcode, WindowsDiginetServer_ip, WindowsDiginetServer_port, WindowsDiginetServer_message, WindowsDiginetServer_encoding, WindowsDiginetServer_codepage1, WindowsDiginetServer_codepage2, WindowsDiginetServer_codepage3);
                                    }
                                }
                            }
                        }

                        DiginetIsPrinting = false;
                        ChextNextQueue();
                        return;
                    }
                }
                catch (System.Exception ex) {
                    Log(ex.Message);
                    DiginetIsPrinting = false;
                    ChextNextQueue();
                }
            }
            private void OnDiginetOpened(object sender, EventArgs e)
            {
            }
            private void OnDiginetError<TEventArgs>(object sender, TEventArgs e) {
                Log((e as WebSocketSharp.ErrorEventArgs).Message);
                if (WindowsDiginetServer_Type == "CLIENT")
                {
                    Escpos escpos = new Escpos();
                    if (e is WebSocketSharp.ErrorEventArgs)
                        escpos.SendMessage(WindowsDiginetServer_ip, "ErrorMessage^^^" + (e as WebSocketSharp.ErrorEventArgs).Message, sendPort);
                    else
                        escpos.SendMessage(WindowsDiginetServer_ip, "ErrorMessage^^^Error on printing with Diginet.", sendPort);
                }
                else
                {
                    if (e is WebSocketSharp.ErrorEventArgs)
                        Common.browser.Load("javascript:showNotification('" + (e as WebSocketSharp.ErrorEventArgs).Message + "');");
                    else
                        Common.browser.Load("javascript:showNotification('Error on printing with Diginet.');");
                }
                try
                {
                    if (Common.ws != null && Common.ws.ReadyState != WebSocketState.Closed)
                        Common.ws.Close();
                }
                catch (System.Exception e1) { }
                Common.ws = null;

                DiginetIsPrinting = false;
                DiginetQueue = new List<DiginetPrintData>();
            }
            private void OnDiginetClose(object sender, CloseEventArgs e) {
                DiginetIsPrinting = false;

                if (!e.WasClean)
                {
                    if (!Common.ws.IsAlive)
                    {
                        Common.ws.OnOpen -= new EventHandler(OnDiginetOpened);
                        Common.ws.OnMessage -= new EventHandler<MessageEventArgs>(OnDiginetMessage);
                        Common.ws.OnError -= new EventHandler<WebSocketSharp.ErrorEventArgs>(OnDiginetError);
                        Common.ws.OnClose -= new EventHandler<CloseEventArgs>(OnDiginetClose);

                        Thread.Sleep(1000);
                        StartNanotaxDriver();

                        ChextNextQueue();
                    }
                }
            }

            private System.Timers.Timer timer;
            private bool CheckForQueue(string windowsDiginetServer_BillType, string windowsDiginetServer_e_line,
                string windowsDiginetServer_message,
                string windowsDiginetServer_ip,
                string windowsDiginetServer_port,
                string windowsDiginetServer_encoding,
                string windowsDiginetServer_codepage1,
                string windowsDiginetServer_codepage2,
                string windowsDiginetServer_codepage3,
                string windowsDiginetServer_InvoiceId,
                string windowsDiginetServer_BillCode,
                string windowsDiginetServer_lineIDs,
                string windowsDiginetServer_Type,
                string windowsDiginetServer_CancelBillCode,
                string windowsDiginetServer_contextData,
                string windowsDiginetServer_fromIP,
                string windowsDiginetServer_PrintType)
            {
                var UseQueue = "1";
                try
                {
                    UseQueue = ConfigurationManager.AppSettings["UseQueue"].ToString();
                }
                catch (System.Exception exx) { }
                if (UseQueue == "0")
                {
                    DiginetIsPrinting = false;
                    DiginetQueue = new List<DiginetPrintData>();
                }


                if (DiginetIsPrinting == false)
                {
                    DiginetIsPrinting = true;
                    return false;
                }

                if (!DiginetQueue.Any(p => p.WindowsDiginetServer_InvoiceId == windowsDiginetServer_InvoiceId))
                {
                    DiginetQueue.Add(new DiginetPrintData()
                    {
                        WindowsDiginetServer_BillType = windowsDiginetServer_BillType,
                        WindowsDiginetServer_BillCode = windowsDiginetServer_BillCode,
                        WindowsDiginetServer_codepage1 = windowsDiginetServer_codepage1,
                        WindowsDiginetServer_codepage2 = windowsDiginetServer_codepage2,
                        WindowsDiginetServer_codepage3 = windowsDiginetServer_codepage3,
                        WindowsDiginetServer_encoding = windowsDiginetServer_encoding,
                        WindowsDiginetServer_e_line = windowsDiginetServer_e_line,
                        WindowsDiginetServer_InvoiceId = windowsDiginetServer_InvoiceId,
                        WindowsDiginetServer_ip = windowsDiginetServer_ip,
                        WindowsDiginetServer_lineIDs = windowsDiginetServer_lineIDs,
                        WindowsDiginetServer_message = windowsDiginetServer_message,
                        WindowsDiginetServer_port = windowsDiginetServer_port,
                        WindowsDiginetServer_Type = windowsDiginetServer_Type,
                        WindowsDiginetServer_CancelBillCode = windowsDiginetServer_CancelBillCode,
                        WindowsDiginetServer_contextData = windowsDiginetServer_contextData,
                        WindowsDiginetServer_fromIP = windowsDiginetServer_fromIP,
                        WindowsDiginetServer_PrintType = windowsDiginetServer_PrintType
                    });

                    if (timer != null)
                    {
                        timer.Stop();
                    }
                    timer = new System.Timers.Timer();
                    timer.Interval = 5000;
                    timer.Enabled = true;
                    timer.Elapsed += new System.Timers.ElapsedEventHandler(OnQueueTimedEvent);
                    return true;
                }
                else
                    return false;
            }
            private void OnQueueTimedEvent(object sender, ElapsedEventArgs e) {
                if (DiginetIsPrinting) {
                    DiginetIsPrinting = false;
                    DiginetQueue.Clear();
                }
            }
            private void ChextNextQueue() {
                try
                {
                    var UseQueue = "1";
                    try
                    {
                        UseQueue = ConfigurationManager.AppSettings["UseQueue"].ToString();
                    }
                    catch (System.Exception exx) { }
                    if (UseQueue == "0") {
                        DiginetQueue = new List<DiginetPrintData>();
                        return;
                    }

                    if (DiginetQueue.Count == 0) return;

                    DiginetPrintData data = DiginetQueue[0];
                    DiginetQueue.Remove(data);

                    if (data.WindowsDiginetServer_Type == "SERVER")
                        ServerDiginetPrint(data.WindowsDiginetServer_BillType, data.WindowsDiginetServer_e_line, data.WindowsDiginetServer_ip, data.WindowsDiginetServer_port, data.WindowsDiginetServer_message, data.WindowsDiginetServer_encoding, data.WindowsDiginetServer_codepage1, data.WindowsDiginetServer_codepage2, data.WindowsDiginetServer_codepage3, data.WindowsDiginetServer_InvoiceId, data.WindowsDiginetServer_BillCode, data.WindowsDiginetServer_lineIDs, data.WindowsDiginetServer_CancelBillCode, data.WindowsDiginetServer_contextData, data.WindowsDiginetServer_fromIP, data.WindowsDiginetServer_PrintType);

                    if (data.WindowsDiginetServer_Type == "CLIENT")
                        ClientDiginetPrint(data.WindowsDiginetServer_BillType, data.WindowsDiginetServer_e_line, data.WindowsDiginetServer_ip, data.WindowsDiginetServer_port, data.WindowsDiginetServer_message, data.WindowsDiginetServer_encoding, data.WindowsDiginetServer_codepage1, data.WindowsDiginetServer_codepage2, data.WindowsDiginetServer_codepage3, data.WindowsDiginetServer_InvoiceId, data.WindowsDiginetServer_BillCode, data.WindowsDiginetServer_lineIDs, data.WindowsDiginetServer_CancelBillCode, data.WindowsDiginetServer_fromIP);
                }
                catch (System.Exception ex) {
                }
            }


            public void executeReturnBillCodeForDiginetPrint(String eLine, String message, String returnIP, String InvoiceId, String lineIDs, String nextcode)
            {
                try {
                    if (CheckForQueue("Bill", eLine, message, returnIP, "", "", "", "", "", InvoiceId, nextcode, lineIDs, "CLIENT", "", "", "", ""))
                    {
                        return;
                    }

                    ClientDiginetPrint("Bill", eLine, returnIP, "", message, "", "", "", "", InvoiceId, nextcode, lineIDs, "", "");
                }
                catch (System.Exception ex) {
                }
            }
            #endregion

            #region  Diginet
            public void executeFHMOrderDiginet(String e_line, String ip, String port, String message, String encoding, String codepage1, String codepage2, String codepage3, String InvoiceId, String BillCode, String lineIDs, String contextData, String fromIP, String PrintType)
            {

                try
                {
                    if (CheckForQueue("Order", e_line, message, ip, port, encoding, codepage1, codepage2, codepage3, InvoiceId, BillCode, lineIDs, "SERVER", "", contextData, fromIP, PrintType))
                    {
                        Log("CheckForQueue ok");
                        return;
                    }

                    ServerDiginetPrint("Order", e_line, ip, port, message, encoding, codepage1, codepage2, codepage3, InvoiceId, BillCode, lineIDs, "", contextData, fromIP, PrintType);

                }
                catch (System.Exception ex)
                {
                }
            }
            public void executeFHMOrderCancelDiginet(String e_line, String ip, String port, String message, String encoding, String codepage1, String codepage2, String codepage3, String InvoiceId, String BillCode, String lineIDs, String contextData, String fromIP, String PrintType)
            {

                try
                {
                    if (CheckForQueue("OrderCancel", e_line, message, ip, port, encoding, codepage1, codepage2, codepage3, InvoiceId, BillCode, lineIDs, "SERVER", "", contextData, fromIP, PrintType))
                    {
                        Log("CheckForQueue ok");
                        return;
                    }

                    ServerDiginetPrint("OrderCancel", e_line, ip, port, message, encoding, codepage1, codepage2, codepage3, InvoiceId, BillCode, lineIDs, "", contextData, fromIP, PrintType);

                }
                catch (System.Exception ex)
                {
                }
            }

            public void executeFHMBillDiginet(String e_line, String ip, String port, String message, String encoding, String codepage1, String codepage2, String codepage3, String InvoiceId, String BillCode, String lineIDs, String contextData, String fromIP, String PrintType)
            {
                try
                {
                    if (CheckForQueue("Bill", e_line, message, ip, port, encoding, codepage1, codepage2, codepage3, InvoiceId, BillCode, lineIDs, "SERVER", "", contextData, fromIP, PrintType))
                    {
                        Log("CheckForQueue ok");
                        return;
                    }

                    ServerDiginetPrint("Bill", e_line, ip, port, message, encoding, codepage1, codepage2, codepage3, InvoiceId, BillCode, lineIDs, "", contextData, fromIP, PrintType);

                }
                catch (System.Exception ex)
                {
                }
            }
            public void executeFHMBillCancelDiginet(String e_line, String ip, String port, String message, String encoding, String codepage1, String codepage2, String codepage3, String InvoiceId, String BillCode, String lineIDs, String contextData, String fromIP, String PrintType)
            {

                try
                {
                    if (CheckForQueue("Cancel", e_line, message, ip, port, encoding, codepage1, codepage2, codepage3, InvoiceId, BillCode, lineIDs, "SERVER", "", contextData, fromIP, PrintType))
                    {
                        Log("CheckForQueue ok");
                        return;
                    }

                    ServerDiginetPrint("Cancel", e_line, ip, port, message, encoding, codepage1, codepage2, codepage3, InvoiceId, BillCode, lineIDs, "", contextData, fromIP, PrintType);

                }
                catch (System.Exception ex)
                {
                }
            }

            private string GetLastOrderCode()
            {
                try
                {
                    string fileName = System.Windows.Forms.Application.StartupPath + "\\" + "DiginetLastOrderValue.txt";
                    if (System.IO.File.Exists(fileName))
                    {
                        string text = System.IO.File.ReadAllText(fileName);
                        if (text != null && text != "")
                        {
                            return text;
                        }
                        else
                            return "0";
                    }
                    else
                    {
                        return "0";
                    }

                }
                catch (System.Exception ex)
                {
                    return "0";
                }
            }
            private string GetLastOrderCancelCode()
            {
                try
                {
                    string fileName = System.Windows.Forms.Application.StartupPath + "\\" + "DiginetLastOrderCancelValue.txt";
                    if (System.IO.File.Exists(fileName))
                    {
                        string text = System.IO.File.ReadAllText(fileName);
                        if (text != null && text != "")
                        {
                            return text;
                        }
                        else
                            return "0";
                    }
                    else
                    {
                        return "0";
                    }

                }
                catch (System.Exception ex)
                {
                    return "0";
                }
            }
            private void SaveLastOrderCode(string BillCode)
            {
                try
                {
                    string fileName = System.Windows.Forms.Application.StartupPath + "\\" + "DiginetLastOrderValue.txt";
                    using (System.IO.FileStream fs = System.IO.File.Create(fileName))
                    {
                        byte[] info = new UTF8Encoding(true).GetBytes(BillCode);
                        fs.Write(info, 0, info.Length);
                    }

                }
                catch (System.Exception ex)
                {
                }
            }
            private void SaveLastCancelOrderCode(string BillCode)
            {
                try
                {
                    string fileName = System.Windows.Forms.Application.StartupPath + "\\" + "DiginetLastOrderCancelValue.txt";
                    using (System.IO.FileStream fs = System.IO.File.Create(fileName))
                    {
                        byte[] info = new UTF8Encoding(true).GetBytes(BillCode);
                        fs.Write(info, 0, info.Length);
                    }

                }
                catch (System.Exception ex)
                {
                }
            }
            public void executePostDataToAndroid(String toIP, string context) {
                String returnIP = GetIP();
                String SendMessage = "PostDataToService^^^" + context + "^^^" + returnIP;
                Escpos escpos = new Escpos();
                escpos.SendMessage(toIP, SendMessage, sendPort);
            }
            #region RBS Check Last Sign
            public void executeRBSFHMGetSign(String SocketAddress, int SignNum)
            {
                try
                {
                    if (Common.wsLastSign == null)
                    {
                        Common.wsLastSign = new WebSocket("ws://" + SocketAddress);
                        Common.wsLastSign.OnMessage += new EventHandler<MessageEventArgs>(OnRbsFHMLastSignMessage);
                        Common.wsLastSign.OnError += new EventHandler<WebSocketSharp.ErrorEventArgs>(OnRbsFHMLastSignError);
                        Common.wsLastSign.OnClose += new EventHandler<CloseEventArgs>(OnRbsFHMLastSignClose);
                        Common.wsLastSign.Connect();
                    }

                    Common.wsLastSign.Send("$/" + SignNum.ToString() + "/");

                }
                catch (System.Exception ex)
                {
                    if (Common.wsLastSign != null)
                        Common.wsLastSign.Close();
                }
            }

            private void OnRbsFHMLastSignMessage<TEventArgs>(object sender, TEventArgs e)
            {
                try
                {
                    var result = (e as WebSocketSharp.MessageEventArgs).Data;

                    Common.browser.Load("javascript:RBSFHMCorrectSign('" + result + "');");
                }
                catch (System.Exception ex)
                {
                }
            }
            private void OnRbsFHMLastSignError<TEventArgs>(object sender, TEventArgs e)
            {
                try
                {
                    if (Common.wsLastSign != null)
                    {
                        if (Common.wsLastSign.ReadyState != WebSocketState.Closed)
                            Common.wsLastSign.Close();
                        Common.wsLastSign = null;
                    }
                }
                catch (System.Exception ex) { }
            }
            private void OnRbsFHMLastSignClose(object sender, CloseEventArgs e)
            {
                try
                {
                    if (!e.WasClean)
                    {
                        if (Common.wsLastSign != null && !Common.wsLastSign.IsAlive)
                        {
                            Common.wsLastSign.OnMessage -= new EventHandler<MessageEventArgs>(OnRbsFHMLastSignMessage);
                            Common.wsLastSign.OnError -= new EventHandler<WebSocketSharp.ErrorEventArgs>(OnRbsFHMLastSignError);
                            Common.wsLastSign.OnClose -= new EventHandler<CloseEventArgs>(OnRbsFHMLastSignClose);

                            Common.wsLastSign = null;
                        }
                    }
                }
                catch (System.Exception ex) { }
            }
            #endregion
            public void executePostDataToWindowsServer(String toIP, string context)
            {
                String message = context.Replace("\n", "\\n").Replace("\r", "\\r").Replace("\'", "\\'").Replace("\u001B", "\\u001B").Replace("\u0000", "\\u0000").Replace("\u0001", "\\u0001").Replace("\0", "\\0");

                String returnIP = GetIP();
                String SendMessage = message + "^^^" + returnIP;
                Escpos escpos = new Escpos();
                escpos.SendMessage(toIP, SendMessage, sendPort);
            }
            #endregion

            public void executeKDSPrinterSendData(String IPAddress, String message)
            {
                String returnIP = GetIP();
                message = message + "^^^" + returnIP;
                Escpos escpos = new Escpos();
                escpos.SendMessage(IPAddress, message, sendPort);
            }

            #region Diagnostic Test
            public void sendDiagnosticMessageESCPOS(String Id, String ip, String port, String encoding, String codepage1, String codepage2, String codepage3)
            {
                try
                {
                    Common.browser.Load("javascript:DiagnosticMessageRequest('" + Id + "')");

                    Escpos esc = new Escpos();
                    if (string.IsNullOrEmpty(port))
                    {
                        port = "9100";
                    }

                    Thread thread = new Thread(() => esc.DiagnosticTestConnection(Id, ip, port, encoding, codepage1, codepage2, codepage3));
                    thread.Start();

                    //bool result = esc.DiagnosticTestConnection(Id ,ip, port, encoding, codepage1, codepage2, codepage3);
                    //esc.Dispose();
                    //esc = null;
                }
                catch (System.Exception ex) {
                    System.Windows.Forms.MessageBox.Show(ex.Message);
                }

            }
            string Temp_Cash_ID = "";
            public void sendDiagnosticMessageRBSFHM(String Id, String SocketAddress) {
                Temp_Cash_ID = Id;
                try
                {
                    if (Common.ws == null)
                    {
                        Common.ws = new WebSocket("ws://" + SocketAddress);
                        Common.ws.OnMessage += new EventHandler<MessageEventArgs>(OnRbsDiagnosticMessage);
                        Common.ws.OnError += new EventHandler<WebSocketSharp.ErrorEventArgs>(OnRbsDiagnosticError);
                        Common.ws.OnClose += new EventHandler<CloseEventArgs>(OnRbsDiagnosticClose);
                        Common.ws.Connect();
                    }
                    Common.ws.Send("a/");
                }
                catch (System.Exception ex)
                {
                    if (Common.ws != null)
                        Common.ws.Close();
                }

            }
            private void OnRbsDiagnosticMessage<TEventArgs>(object sender, TEventArgs e)
            {
                var result = (e as WebSocketSharp.MessageEventArgs).Data;
                try {
                    Common.browser.Load("javascript:DiagnosticMessageAnswer('" + Temp_Cash_ID + "')");
                }
                catch (System.Exception ex) { }
            }
            private void OnRbsDiagnosticError<TEventArgs>(object sender, TEventArgs e)
            {
                try
                {
                    if (Common.ws != null)
                    {
                        if (Common.ws.ReadyState != WebSocketState.Closed)
                            Common.ws.Close();
                        Common.ws = null;
                    }
                }
                catch (System.Exception ex) { }
            }
            private void OnRbsDiagnosticClose(object sender, CloseEventArgs e)
            {
                try
                {
                    if (!e.WasClean)
                    {
                        if (Common.ws != null && !Common.ws.IsAlive)
                        {
                            Common.ws.OnOpen -= new EventHandler(OnDiginetOpened);
                            Common.ws.OnMessage -= new EventHandler<MessageEventArgs>(OnRbsFHMMessage);
                            Common.ws.OnError -= new EventHandler<WebSocketSharp.ErrorEventArgs>(OnRbsFHMError);
                            Common.ws.OnClose -= new EventHandler<CloseEventArgs>(OnRbsFHMClose);

                            Common.ws = null;
                        }
                    }
                }
                catch (System.Exception ex) { }
            }
            #endregion

            public PrintZData executeGetDiginetZ(string dateFrom, string dateTo, string CompanyName, string Afm, string address, string diginetModel) {
                string path = "C:\\NanoTax";

                string PrintData = "";
                string zBills = "";
                string zCancels = "";

                try
                {
                    DateTime from = DateTime.ParseExact(dateFrom, "yyyy-MM-ddTHH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                    DateTime to = DateTime.ParseExact(dateTo, "yyyy-MM-ddTHH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);

                    try
                    {
                        path = ConfigurationManager.AppSettings["DiginetSignsPath"].ToString();
                    }
                    catch (System.Exception exx) { }

                    string[] allfiles = System.IO.Directory.GetFiles(path, "*.*", System.IO.SearchOption.AllDirectories);
                    List<string> fileLines = new List<string>();

                    foreach (var filestr in allfiles)
                    {
                        System.IO.FileInfo file = new System.IO.FileInfo(filestr);

                        if (filestr.Substring(filestr.Length - 6) == "_e.txt")
                        {
                            using (System.IO.StreamReader reader = new System.IO.StreamReader(filestr))
                            {
                                string firstLine = reader.ReadLine() ?? "";
                                string[] items = firstLine.Split(';');

                                DateTime date = new DateTime(Convert.ToInt32(items[4].Substring(0, 4)), Convert.ToInt32(items[4].Substring(4, 2)), Convert.ToInt32(items[4].Substring(6, 2)), Convert.ToInt32(items[4].Substring(8, 2)), Convert.ToInt32(items[4].Substring(10, 2)), 0);
                                if (date >= from && date <= to)
                                {
                                    fileLines.Add(firstLine);
                                }
                            }
                        }
                    }

                    string newLine = "\n";

                    List<ZBill> Bills = new List<ZBill>();
                    List<ZBill> Cancels = new List<ZBill>();


                    PrintData += "Εταιρία : " + CompanyName + newLine;
                    PrintData += "ΑΦΜ : " + Afm + newLine;
                    PrintData += "Διεύθυνση : " + address + newLine;
                    PrintData += "Ημ/νία από : " + from.ToString() + newLine;
                    PrintData += "Ημ/νία εώς : " + to.ToString() + newLine;


                    if (fileLines.Count > 0)
                    {
                        String StartBill = "";
                        String ToBill = "";
                        int BillCount = 0;
                        decimal Net13 = 0;
                        decimal Vat13 = 0;
                        decimal Net24 = 0;
                        decimal Vat24 = 0;

                        String StartCancelBill = "";
                        String ToCancelBill = "";
                        int CancelBillCount = 0;
                        decimal CancelNet13 = 0;
                        decimal CancelVat13 = 0;
                        decimal CancelNet24 = 0;
                        decimal CancelVat24 = 0;

                        foreach (string line in fileLines)
                        {
                            string[] items = line.Split(';');
                            if (items[8] == "173")
                            {
                                if (StartBill == "") StartBill = items[10];
                                if (ToBill == "") ToBill = items[10];
                                if (Convert.ToInt32(items[10]) < Convert.ToInt32(StartBill))
                                    StartBill = items[10];
                                if (Convert.ToInt32(items[10]) > Convert.ToInt32(ToBill))
                                    ToBill = items[10];
                                BillCount++;

                                Net13 = Net13 + Convert.ToDecimal(items[13].Replace('.', ','));
                                Vat13 = Vat13 + Convert.ToDecimal(items[18].Replace('.', ','));
                                Net24 = Net24 + Convert.ToDecimal(items[12].Replace('.', ','));
                                Vat24 = Vat24 + Convert.ToDecimal(items[17].Replace('.', ','));

                                decimal totalValue = Convert.ToDecimal(items[13].Replace('.', ',')) + Convert.ToDecimal(items[18].Replace('.', ',')) + Convert.ToDecimal(items[12].Replace('.', ',')) + Convert.ToDecimal(items[17].Replace('.', ','));
                                DateTime date = new DateTime(Convert.ToInt32(items[4].Substring(0, 4)), Convert.ToInt32(items[4].Substring(4, 2)), Convert.ToInt32(items[4].Substring(6, 2)), Convert.ToInt32(items[4].Substring(8, 2)), Convert.ToInt32(items[4].Substring(10, 2)), 0);
                                Bills.Add(new ZBill() { BillDate = date, Data = items[10] + " " + totalValue.ToString() + " Euro " + date.ToString() });
                            }
                            if (items[8] == "175")
                            {
                                if (StartCancelBill == "") StartCancelBill = items[10];
                                if (ToCancelBill == "") ToCancelBill = items[10];
                                if (Convert.ToInt32(items[10]) < Convert.ToInt32(StartCancelBill))
                                    StartCancelBill = items[10];
                                if (Convert.ToInt32(items[10]) > Convert.ToInt32(ToCancelBill))
                                    ToCancelBill = items[10];
                                CancelBillCount++;

                                CancelNet13 = CancelNet13 - Math.Abs(Convert.ToDecimal(items[13].Replace('.', ',')));
                                CancelVat13 = CancelVat13 - Math.Abs(Convert.ToDecimal(items[18].Replace('.', ',')));
                                CancelNet24 = CancelNet24 - Math.Abs(Convert.ToDecimal(items[12].Replace('.', ',')));
                                CancelVat24 = CancelVat24 - Math.Abs(Convert.ToDecimal(items[17].Replace('.', ',')));

                                decimal totalValue = Convert.ToDecimal(items[13].Replace('.', ',')) + Convert.ToDecimal(items[18].Replace('.', ',')) + Convert.ToDecimal(items[12].Replace('.', ',')) + Convert.ToDecimal(items[17].Replace('.', ','));
                                DateTime date = new DateTime(Convert.ToInt32(items[4].Substring(0, 4)), Convert.ToInt32(items[4].Substring(4, 2)), Convert.ToInt32(items[4].Substring(6, 2)), Convert.ToInt32(items[4].Substring(8, 2)), Convert.ToInt32(items[4].Substring(10, 2)), 0);
                                Cancels.Add(new ZBill() { BillDate = date, Data = items[10] + " " + totalValue.ToString() + " Euro " + date.ToString() });
                            }
                        }


                        PrintData += "--------------------------------" + newLine;
                        PrintData += "Αποδείξεις :" + newLine;
                        PrintData += "Κινήσεις Από : " + StartBill + " Εώς " + ToBill + newLine;
                        PrintData += "Σύνολο κινήσεων :" + BillCount + newLine;

                        PrintData += newLine;

                        PrintData += "13%" + newLine;
                        PrintData += "Καθαρή Αξία : " + Net13.ToString() + newLine;
                        PrintData += "Καθαρή ΦΠΑ : " + Vat13.ToString() + newLine;
                        PrintData += "Σύνολο : " + (Vat13 + Net13).ToString() + newLine;

                        PrintData += newLine;

                        PrintData += "24%" + newLine;
                        PrintData += "Καθαρή Αξία : " + Net24.ToString() + newLine;
                        PrintData += "Καθαρή ΦΠΑ : " + Vat24.ToString() + newLine;
                        PrintData += "Σύνολο : " + (Vat24 + Net24).ToString() + newLine;

                        PrintData += "--------------------------------" + newLine;
                        PrintData += "Ακυρώσεις :" + newLine;
                        PrintData += "Κινήσεις Από : " + StartCancelBill + " Εώς " + ToCancelBill + newLine;
                        PrintData += "Σύνολο κινήσεων :" + CancelBillCount + newLine;

                        PrintData += newLine;

                        PrintData += "13%" + newLine;
                        PrintData += "Καθαρή Αξία : " + CancelNet13.ToString() + newLine;
                        PrintData += "Καθαρή ΦΠΑ : " + CancelVat13.ToString() + newLine;
                        PrintData += "Σύνολο : " + (CancelVat13 + CancelNet13).ToString() + newLine;

                        PrintData += newLine;

                        PrintData += "24%" + newLine;
                        PrintData += "Καθαρή Αξία : " + CancelNet24.ToString() + newLine;
                        PrintData += "Καθαρή ΦΠΑ : " + CancelVat24.ToString() + newLine;
                        PrintData += "Σύνολο : " + (CancelVat24 + CancelNet24).ToString() + newLine;

                        PrintData += "--------------------------------" + newLine;
                        PrintData += "Σύνολο :" + newLine;

                        PrintData += newLine;

                        PrintData += "13%" + newLine;
                        PrintData += "Καθαρή Αξία : " + (Net13 - CancelNet13).ToString() + newLine;
                        PrintData += "Καθαρή ΦΠΑ : " + (Vat13 - CancelVat13).ToString() + newLine;
                        PrintData += "Σύνολο : " + (Vat13 + Net13 + CancelNet13 + CancelVat13).ToString() + newLine;

                        PrintData += newLine;
                        PrintData += "24%" + newLine;
                        PrintData += "Καθαρή Αξία : " + (Net24 - CancelNet24).ToString() + newLine;
                        PrintData += "Καθαρή ΦΠΑ : " + (Vat24 - CancelVat24).ToString() + newLine;
                        PrintData += "Σύνολο : " + (Vat24 + Net24 + CancelNet24 + CancelVat24).ToString() + newLine;

                    }

                    foreach (ZBill b in Bills.OrderByDescending(p => p.BillDate))
                    {
                        zBills = zBills + b.Data.ToString() + newLine;
                    }

                    foreach (ZBill b in Cancels.OrderByDescending(p => p.BillDate))
                    {
                        zCancels = zCancels + b.Data.ToString() + newLine;
                    }
                }
                catch (System.Exception ex) {
                    System.Windows.Forms.MessageBox.Show(ex.Message);
                }

                PrintZData pzd = new PrintZData();
                pzd.ZReport = PrintData;
                pzd.ZBills = zBills;
                pzd.ZCancels = zCancels;
                return pzd;
            }

            #region RBS FHM
            public void executeRBSFHMBill(String SocketAddress, String e_line, String ip, String port, String message, String encoding, String codepage1, String codepage2, String codepage3, String InvoiceId, String BillCode, String lineIDs, String contextData, String fromIP, String PrintType)
            {
                RBSFHMCall(SocketAddress, e_line, ip, port, message, encoding, codepage1, codepage2, codepage3, InvoiceId, BillCode, lineIDs, contextData, fromIP, PrintType, "Bill");
            }
            public void executeRBSFHMBillCancel(String SocketAddress, String e_line, String ip, String port, String message, String encoding, String codepage1, String codepage2, String codepage3, String InvoiceId, String BillCode, String lineIDs, String contextData, String fromIP, String PrintType)
            {
                RBSFHMCall(SocketAddress, e_line, ip, port, message, encoding, codepage1, codepage2, codepage3, InvoiceId, BillCode, lineIDs, contextData, fromIP, PrintType, "Cancel");
            }
            public void executeRBSFHMOrder(String SocketAddress, String e_line, String ip, String port, String message, String encoding, String codepage1, String codepage2, String codepage3, String InvoiceId, String BillCode, String lineIDs, String contextData, String fromIP, String PrintType)
            {
                RBSFHMCall(SocketAddress, e_line, ip, port, message, encoding, codepage1, codepage2, codepage3, InvoiceId, BillCode, lineIDs, contextData, fromIP, PrintType, "Order");
            }
            public void executeRBSFHMOrderCancel(String SocketAddress, String e_line, String ip, String port, String message, String encoding, String codepage1, String codepage2, String codepage3, String InvoiceId, String BillCode, String lineIDs, String contextData, String fromIP, String PrintType)
            {
                RBSFHMCall(SocketAddress, e_line, ip, port, message, encoding, codepage1, codepage2, codepage3, InvoiceId, BillCode, lineIDs, contextData, fromIP, PrintType, "OrderCancel");
            }

            private void RBSFHMCall(String SocketAddress, String e_line, String ip, String port, String message, String encoding, String codepage1, String codepage2, String codepage3, String InvoiceId, String BillCode, String lineIDs, String contextData, String fromIP, String PrintType, String BillType) {

                if (!RBSBillItemsQueue.Any(p => p.InvoiceId == InvoiceId))
                {
                    BillItem item = new BillItem();
                    item.SocketAddress = SocketAddress;
                    item.e_line = e_line;
                    item.ip = ip;
                    item.port = port;
                    item.message = message;
                    item.encoding = encoding;
                    item.codepage1 = codepage1;
                    item.codepage2 = codepage2;
                    item.codepage3 = codepage3;
                    item.lineIDs = lineIDs;
                    item.contextData = contextData;
                    item.fromIP = fromIP;
                    item.PrintType = PrintType;
                    item.BillType = BillType;
                    item.InvoiceId = InvoiceId;
                    item.BillCode = BillCode;
                    RBSBillItemsQueue.Add(item);

                    RBSBillQueueExecute();
                }
            }
            private bool RBSFHMIsExecuting = false;
            private List<BillItem> RBSBillItemsQueue = new List<BillItem>();
            private void RBSBillQueueExecute()
            {
                if (RBSBillItemsQueue.Count == 0)
                {
                    return;
                }
                if (RBSFHMIsExecuting == true) return;
                RBSFHMIsExecuting = true;

                BillItem item = RBSBillItemsQueue[0];

                if (item.BillType == "Bill")
                {
                    String lastBillCode = GetLastBillCode();
                    if (lastBillCode != null && lastBillCode != "")
                    {
                        int SendCode = Convert.ToInt32(RBSBillItemsQueue[0].BillCode.Substring(RBSBillItemsQueue[0].BillCode.Length - 8));
                        int LastCode = Convert.ToInt32(lastBillCode.Substring(RBSBillItemsQueue[0].BillCode.Length - 8));
                        if (SendCode <= LastCode)
                        {
                            Common.browser.Load("javascript:FixDiginetBillCodeByLastCode(" + LastCode + ")");

                            String retMessage = RBSBillItemsQueue[0].contextData.Replace("\n", "\\n").Replace("\r", "\\r").Replace("\'", "\\'").Replace("\u001B", "\\u001B").Replace("\u0000", "\\u0000").Replace("\u0001", "\\u0001").Replace("\0", "\\0");
                            retMessage = retMessage.Replace("\\\"", "\\\\\"");
                            Common.browser.Load("javascript:PostDataFromClient('" + retMessage + "','" + RBSBillItemsQueue[0].fromIP + "',1)");

                            RBSBillItemQueueCompleted();
                            return;
                        }
                    }
                }
                if (item.BillType == "Cancel")
                {
                    String lastBillCode = GetLastCancelBillCode();
                    if (lastBillCode != null && lastBillCode != "")
                    {
                        int SendCode = Convert.ToInt32(RBSBillItemsQueue[0].BillCode.Substring(RBSBillItemsQueue[0].BillCode.Length - 8));
                        int LastCode = Convert.ToInt32(lastBillCode.Substring(RBSBillItemsQueue[0].BillCode.Length - 8));
                        if (SendCode <= LastCode)
                        {
                            Common.browser.Load("javascript:FixDiginetCancelCodeByLastCode(" + LastCode + ")");
                            String retMessage = RBSBillItemsQueue[0].contextData.Replace("\n", "\\n").Replace("\r", "\\r").Replace("\'", "\\'").Replace("\u001B", "\\u001B").Replace("\u0000", "\\u0000").Replace("\u0001", "\\u0001").Replace("\0", "\\0");
                            retMessage = retMessage.Replace("\\\"", "\\\\\"");
                            Common.browser.Load("javascript:PostDataFromClient('" + retMessage + "','" + RBSBillItemsQueue[0].fromIP + "',1)");

                            RBSBillItemQueueCompleted();
                            return;
                        }
                    }
                }
                if (item.BillType == "Order")
                {
                    String lastBillCode = GetLastOrderCode();
                    if (lastBillCode != null && lastBillCode != "")
                    {
                        int SendCode = Convert.ToInt32(RBSBillItemsQueue[0].BillCode.Substring(RBSBillItemsQueue[0].BillCode.Length - 8));
                        int LastCode = Convert.ToInt32(lastBillCode.Substring(RBSBillItemsQueue[0].BillCode.Length - 8));
                        if (SendCode <= LastCode)
                        {
                            Common.browser.Load("javascript:FixDiginetOrderCodeByLastCode(" + LastCode + ")");
                            String retMessage = RBSBillItemsQueue[0].contextData.Replace("\n", "\\n").Replace("\r", "\\r").Replace("\'", "\\'").Replace("\u001B", "\\u001B").Replace("\u0000", "\\u0000").Replace("\u0001", "\\u0001").Replace("\0", "\\0");
                            retMessage = retMessage.Replace("\\\"", "\\\\\"");
                            Common.browser.Load("javascript:PostDataFromClient('" + retMessage + "','" + RBSBillItemsQueue[0].fromIP + "',1)");

                            RBSBillItemQueueCompleted();
                            return;
                        }
                    }
                }
                if (item.BillType == "OrderCancel")
                {
                    String lastBillCode = GetLastOrderCancelCode();
                    if (lastBillCode != null && lastBillCode != "")
                    {
                        int SendCode = Convert.ToInt32(RBSBillItemsQueue[0].BillCode.Substring(RBSBillItemsQueue[0].BillCode.Length - 8));
                        int LastCode = Convert.ToInt32(lastBillCode.Substring(RBSBillItemsQueue[0].BillCode.Length - 8));
                        if (SendCode <= LastCode)
                        {
                            Common.browser.Load("javascript:FixDiginetOrderCancelCodeByLastCode(" + LastCode + ")");
                            String retMessage = RBSBillItemsQueue[0].contextData.Replace("\n", "\\n").Replace("\r", "\\r").Replace("\'", "\\'").Replace("\u001B", "\\u001B").Replace("\u0000", "\\u0000").Replace("\u0001", "\\u0001").Replace("\0", "\\0");
                            retMessage = retMessage.Replace("\\\"", "\\\\\"");
                            Common.browser.Load("javascript:PostDataFromClient('" + retMessage + "','" + RBSBillItemsQueue[0].fromIP + "',1)");

                            RBSBillItemQueueCompleted();
                            return;
                        }
                    }
                }



                try
                {
                    if (Common.ws == null)
                    {
                        Common.ws = new WebSocket("ws://" + RBSBillItemsQueue[0].SocketAddress);
                        Common.ws.OnMessage += new EventHandler<MessageEventArgs>(OnRbsFHMMessage);
                        Common.ws.OnError += new EventHandler<WebSocketSharp.ErrorEventArgs>(OnRbsFHMError);
                        Common.ws.OnClose += new EventHandler<CloseEventArgs>(OnRbsFHMClose);
                        Common.ws.Connect();
                    }

                    RBSBillSendELine();

                    RBSFHMTimer = new System.Threading.Timer(RBSFHMTimer_CallBack, null, 5000, Timeout.Infinite);

                }
                catch (System.Exception ex)
                {
                    RBSFHMIsExecuting = false;
                    if (Common.ws != null)
                        Common.ws.Close();
                    RBSBillQueueExecute();
                }

            }

            System.Threading.Timer RBSFHMTimer;
            private void RBSFHMTimerStop() {
                try
                {
                    if (RBSFHMTimer != null)
                    {
                        ManualResetEvent waitHandle = new ManualResetEvent(false);
                        if (RBSFHMTimer.Dispose(waitHandle))
                        {
                            if (!waitHandle.WaitOne(2000))
                                throw new TimeoutException("Timeout waiting for timer to stop");
                        }
                        waitHandle.Close();   // Only close if Dispose has completed succesful
                        RBSFHMTimer = null;
                    }
                }
                catch (System.Exception ex) { }
                RBSFHMTimer = null;
            }
            private void RBSFHMTimer_CallBack(Object state)
            {
                RBSFHMIsExecuting = false;
                if (Common.ws != null)
                    Common.ws.Close();
                RBSBillQueueExecute();
            }

            private void RBSBillSendELine() {
                RBSBillItemsQueue[0].SendSocketMessageStatus = "Eline";
                Common.ws.Send("^/");
            }
            private void RBSBillItemQueueCompleted() {
                if (RBSBillItemsQueue.Count > 0) {
                    RBSBillItemsQueue.RemoveAt(0);
                }

                RBSFHMIsExecuting = false;
                RBSBillQueueExecute();
            }
            private void CheckRBSErrorCode(string code) {
                string message = "";
                if (code == "01") message = @"Wrong number of fields . Check the command's field count ";
                if (code == "02") message = @"Field too long . A field is long: check it & retry";
                if (code == "03") message = @"Field too small . A field is small: check it & retry";
                if (code == "04") message = @"Field fixed size mismatch . A field size is wrong: check it & retry";
                if (code == "05") message = @"Field range or type check failed . Check ranges or types in command";
                if (code == "06") message = @"Bad request code . Correct the request code (unknown)";
                if (code == "09") message = @"Printing type bad . Correct the specified printing style";
                if (code == "0A") message = @"Cannot execute with day open . Issue a Z report to close the day ";
                if (code == "0B") message = @"RTC programming requires jumper . Short the 'clock' jumper and retry ";
                if (code == "0C") message = @"RTC date or time invalid . Check the date/time range. Also check if date is prior to a date of a fiscal record.";
                if (code == "0D") message = @"No records in fiscal period . No suggested action; the operation cannot be executed in the specified period";
                if (code == "0E") message = @"Device is busy in another task . Wait for the device to get ready";
                if (code == "0F") message = @"No more header records allowed . No suggested action; the header programming cannot be executed because the fiscal memory cannot hold more records";
                if (code == "10") message = @"Cannot execute with block open . The specified command requires no open signature block for proceeding. Close the block and retry.";
                if (code == "11") message = @"Block not open . The specified command requires a signature block to be open to execute. Open a block and retry.";
                if (code == "12") message = @"Bad data stream . Means that the passed data to be signed are of incorrect format. The expected format is in HEX (hexadecimal) pairs,so expected field must have an even size and its contents must be in range 0-9 or A-F inclusive.";
                if (code == "13") message = @"Bad signature field . Means that the passed signature is of incorrect format. The expected format is of 40 characters formatted as 20 HEX (hexadecimal) pairs.";
                if (code == "14") message = @"Z closure time limit . Means that 24 hours passed from the last Z closure. Issue a Z and retry. ";
                if (code == "15") message = @"Z closure not found . The specified Z closure number does not exist. Pass an existing Z number.";
                if (code == "16") message = @"Z closure record bad . The requested Z record is unreadable (damaged). Device requires service";
                if (code == "17") message = @"User browsing in progress . The user is accessing the device by manual operation. The protocol usage is suspended until the user terminates the keyboard browsing. Just wait or inform application user.";
                if (code == "18") message = @"Signature daily limit reached . The max number of signatures in a day have been issued. A Z closure is needed to free the daily storage memory. ";
                if (code == "19") message = @"Printer paper end detected . Replace the paper roll and retry ";
                if (code == "1A") message = @"Printer is offline . Printer disconnection. Service required";
                if (code == "1B") message = @"Fiscal unit is offline . Fiscal disconnection. Service required.";
                if (code == "1C") message = @"Fatal hardware error . Mostly fiscal errors. Service required.";
                if (code == "1D") message = @"Fiscal unit is full . Need fiscal replacement. Service ";
                if (code == "1E") message = @"No data passed for signature . Need to pass some data to close block";
                if (code == "1F") message = @"Signature does not exist . Correct requested signature number";
                if (code == "20") message = @"Battery fault detected . If problem persists, service required";
                if (code == "21") message = @"Recovery in progress . This command is not allowed when a recovery has started. Finish the recovery procedure and retry";
                if (code == "22") message = @"Recovery only after CMOS reset . Attempted to initiate a recovery procedure without a previous CMOS reset. The recovery is not needed. ";
                if (code == "23") message = @"Real-Time Clock needs programming . This means that the RTC has invalid data and needs to be reprogrammed. As a consequence, service is needed.";
                if (code == "24") message = @"Z closure date warning . This is an error returned by a closure request, when the RTC s date has a value at least 48 hours later than the last closure time stamp (see XZreport)";
                if (code == "25") message = @"Bad character in stream . This error is returned when a stream sent contains one or more invalid characters. A table of allowed binary values is defined in table 2. This error means that device has rejected the specified frame. A filtering of data sent to the device must be performed by host.";

                message = code + " . " + message;

                if (RBSBillItemsQueue[0].fromIP == "")
                {
                    Common.browser.Load("javascript:showNotification('" + message + "');");
                }
                else
                {
                    Escpos escpos = new Escpos();
                    escpos.SendMessage(RBSBillItemsQueue[0].fromIP, "ErrorMessage^^^" + message, sendPort);
                }
                RBSBillItemQueueCompleted();
            }
            private void OnRbsFHMMessage<TEventArgs>(object sender, TEventArgs e)
            {
                RBSFHMTimerStop();

                try
                {
                    if (RBSBillItemsQueue[0].SendSocketMessageStatus == "Z")
                    {
                        RBSBillItemsQueue[0].SendSocketMessageStatus = "SendZ";
                        Common.ws.Send("x/2/0");
                        return;
                    }
                    if (RBSBillItemsQueue[0].SendSocketMessageStatus == "SendZ")
                    {
                        RBSBillItemsQueue[0].SendSocketMessageStatus = "Eline";
                        Common.ws.Send("^/");
                        return;
                    }
                    if (RBSBillItemsQueue[0].SendSocketMessageStatus == "Eline")
                    {
                        RBSBillItemsQueue[0].SendSocketMessageStatus = "Message";
                        Common.ws.Send(RBSBillItemsQueue[0].e_line);
                        return;
                    }
                    if (RBSBillItemsQueue[0].SendSocketMessageStatus == "Message")
                    {
                        var result = (e as WebSocketSharp.MessageEventArgs).Data;
                        if (result.Substring(0, 2) == "14") {
                            RBSBillItemsQueue[0].SendSocketMessageStatus = "Z";
                            Common.ws.Send("^/");
                            return;
                        }
                        if (result.Substring(0, 2) != "00") {
                            CheckRBSErrorCode(result.Substring(0, 2));
                            return;
                        }

                        string message = RBSBillItemsQueue[0].message;
                        message = message.Replace("\u0001", " ");
                        message = message.Replace("\u0003", " ");
                        message = message.Replace("\u0004", " ");
                        message = message.Replace("\u0005", " ");
                        message = message.Replace("\u0006", " ");
                        message = message.Replace("\a", " ");
                        message = message.Replace("\b", " ");
                        message = message.Replace("\v", " ");
                        message = message.Replace("\u000e", " ");
                        message = message.Replace("\u000f", " ");
                        message = message.Replace("\u0010", " ");
                        message = message.Replace("\u0011", " ");
                        message = message.Replace("\u0012", " ");
                        message = message.Replace("\u0013", " ");
                        message = message.Replace("\u0014", " ");
                        message = message.Replace("\u0015", " ");
                        message = message.Replace("\u0016", " ");
                        message = message.Replace("\u0017", " ");
                        message = message.Replace("\u0018", " ");
                        message = message.Replace("\u0019", " ");
                        message = message.Replace("\u001b", " ");
                        message = message.Replace("\u007f", " ");

                        message = message.Replace("ƒ", " ");
                        message = message.Replace("„", " ");
                        message = message.Replace("…", " ");
                        message = message.Replace("†", " ");
                        message = message.Replace("‡", " ");
                        message = message.Replace("‰", " ");
                        message = message.Replace("‹", " ");
                        message = message.Replace("‘", " ");
                        message = message.Replace("’", " ");
                        message = message.Replace("“", " ");

                        message = message.Replace("”", " ");
                        message = message.Replace("•", " ");
                        message = message.Replace("–", " ");
                        message = message.Replace("—", " ");
                        message = message.Replace("™", " ");
                        message = message.Replace("›", " ");
                        message = message.Replace("­", " ");
                        message = message.Replace("", " ");

                        if (message.Length <= 500)
                        {
                            RBSBillItemsQueue[0].SendSocketMessageStatus = "Close";
                            Common.ws.Send("@/" + message);
                        }
                        else
                        {
                            int chunkSize = 500;
                            int stringLength = message.Length;
                            List<string> messages = new List<string>();
                            for (int i = 0; i < stringLength; i += chunkSize)
                            {
                                if (i + chunkSize > stringLength) chunkSize = stringLength - i;
                                messages.Add(message.Substring(i, chunkSize));

                            }

                            if (messages.Count() == 1)
                            {
                                RBSBillItemsQueue[0].SendSocketMessageStatus = "Close";
                                Common.ws.Send("@/" + message);
                            }
                            else
                            {
                                if (RBSBillItemsQueue[0].MessageNum == messages.Count() - 1)
                                {
                                    RBSBillItemsQueue[0].SendSocketMessageStatus = "Close";
                                    Common.ws.Send("@/" + messages.ElementAt(RBSBillItemsQueue[0].MessageNum));
                                }
                                else
                                {
                                    RBSBillItemsQueue[0].SendSocketMessageStatus = "Message";
                                    Common.ws.Send("@/" + messages.ElementAt(RBSBillItemsQueue[0].MessageNum));
                                }
                                RBSBillItemsQueue[0].MessageNum++;

                            }
                        }
                        return;
                    }
                    if (RBSBillItemsQueue[0].SendSocketMessageStatus == "Close")
                    {
                        var result = (e as WebSocketSharp.MessageEventArgs).Data;
                        if (result.Substring(0, 2) != "00")
                        {
                            CheckRBSErrorCode(result.Substring(0, 2));
                            return;
                        }

                        RBSBillItemsQueue[0].SendSocketMessageStatus = "End";
                        Common.ws.Send("}/");
                        return;
                    }
                    if (RBSBillItemsQueue[0].SendSocketMessageStatus == "End")
                    {
                        var result = (e as WebSocketSharp.MessageEventArgs).Data;

                        BillItem item = RBSBillItemsQueue[0];

                        if (result.Substring(0, 2) == "00")
                        {
                            string[] results = result.Split('/');
                            string[] eSignature = results[8].Split(';');

                            string totalNum = "000000" + results[3];
                            totalNum = totalNum.Substring(totalNum.Length - 8, 8);

                            String simansi = results[7] + " " + eSignature[5] + " " + eSignature[6] + " " + results[5] + results[6] + " " + results[9];

                            string totalValue = eSignature[20];
                            if (totalValue == "0") totalValue = "0.000";

                            String qrcode = "https://www1.aade.gr/tameiakes/myweb/q1.php?SIG=" + results[9] + "0" + eSignature[6] + eSignature[22] + totalValue;
                            String aSign = simansi + qrcode;
                            String aMessage = item.message.Replace("\n", "\\n");
                            String contextData = item.contextData.Replace("\n", "\\n").Replace("\r", "\\r").Replace("\'", "\\'").Replace("\u001B", "\\u001B").Replace("\u0000", "\\u0000").Replace("\u0001", "\\u0001").Replace("\0", "\\0");
                            contextData = contextData.Replace("\\\"", "\\\\\"");

                            item.lineIDs = item.lineIDs + "~~~" + eSignature[7];

                            if (item.BillType == "Bill")
                            {
                                Common.browser.Load("javascript:BillDigiNetCompleted('" + aSign + "','" + item.BillCode + "','" + item.InvoiceId + "','" + item.lineIDs + "','" + aMessage + "','" + contextData + "','" + item.fromIP + "')");
                                SaveLastBillCode(item.BillCode);
                            }
                            if (item.BillType == "Cancel")
                            {
                                Common.browser.Load("javascript:BillCancelDigiNetCompleted('" + aSign + "','" + item.BillCode + "','" + item.InvoiceId + "','" + item.lineIDs + "','" + aMessage + "','" + contextData + "','" + item.fromIP + "')");
                                SaveLastCancelBillCode(item.BillCode);
                            }
                            if (item.BillType == "Order")
                            {
                                Common.browser.Load("javascript:OrderDigiNetCompleted('" + aSign + "','" + item.BillCode + "','" + item.InvoiceId + "','" + item.lineIDs + "','" + aMessage + "','" + contextData + "','" + item.fromIP + "')");
                                SaveLastOrderCode(item.BillCode);
                            }
                            if (item.BillType == "OrderCancel")
                            {
                                Common.browser.Load("javascript:OrderCancelDigiNetCompleted('" + aSign + "','" + item.BillCode + "','" + item.InvoiceId + "','" + item.lineIDs + "','" + aMessage + "','" + contextData + "','" + item.fromIP + "')");
                                SaveLastCancelOrderCode(item.BillCode);
                            }

                            if (item.PrintType == "EscPos")
                            {
                                Escpos esc = new Escpos();
                                esc.PrintDigiNetESCPos(simansi, qrcode, item.ip, item.port, item.message, item.encoding, item.codepage1, item.codepage2, item.codepage3);
                            }
                        }
                        else
                        {
                            CheckRBSErrorCode(result.Substring(0, 2));
                            return;
                        }

                        Thread.Sleep(1000);

                        RBSBillItemQueueCompleted();
                        return;
                    }

                }
                catch (System.Exception ex) {
                    // return error RBS ?????

                    RBSBillItemQueueCompleted();
                }
            }
            private void OnRbsFHMError<TEventArgs>(object sender, TEventArgs e) {
                try
                {
                    if (Common.ws != null)
                    {
                        if (Common.ws.ReadyState != WebSocketState.Closed)
                            Common.ws.Close();
                        Common.ws = null;
                    }
                }
                catch (System.Exception ex) { }
                RBSBillItemQueueCompleted();
            }
            private void OnRbsFHMClose(object sender, CloseEventArgs e)
            {
                RBSFHMIsExecuting = false;

                try
                {
                    if (!e.WasClean)
                    {
                        if (Common.ws != null && !Common.ws.IsAlive)
                        {
                            Common.ws.OnOpen -= new EventHandler(OnDiginetOpened);
                            Common.ws.OnMessage -= new EventHandler<MessageEventArgs>(OnRbsFHMMessage);
                            Common.ws.OnError -= new EventHandler<WebSocketSharp.ErrorEventArgs>(OnRbsFHMError);
                            Common.ws.OnClose -= new EventHandler<CloseEventArgs>(OnRbsFHMClose);

                            Common.ws = null;
                        }
                    }
                }
                catch (System.Exception ex) { }

                RBSFHMIsExecuting = false;
                RBSBillQueueExecute();
            }
            #endregion

            #region RBS TAX
            private List<RBSTAXItem> RBSTAXItemsQueue = new List<RBSTAXItem>();
            private bool RBSTAXIsExecuting = false;
            private List<RBSTAXFail> RBSTAXFails = new List<RBSTAXFail>();
            public void executeRBSTAXPrint(string ip, string message, String InvoiceId, String lineIDs, String fromIP) {
                if (!RBSTAXItemsQueue.Any(p => p.InvoiceId == InvoiceId)) {
                    RBSTAXItem item = new RBSTAXItem();
                    item.ip = ip;
                    item.message = message;
                    item.InvoiceId = InvoiceId;
                    item.lineIDs = lineIDs;
                    item.fromIP = fromIP;
                    RBSTAXItemsQueue.Add(item);

                    if (timer != null)
                    {
                        timer.Stop();
                    }
                    //timer = new System.Timers.Timer();
                    //timer.Interval = 20000;
                    //timer.Enabled = true;
                    //timer.Elapsed += new System.Timers.ElapsedEventHandler(OnQueueRBSTAXTimedEvent);

                    RBSTAXQueueExecute();
                }
            }
            private void OnQueueRBSTAXTimedEvent(object sender, ElapsedEventArgs e)
            {
                if (RBSTAXIsExecuting)
                {
                    RBSTAXIsExecuting = false;
                    RBSTaxItemQueueCompleted();
                }
            }
            private void RBSTAXQueueExecute()
            {
                if (RBSTAXItemsQueue.Count == 0)
                {
                    return;
                }
                if (RBSTAXIsExecuting == true) return;
                RBSTAXIsExecuting = true;

                RBSTAXItem item = RBSTAXItemsQueue[0];

                if (Common.ws != null)
                {
                    RBSTaxItemQueueCompleted();
                }
                else
                {
                    Common.ws = new WebSocket("ws://" + item.ip);
                    Common.ws.OnMessage += new EventHandler<MessageEventArgs>(OnRbsTaxMessage);
                    Common.ws.OnError += new EventHandler<WebSocketSharp.ErrorEventArgs>(OnRbsTaxError);
                    Common.ws.Connect();

                    RBSTAXFails = new List<RBSTAXFail>();
                    string[] parts = item.message.Split(';');
                    PartsCount = parts.Length;
                    PartsNum = 0;
                    //     PartsNum = 1; Old
                    Common.ws.Send(parts[0]);
                }

            }
            private void OnRbsTaxMessage<TEventArgs>(object sender, TEventArgs e)
            {

                try {
                    string res = (e as WebSocketSharp.MessageEventArgs).Data;
                    string result = res.Split('/').FirstOrDefault();
                    String[] separated = RBSTAXItemsQueue[0].message.Split(';');
                    PartsNum++;

                    if (PartsNum == PartsCount)
                    {

                        if (result == "00" || result == "2C" || result == "5C")
                        {
                            if (RBSTAXFails.Count == 0)
                            {
                                Common.browser.Load("javascript:BillDigiNetCompleted('" + Guid.NewGuid() + "','','" + RBSTAXItemsQueue[0].InvoiceId + "','" + RBSTAXItemsQueue[0].lineIDs + "','" + RBSTAXItemsQueue[0].message + "','','" + RBSTAXItemsQueue[0].fromIP + "')");
                            }
                            else
                            {
                                var errors = JsonConvert.SerializeObject(RBSTAXFails);

                                Common.browser.Load("javascript:RBSTaxBillCompletedWithErrors('" + Guid.NewGuid() + "','','" + RBSTAXItemsQueue[0].InvoiceId + "','" + RBSTAXItemsQueue[0].lineIDs + "','" + RBSTAXItemsQueue[0].message + "','','" + RBSTAXItemsQueue[0].fromIP + "','" + errors + "')");

                            }
                            RBSTAXFails.Clear();
                        }
                        else
                        {
                            RBSTAXFails.Clear();
                            CheckRBSTaxErrorCode(result);
                        }

                        Common.ws.Close();
                        Common.ws = null;
                        RBSTaxItemQueueCompleted();

                    }
                    else
                    {
                        if (result != "00" && result != "15")
                        {
                            RBSTAXFails.Add(new RBSTAXFail() { PartNum = PartsNum - 1, Error = RBSTaxGetErrorMessage(result), Description = separated[PartsNum - 1] });
                        }

                        Common.ws.Send(separated[PartsNum]);
                    }

                }
                catch (System.Exception ex) {
                    Log("RBS TAX OnMessage Error : " + ex.Message);
                    RBSTaxItemQueueCompleted();
                }


                //Old
                //if (PartsNum < PartsCount)
                //{
                //    PartsNum++;
                //    String[] separated = RBSTAXItemsQueue[0].message.Split(';');
                //    Common.ws.Send(separated[PartsNum - 1]);
                //    if (PartsNum == PartsCount)
                //    {
                //        if (e is WebSocketSharp.MessageEventArgs)
                //        {
                //            string res = (e as WebSocketSharp.MessageEventArgs).Data;
                //            if (res.Substring(0, 2) == "00")
                //            {

                //                Common.browser.Load("javascript:BillDigiNetCompleted('"+ Guid.NewGuid() +"','','" + RBSTAXItemsQueue[0].InvoiceId + "','" + RBSTAXItemsQueue[0].lineIDs + "','" + RBSTAXItemsQueue[0].message + "','','" + RBSTAXItemsQueue[0].fromIP + "')");
                //            }
                //            else
                //            {
                //                CheckRBSTaxErrorCode(res.Split('/').FirstOrDefault());

                //            }
                //        }
                //        else
                //        {
                //            string message = "Error on printing with RBS. Null EventArgs";
                //            Common.browser.Load("javascript:PostDBInvoiceBillError('','','" + RBSTAXItemsQueue[0].InvoiceId + "','" + RBSTAXItemsQueue[0].message + "','" + message + "');");
                //            if (RBSTAXItemsQueue[0].fromIP == null || RBSTAXItemsQueue[0].fromIP == "")
                //            {
                //                Common.browser.Load("javascript:showNotification('Error on printing with RBS. " + message + "');");
                //            }
                //            else
                //            {
                //                Escpos escpos = new Escpos();
                //                escpos.SendMessage(RBSTAXItemsQueue[0].fromIP, "ErrorMessage^^^" + message, sendPort);
                //            }

                //        }
                //        Common.ws.Close();
                //        Common.ws = null;
                //        RBSTaxItemQueueCompleted();
                //    }
                //}
            }
            private void RBSTaxItemQueueCompleted()
            {
                RBSTAXIsExecuting = false;
                if (RBSTAXItemsQueue.Count > 0)
                {
                    RBSTAXItemsQueue.RemoveAt(0);
                }
                if (Common.ws != null && Common.ws.ReadyState != WebSocketState.Closed) {
                    Common.ws.Close();
                }
                Common.ws = null;
                RBSTAXQueueExecute();
            }
            private void OnRbsTaxError<TEventArgs>(object sender, TEventArgs e)
            {
                string message = "Error on printing with RBS.";
                if (e is WebSocketSharp.ErrorEventArgs)
                    message = (e as WebSocketSharp.ErrorEventArgs).Message;

                if (RBSTAXItemsQueue.Count > 0)
                {
                    Common.browser.Load("javascript:PostDBInvoiceBillError('','','" + RBSTAXItemsQueue[0].InvoiceId + "','" + RBSTAXItemsQueue[0].message + "','" + message + "');");
                    if (RBSTAXItemsQueue[0].fromIP == null || RBSTAXItemsQueue[0].fromIP == "")
                    {
                        Common.browser.Load("javascript:showNotification('Error on printing with RBS. " + message + "');");
                    }
                    else
                    {
                        Escpos escpos = new Escpos();
                        escpos.SendMessage(RBSTAXItemsQueue[0].fromIP, "ErrorMessage^^^" + message, sendPort);
                    }
                }


                try
                {
                    if (Common.ws != null && Common.ws.ReadyState != WebSocketState.Closed)
                        Common.ws.Close();
                }
                catch (System.Exception e1) { }
                Common.ws = null;
                RBSTaxItemQueueCompleted();
            }
            private void CheckRBSTaxErrorCode(string code) {
                string message = RBSTaxGetErrorMessage(code);

                Common.browser.Load("javascript:PostDBInvoiceBillError('','','" + RBSTAXItemsQueue[0].InvoiceId + "','" + RBSTAXItemsQueue[0].message + "','" + message + "');");
                if (RBSTAXItemsQueue[0].fromIP == null || RBSTAXItemsQueue[0].fromIP == "")
                {
                    Common.browser.Load("javascript:showNotification('Error on printing with RBS. " + message + "');");
                }
                else {
                    Escpos escpos = new Escpos();
                    escpos.SendMessage(RBSTAXItemsQueue[0].fromIP, "ErrorMessage^^^" + message, sendPort);
                }
            }
            private string RBSTaxGetErrorMessage(string code) {
                string message = "RBS Error : " + code;
                int intCode;
                bool successfullyParsed = int.TryParse(code, out intCode);
                if (successfullyParsed)
                {
                    if (intCode == 1) message = "The protocol command expects more fields";
                    if (intCode == 2) message = "A protocol command field is longer than expected";
                    if (intCode == 3) message = "A protocol command filed is smaller than expected";
                    if (intCode == 4) message = "Check the protocol command fields";
                    if (intCode == 5) message = "Check the protocol command fields";
                    if (intCode == 6) message = "The protocol command is not supported";
                    if (intCode == 7) message = "The PLU code doesn’t exist";
                    if (intCode == 8) message = "The DPT Code doesn’t exist";
                    if (intCode == 9) message = "Wrong VAT code";
                    if (intCode == 10) message = "The Clerk’s index number doesn’t exist";
                    if (intCode == 11) message = "Wrong Clerk’s password";
                    if (intCode == 12) message = "The payment code doesn’t exist";
                    if (intCode == 13) message = "The requested Fiscal record doesn’t exist";
                    if (intCode == 14) message = "The requested Fiscal record type doesn’t exist";
                    if (intCode == 15) message = "Printing type Error";
                    if (intCode == 16) message = "The day is open, issue a Z- Report first";
                    if (intCode == 17) message = "Disconnect Jumpers first";
                    if (intCode == 18) message = "Wrong TIME, not allowed operation";
                    if (intCode == 19) message = "CAN NOT PERFORM SALES";
                    if (intCode == 20) message = "A transaction is open, close the transaction first";
                    if (intCode == 21) message = "Receipt in Payment";
                    if (intCode == 22) message = "CASH IN/OUT transaction in progress";
                    if (intCode == 23) message = "Wrong VAT rate";
                    if (intCode == 24) message = "Price Error";
                    if (intCode == 25) message = "The online communication of the ECR is ON";
                    if (intCode == 26) message = "The ECR is busy, try again later";
                    if (intCode == 27) message = "Invalid sales operation";
                    if (intCode == 28) message = "Invalid Discount/Markup type";
                    if (intCode == 29) message = "No more headers can be programmed";
                    if (intCode == 30) message = "A user’s report is open";
                    if (intCode == 31) message = "A user’s report is open";
                    if (intCode == 32) message = "The Fiscal Memory has no transactions";
                    if (intCode == 33) message = "Discount/Markup index number Error";
                    if (intCode == 34) message = "You can’t program any more PLUs";
                    if (intCode == 35) message = "Error in BMP Data";
                    if (intCode == 36) message = "The BMP index number doesn’t exist";
                    if (intCode == 37) message = "The category index number doesn’t exist";
                    if (intCode == 38) message = "Printer ID index Error";
                    if (intCode == 39) message = "Error printing type";
                    if (intCode == 40) message = "Unit ID index Error";
                    if (intCode == 41) message = "No more sales can be performed";
                    if (intCode == 42) message = "Keyboard Error-or keyboard disconnected";
                    if (intCode == 43) message = "Battery Error.";
                    if (intCode == 44) message = "Paper end.";
                    if (intCode == 45) message = "Error with the cutter.";
                    if (intCode == 46) message = "The printer is disconnected.";
                    if (intCode == 47) message = "Printer overheated.";
                    if (intCode == 48) message = "The fiscal memory is offline.";
                    if (intCode == 49) message = "Fatal Error.";
                    if (intCode == 50) message = "Jumpers are still ON.";
                    if (intCode == 51) message = "The printer’s cover is open.";
                    if (intCode == 52) message = "No more VATs can be programmed";
                    if (intCode == 53) message = "No more lines.";
                    if (intCode == 54) message = "Into Menu.";
                    if (intCode == 55) message = "No Ticket Discount.";
                    if (intCode == 56) message = "Not supported.";
                    if (intCode == 57) message = "Access not allowed for current clerk.";
                    if (intCode == 58) message = "Wrong Baud Rate.";
                    if (intCode == 60) message = "Inactive PLU.";
                    if (intCode == 61) message = "Fiscal Memory is full.";
                    if (intCode == 62) message = "Wrong Coupon Index";
                    if (intCode == 63) message = "Larger Quantity Value than the one allowed.";
                    if (intCode == 64) message = "Inactive payment type.";
                    if (intCode == 65) message = "Inactive DPT.";
                    if (intCode == 66) message = "No more Stock";
                    if (intCode == 67) message = "Serial port error";
                    if (intCode == 68) message = "Cannot open Drawer.";
                    if (intCode == 69) message = "Open ticket";
                    if (intCode == 70) message = "There is not an open receipt.";
                    if (intCode == 71) message = "Error in SD reports.";
                    if (intCode == 72) message = "Inactive ticket";
                    if (intCode == 73) message = "Wrong coupon";
                    if (intCode == 74) message = "Discount/Markup limit is exceeded.";
                    if (intCode == 75) message = "Zero Discount/Markup.";
                    if (intCode == 76) message = "Description is blank.";
                    if (intCode == 77) message = "Wrong barcode";
                    if (intCode == 78) message = "Wrong Key code.";
                    if (intCode == 79) message = "EJ Transfer";
                    if (intCode == 81) message = "The SD is full.";
                    if (intCode == 82) message = "The SD is old.";
                    if (intCode == 83) message = "Negative Receipt total.";
                    if (intCode == 84) message = "Clientindex Error";
                    if (intCode == 85) message = "Wrong Client code.";
                    if (intCode == 86) message = "Out of limits bonus.";
                    if (intCode == 87) message = "Wrong Promotion link index.";
                    if (intCode == 88) message = "WORD NOT ALLOWED";
                    if (intCode == 89) message = "Error in barcode data.";
                    if (intCode == 90) message = "Change is not allowed for this Payment type.";
                    if (intCode == 91) message = "Must insert the payment amount.";
                    if (intCode == 92) message = "Same Header lines.";
                    if (intCode == 93) message = "In Error Message.";
                    if (intCode == 94) message = "Not found SD data.";
                    if (intCode == 95) message = "There is no more data to read from SD.";
                    if (intCode == 96) message = "Set the limit to read SD data.";
                    if (intCode == 97) message = "Receipt sales amount exceeded.";
                    if (intCode == 98) message = "Daily sales amount exceeded.";
                    if (intCode == 99) message = "The SD is full.";
                    if (intCode == 100) message = "Wrong ticket index";
                    if (intCode == 101) message = "Fiscal Communication problem.";
                    if (intCode == 102) message = "Cannot transfer payment amount";
                    if (intCode == 104) message = "LCD disconnection";
                    if (intCode == 105) message = "SD disconnection";
                    if (intCode == 106) message = "Battery Error";
                    if (intCode == 107) message = "NAND Memory full";
                    if (intCode == 108) message = "Wrong TIN";
                    if (intCode == 109) message = "Empty EJ";
                    if (intCode == 110) message = "Invalid IP";
                    if (intCode == 111) message = "Invalid refund";
                    if (intCode == 112) message = "Invalid void";
                    if (intCode == 113) message = "Exceed amount limit";
                    if (intCode == 114) message = "Null header";
                    if (intCode == 115) message = "Inactive clerk";
                    if (intCode == 116) message = "Barcode error";
                    if (intCode == 117) message = "Need program TIN";
                    if (intCode == 118) message = "SD memory need format";
                    if (intCode == 119) message = "Wrong Date";
                    if (intCode == 120) message = "Wrong Time";
                    if (intCode == 121) message = "Call technician";
                    if (intCode == 122) message = "Cannot open EJ file";
                    if (intCode == 123) message = "Cannot write EJ file";
                    if (intCode == 124) message = "Cannot read EJ file";
                    if (intCode == 125) message = "Wrong GSIS AES Key";
                    if (intCode == 126) message = "Cannot change";
                    if (intCode == 127) message = "Ethernet communication error";
                    if (intCode == 128) message = "Send GSIS error";
                    if (intCode == 129) message = "Fiscal Blank";
                    if (intCode == 130) message = "Web Services file error";
                    if (intCode == 132) message = "Active GPRS";
                    if (intCode == 133) message = "Wrong Activate Services Key";
                    if (intCode == 134) message = "Cannot open file in SD";
                    if (intCode == 135) message = "Cannot write file in SD";
                    if (intCode == 136) message = "Need quantity";
                    if (intCode == 137) message = "Receipt closed";
                    if (intCode == 138) message = "Unauthorized function";
                    if (intCode == 139) message = "Already used coupon";
                    if (intCode == 140) message = "Invalid key for services’ activation or already activated";
                    if (intCode == 141) message = "Invalid client coupon";
                    if (intCode == 142) message = "Inactive client";
                    if (intCode == 143) message = "Barcode error (not valid EAN)";
                    if (intCode == 144) message = "Same client code";
                    if (intCode == 145) message = "Reserved";
                    if (intCode == 146) message = "Error in header FM write";
                    if (intCode == 147) message = "No more clients";
                    if (intCode == 148) message = "Reserved";
                    if (intCode == 149) message = "Error in I-BANK Pay";
                    if (intCode == 150) message = "Reserved";
                    if (intCode == 151) message = "Client not found";
                    if (intCode == 152) message = "Need to confirm new invoice row";

                    message = code + " " + message;
                }
                return message;
            }
            #endregion

            #region RBS FHM Restaurant ORDER
            private List<RBSTAXItem> RBSFHMRestaurantItemsQueue = new List<RBSTAXItem>();
            private bool RBSFHMRestaurantIsExecuting = false;
            private RBSFHMRestaurant rBSFHM;
            private string RBSFHMRestaurantExecutingInvoiceID;
            public void executePrintFHMOrder(string ip, string message, String InvoiceId, String lineIDs, String fromIP)
            {
                if (!RBSFHMRestaurantItemsQueue.Any(p => p.InvoiceId == InvoiceId))
                {
                    RBSTAXItem item = new RBSTAXItem();
                    item.ip = ip;
                    item.message = message;
                    item.InvoiceId = InvoiceId;
                    item.lineIDs = lineIDs;
                    item.fromIP = fromIP;
                    item.MoveType = RBSFHMMoveType.Order;
                    item.KeepOpenForPayment = false;
                    RBSFHMRestaurantItemsQueue.Add(item);

                    RBSFHMRestaurantQueueExecute();
                }
            }
            private void RBSFHMRestaurantQueueExecute(bool executeWhileIsExecuting = false)
            {
                if (RBSFHMRestaurantItemsQueue.Count == 0)
                {
                    return;
                }

                if (executeWhileIsExecuting == false && RBSFHMRestaurantIsExecuting == true) return;
                RBSFHMRestaurantIsExecuting = true;

                RBSTAXItem item = RBSFHMRestaurantItemsQueue[0];
                RBSFHMRestaurantExecutingInvoiceID = item.InvoiceId;

                string[] ipDetails = item.ip.Split(':');
                if (ipDetails.Count() == 2) {
                    rBSFHM = new RBSFHMRestaurant(item.message, ipDetails[0], Convert.ToInt32(ipDetails[1]), item.fromIP, item.InvoiceId, item.lineIDs, item.MoveType, item.KeepOpenForPayment,item.BillId,item.FirstMessage);
                    rBSFHM.PrintEndedEvent += rBSFHM_PrintEndedEvent;
                    rBSFHM.PrintReceipt();
                }
                else
                {
                }

            }
            private void rBSFHM_PrintEndedEvent() {
                if (RBSFHMRestaurantItemsQueue.Count > 0)
                {
                    if (rBSFHM != null && rBSFHM.KeepPrinting) {
                        RBSFHMRestaurantItemsQueue[0].KeepOpenForPayment = true;
                    }
                    if (!RBSFHMRestaurantItemsQueue[0].KeepOpenForPayment)
                        RBSFHMRestaurantIsExecuting = false;
                    RBSFHMRestaurantItemsQueue.RemoveAt(0);
                }
                else
                {
                    //RBSFHMRestaurantIsExecuting = false;
                }
                if (rBSFHM != null && rBSFHM.PrintEnded) rBSFHM = null;

                if (RBSFHMRestaurantIsExecuting == false)
                    RBSFHMRestaurantQueueExecute();
            }
            public void executePrintFHMCloseTable(string ip, string message, String InvoiceId, String lineIDs, String fromIP, bool keepOpenForPayment, bool executeWithoutQueue, bool isTakeAway,bool FirstMessage)
            {
                if (executeWithoutQueue)
                {
                    if (RBSFHMIsExecuting == true && rBSFHM != null && InvoiceId == rBSFHM.InvoiceId) {
                        rBSFHM.CloseConnection();
                        rBSFHM = null;
                    }

                    if (rBSFHM != null && rBSFHM.MoveType == RBSFHMMoveType.CloseTable && RBSFHMRestaurantExecutingInvoiceID == InvoiceId)
                    {
                        rBSFHM.Payment(message, keepOpenForPayment, FirstMessage);
                    }
                    else
                    {
                        if (RBSFHMRestaurantExecutingInvoiceID == InvoiceId)
                        {
                            RBSTAXItem item = new RBSTAXItem();
                            item.ip = ip;
                            item.message = message;
                            item.InvoiceId = InvoiceId;
                            item.lineIDs = lineIDs;
                            item.fromIP = fromIP;
                            item.FirstMessage = FirstMessage;
                            item.MoveType = RBSFHMMoveType.CloseTable;
                            if (isTakeAway) item.MoveType = RBSFHMMoveType.TakeAway;
                            item.KeepOpenForPayment = keepOpenForPayment;
                            RBSFHMRestaurantItemsQueue.Insert(0, item);

                            RBSFHMRestaurantQueueExecute(true);
                        }
                        else
                        {
                            RBSTAXItem item = new RBSTAXItem();
                            item.ip = ip;
                            item.message = message;
                            item.InvoiceId = InvoiceId;
                            item.lineIDs = lineIDs;
                            item.fromIP = fromIP;
                            item.FirstMessage = FirstMessage;
                            item.MoveType = RBSFHMMoveType.CloseTable;
                            if (isTakeAway) item.MoveType = RBSFHMMoveType.TakeAway;
                            item.KeepOpenForPayment = keepOpenForPayment;
                            RBSFHMRestaurantItemsQueue.Add(item);

                            RBSFHMRestaurantQueueExecute();
                        }

                    }
                }
                else
                {
                    RBSTAXItem item = new RBSTAXItem();
                    item.ip = ip;
                    item.message = message;
                    item.InvoiceId = InvoiceId;
                    item.lineIDs = lineIDs;
                    item.fromIP = fromIP;
                    item.FirstMessage = FirstMessage;
                    item.MoveType = RBSFHMMoveType.CloseTable;
                    if (isTakeAway) item.MoveType = RBSFHMMoveType.TakeAway;
                    item.KeepOpenForPayment = keepOpenForPayment;
                    RBSFHMRestaurantItemsQueue.Add(item);

                    RBSFHMRestaurantQueueExecute();
                }
            }
            public void executePrintFHMCancelOrder(string ip, string message, String InvoiceId, String lineIDs, String fromIP)
            {
                if (!RBSFHMRestaurantItemsQueue.Any(p => p.InvoiceId == InvoiceId))
                {
                    RBSTAXItem item = new RBSTAXItem();
                    item.ip = ip;
                    item.message = message;
                    item.InvoiceId = InvoiceId;
                    item.lineIDs = lineIDs;
                    item.fromIP = fromIP;
                    item.MoveType = RBSFHMMoveType.Return;
                    RBSFHMRestaurantItemsQueue.Add(item);

                    if (timer != null)
                    {
                        timer.Stop();
                    }

                    RBSFHMRestaurantQueueExecute();
                }
            }
            public void executePrintFHMCancel(string ip,string message, string InvoiceId, string lineIDs, string fromIP, string BillId){

                RBSTAXItem item = new RBSTAXItem();
                item.ip = ip;
                item.message = message;
                item.InvoiceId = InvoiceId;
                item.lineIDs = lineIDs;
                item.fromIP = fromIP;
                item.MoveType = RBSFHMMoveType.Cancel;
                item.BillId = BillId;

                RBSFHMRestaurantItemsQueue.Add(item);

                RBSFHMRestaurantQueueExecute();

            }
            public void executePrintFHMTransfer(string ip, string message, string InvoiceId, string fromIP , string id,string tablecode)
            {
                if (!RBSFHMRestaurantItemsQueue.Any(p => p.InvoiceId == InvoiceId))
                {
                    RBSTAXItem item = new RBSTAXItem();
                    item.ip = ip;
                    item.message = message;
                    item.InvoiceId = InvoiceId;
                    item.lineIDs = id;
                    item.BillId = tablecode;
                    item.fromIP = fromIP;
                    item.MoveType = RBSFHMMoveType.Transfer;
                    item.KeepOpenForPayment = false;
                    RBSFHMRestaurantItemsQueue.Add(item);

                    RBSFHMRestaurantQueueExecute();
                }
            }
            public void executeInvoicePaymentForFHM(string ip, string message, string InvoiceId, string fromIP) {
                if (!RBSFHMRestaurantItemsQueue.Any(p => p.InvoiceId == InvoiceId))
                {
                    RBSTAXItem item = new RBSTAXItem();
                    item.ip = ip;
                    item.message = message;
                    item.InvoiceId = InvoiceId;
                    item.lineIDs = "";
                    item.BillId = "";
                    item.fromIP = fromIP;
                    item.MoveType = RBSFHMMoveType.InvoicePayment;
                    item.KeepOpenForPayment = false;
                    RBSFHMRestaurantItemsQueue.Add(item);

                    RBSFHMRestaurantQueueExecute();
                }
            }
            public void executeFHMClearTable(string ip, string message, string InvoiceId, string lineIDs, string fromIP) {
                if (!RBSFHMRestaurantItemsQueue.Any(p => p.InvoiceId == InvoiceId))
                {
                    RBSTAXItem item = new RBSTAXItem();
                    item.ip = ip;
                    item.message = message;
                    item.InvoiceId = InvoiceId;
                    item.lineIDs = lineIDs;
                    item.fromIP = fromIP;
                    item.MoveType = RBSFHMMoveType.ClearTable;
                    RBSFHMRestaurantItemsQueue.Add(item);

                    if (timer != null)
                    {
                        timer.Stop();
                    }

                    RBSFHMRestaurantQueueExecute();
                }
            }
            #endregion

            #region myPOS GO
            public void executeUsbTransaction(String amount)
			{
				RequestResult r = t.Purchase(Convert.ToDouble(amount.Replace(".", ",")), Currencies.EUR, "00230");
				switch (r)
				{
					case RequestResult.Busy:
						Common.browser.Load("javascript:showNotification('Πρόβλημα συναλλαγής η συσκευή είναι δεσμευμένη');");
						break;
					case RequestResult.InvalidParams:
						Common.browser.Load("javascript:showNotification('Πρόβλημα συναλλαγής λάθος παράμετροι');");
						break;
					case RequestResult.NotInitialized:
						//MessageBox.Show("RequestResult: " + r.ToString());
						Common.browser.Load("javascript:showNotification('Πρόβλημα συναλλαγής δεν βρέθηκε το myPOS Go');");
						break;
					default: break;
				}
			}
			#endregion

		}
		private void OnIsBrowserInitializedChanged(object sender, EventArgs e)
        {
            var b = ((ChromiumWebBrowser)sender);

            this.InvokeOnUiThreadIfRequired(() => b.Focus());
        }

        private void OnBrowserConsoleMessage(object sender, ConsoleMessageEventArgs args)
        {
			//DisplayOutput(string.Format("Line: {0}, Source: {1}, Message: {2}", args.Line, args.Source, args.Message));
			Console.WriteLine(string.Format("Line: {0}, Source: {1}, Message: {2}", args.Line, args.Source, args.Message));
        }

        private void OnBrowserStatusMessage(object sender, StatusMessageEventArgs args)
        {
            this.InvokeOnUiThreadIfRequired(() => status = args.Value);
        }

        private void OnLoadingStateChanged(object sender, LoadingStateChangedEventArgs args)
        {
            SetCanGoBack(args.CanGoBack);
            SetCanGoForward(args.CanGoForward);

            this.InvokeOnUiThreadIfRequired(() => SetIsLoading(!args.CanReload));
        }

        private void OnBrowserTitleChanged(object sender, TitleChangedEventArgs args)
        {
            this.InvokeOnUiThreadIfRequired(() => Text = args.Title);
        }

        private void OnBrowserAddressChanged(object sender, AddressChangedEventArgs args)
        {
            //this.InvokeOnUiThreadIfRequired(() => urlTextBox.Text = args.Address);
        }

        private void SetCanGoBack(bool canGoBack)
        {
            //this.InvokeOnUiThreadIfRequired(() => backButton.Enabled = canGoBack);
        }

        private void SetCanGoForward(bool canGoForward)
        {
            //this.InvokeOnUiThreadIfRequired(() => forwardButton.Enabled = canGoForward);
        }

        private void SetIsLoading(bool isLoading)
        {
           
        }

        public void DisplayOutput(string output)
        {
            this.InvokeOnUiThreadIfRequired(() => status = output);
        }

       



        private void ExitMenuItemClick(object sender, EventArgs e)
        {
            Common.browser.Dispose();
            Cef.Shutdown();
            Close();
        }

      

        private void LoadUrl(string url)
        {
            if (Uri.IsWellFormedUriString(url, UriKind.RelativeOrAbsolute))
            {
                Common.browser.Load(url);
            }
        }

 

	
        protected override void WndProc(ref Message m) {
            base.WndProc(ref m);
            //m.WParam = the wID you gave the Menu Item
            if ((m.Msg == WM_SYSCOMMAND) && ((int)m.WParam == 0x1)) {
                Common.browser.ShowDevTools();
            }

            if ((m.Msg == WM_SYSCOMMAND) && ((int)m.WParam == 0x2)) {
                Escpos esc = new Escpos();
                frmIP f = new frmIP();
                if (f.ShowDialog() == DialogResult.OK) {
                    esc.PrintCodePageTest(f.IP, "9100",f.Encoding);
                }
                esc.Dispose();
                esc = null;
            }

			if ((m.Msg == WM_SYSCOMMAND) && ((int)m.WParam == 0x3))
            {
				RequestResult r = t.Purchase(1.00, Currencies.EUR , "00230");
				switch (r)
				{
					case RequestResult.Busy:
					case RequestResult.InvalidParams:
					case RequestResult.NotInitialized:
						MessageBox.Show("RequestResult: " + r.ToString());
						break;
					default: break;
				}

			}
		}

		private void BrowserForm_FormClosing(object sender, FormClosingEventArgs e)
		{
            Common.browser.Dispose();
            Common.browser = null;
            this.Dispose();

            Process.GetCurrentProcess().Kill();

        }

        public static void Log(string message) {
            try
            {
                string fileName = System.Windows.Forms.Application.StartupPath + "\\" + "Log.txt";
                using (System.IO.StreamWriter fs = System.IO.File.AppendText(fileName))
                {
                    fs.WriteLine(DateTime.Now.ToString());
                    fs.WriteLine(message);
                }

            }
            catch (System.Exception ex)
            {
            }
        }


		#region myPOS GO Methods

	

		private bool ParseAmount(String amount, double cAmount)
		{
			string amt = amount;
			amt = amt.Replace(",", ".");
			return Double.TryParse(amt, System.Globalization.NumberStyles.Number, CultureInfo.InvariantCulture, out cAmount);
		}


		protected void ProcessResult(ProcessingResult r)
		{
			StringBuilder sb = new StringBuilder();
			sb.AppendFormat("Processing \"{0}\" finished\r\n", r.Method.ToString());
			sb.AppendFormat("Status: \"{0}\"\r\n", r.Status.ToString());

			if (r.TranData != null)
			{
				sb.AppendFormat("\r\nTransaction data:\r\n");
				sb.AppendFormat("Type: {0}\r\n", r.TranData.Type);
				sb.AppendFormat("Amount: {0}\r\n", r.TranData.Amount);
				sb.AppendFormat("Tip Amount: {0}\r\n", r.TranData.TipAmount);
				sb.AppendFormat("Currency: {0}\r\n", r.TranData.Currency.ToString());
				sb.AppendFormat("Approval: {0}\r\n", r.TranData.Approval);
				sb.AppendFormat("Auth code: {0}\r\n", r.TranData.AuthCode);
				sb.AppendFormat("Preauth Code: {0}\r\n", r.TranData.PreauthCode);
				sb.AppendFormat("RRN: {0}\r\n", r.TranData.RRN);
				sb.AppendFormat("Date: {0}\r\n", r.TranData.TransactionDate.ToString("dd.MM.yyyy"));
				sb.AppendFormat("Time: {0}\r\n", r.TranData.TransactionDate.ToString("HH:mm:ss"));
				sb.AppendFormat("Terminal ID: {0}\r\n", r.TranData.TerminalID);
				sb.AppendFormat("Merchant ID: {0}\r\n", r.TranData.MerchantID);
				sb.AppendFormat("Merchant Name: {0}\r\n", r.TranData.MerchantName);
				sb.AppendFormat("Merchant Address Line 1: {0}\r\n", r.TranData.MerchantAddressLine1);
				sb.AppendFormat("Merchant Address Line 2: {0}\r\n", r.TranData.MerchantAddressLine2);
				sb.AppendFormat("PAN Masked: {0}\r\n", r.TranData.PANMasked);
				sb.AppendFormat("Emboss Name: {0}\r\n", r.TranData.EmbossName);
				sb.AppendFormat("AID: {0}\r\n", r.TranData.AID);
				sb.AppendFormat("AID Name: {0}\r\n", r.TranData.AIDName);
				sb.AppendFormat("AID Preferred Name: {0}\r\n", r.TranData.ApplicationPreferredName);
				sb.AppendFormat("STAN: {0}\r\n", r.TranData.Stan);
				sb.AppendFormat("Signature Required: {0}\r\n", r.TranData.SignatureRequired ? "Yes" : "No");
				sb.AppendFormat("Software Version: {0}\r\n", r.TranData.SoftwareVersion);
			}


            if(r.Status!= TransactionStatus.Success)
            {
				Common.browser.Load("javascript:showNotification('Πρόβλημα συναλλαγής:' " + r.Status.ToString() +");");

			}
            else
            {
				Common.browser.Load("javascript:CardlinkPaymentApproved()");
				Common.browser.Load("javascript:CardlinkSuccess()");
			}
		


			Log(sb.ToString());
			//MessageBox.Show(sb.ToString());
		}

		void _PresentCard()
		{
			Log("Present card");
		}

		void _CardDetected(bool is_wrong_card)
		{
			if (!is_wrong_card)
			{
				Common.browser.Load("javascript:showNotification('Βρέθηκε κάρτα');");
				Log("Card detected");
			}
			else
			{
				Common.browser.Load("javascript:showNotification('Βρέθηκε μη έγκυρη κάρτα');");
				Log("Bad card detected");
			}
		}

		string _ReceiptReceiver()
		{
			string result = "";
			//ReceiptReceiverForm formEntry = new ReceiptReceiverForm();
			//if (formEntry.ShowDialog() == DialogResult.OK)
			//{
			//	result = formEntry.txtResult.Text;
			//}
			return result;
		}

		void _PresentPin(int pin_tries_left)
		{
			if (pin_tries_left == 0)
			{
				Common.browser.Load("javascript:showNotification('Απαιτείται ΠΙΝ');");
				Log("Present online pin");
			}
			else
			{
				Common.browser.Load("javascript:showNotification('Απαιτείται ΠΙΝ: Επιτρεπτές προσπάθειες: "+ pin_tries_left + " ');");
				Log(String.Format("Present offline pin. Tries left: {0}", pin_tries_left));
			}
		}

		void _PinEntered(bool is_wrong_pin, int pin_tries_left)
		{
			if (is_wrong_pin == false)
			{
				Common.browser.Load("javascript:showNotification('Πληκτρολογήθηκε το ΠΙΝ');");
				Log("PIN entered");
			}
			else
			{
				Common.browser.Load("javascript:showNotification('Λάθος ΠΙΝ: Υπολοιπόμενες προσπάθειες "+ pin_tries_left + "');");
				Log(String.Format("Wrong PIN entered. Tries left: {0}", pin_tries_left));
			}
		}

		void _PresentDCC(DCCRequest dcc_req)
		{
			Log("Present DCC");
			Log(String.Format("{0} {1}", dcc_req.OriginalAmount, dcc_req.OriginalCurrencyName));
			Log(String.Format("{0} {1}", dcc_req.DCCAmount, dcc_req.DCCCurrencyName));
			Log(String.Format("1 {0} = {1} {2}", dcc_req.OriginalCurrencyName, dcc_req.DCCExchangeRate, dcc_req.DCCCurrencyName));
		}

		void _DCCSelected(bool is_dcc_used)
		{
			if (is_dcc_used)
			{
				Log("DCC used");
			}
			else
			{
				Log("No DCC used");
			}
		}

        #endregion

        private void BrowserForm_Load(object sender, EventArgs e)
        {
            AutoUpdater.Start("https://www.ninepos.com/versioninfo.xml");
            //AutoUpdater.Start("ftps://waws-prod-am2-443.ftp.azurewebsites.windows.net/site/wwwroot/versioninfo.xml", new NetworkCredential("NineSoftBackOffice\\$NineSoftBackOffice", "ZiLrPl7cqHvjyB29iRHreiB1hZy6GdhsdlY24oP5gFoF73bNgpvwpoMaaRbf"));

            AutoUpdater.Synchronous = true;
            AutoUpdater.ShowSkipButton = false;
            AutoUpdater.ShowRemindLaterButton = false;
            AutoUpdater.Mandatory = true;
            AutoUpdater.UpdateMode = Mode.Forced;
            //AutoUpdater.DownloadPath = this.ab;
        }
    }
}


public class PrintZData { 
    public string ZReport { get; set; }
    public string ZBills { get; set; }
    public string ZCancels { get; set; }
}
public class ZBill { 
    public DateTime BillDate { get; set; }
    public string Data { get; set; }
}

public class BillItem {
    public String SendSocketMessageStatus { get; set; }
    public String  SocketAddress { get; set; }
    public String e_line { get; set; }
    public String ip { get; set; }
    public String port { get; set; }
    public String message { get; set; }
    public String encoding { get; set; }
    public String codepage1 { get; set; }
    public String codepage2 { get; set; }
    public String codepage3 { get; set; }
    public String InvoiceId { get; set; }
    public String BillCode { get; set; }
    public String lineIDs { get; set; }
    public String contextData { get; set; }
    public String fromIP { get; set; }
    public String PrintType { get; set; }
    public String BillType { get; set; }
    public int MessageNum { get; set; }
}

public class RBSTAXItem {
    public string ip { get; set; }
    public string message { get; set; }
    public string InvoiceId { get; set; }
    public string lineIDs { get; set; }
    public string fromIP { get; set; }
    public RBSFHMMoveType MoveType { get; set; }
    public bool KeepOpenForPayment { get; set; }
    public string BillId { get; set; }
    public bool FirstMessage { get; set; }
}

public class RBSTAXFail
{
    public int PartNum { get; set; }
    public string Description { get; set; }
    public string Error { get; set; }
}

public enum RBSFHMMoveType { 
    None,
    Order,
    Return,
    CloseTable,
    TakeAway,
    Cancel,
    Transfer,
    InvoicePayment,
    ClearTable
}